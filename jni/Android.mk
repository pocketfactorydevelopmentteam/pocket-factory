# Copyright (C) 2010 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := pocketfactory # should match the name in the AndroidManifest.xml
LOCAL_SRC_FILES := main.cpp Utils.cpp common/tile/TileLandMark.cpp common/tile/FarmBlock.cpp common/tile/entity/FarmEntity.cpp common/tile/Drill.cpp common/tile/entity/DrillEntity.cpp common/item/Wrench.cpp common/tile/Wire.cpp common/tile/entity/WireEntity.cpp common/tile/InfiniteEnergy.cpp common/tile/entity/InfiniteEnergyEntity.cpp common/tile/SolarPanel.cpp common/tile/entity/SolarEntity.cpp

LOCAL_LDLIBS    := -L$(LOCAL_PATH) -llog -ldl -lminecraftpe -lmcpelauncher_tinysubstrate

LOCAL_CFLAGS          := -pthread

TARGET_NO_UNDEFINED_LDFLAGS :=

include $(BUILD_SHARED_LIBRARY)
