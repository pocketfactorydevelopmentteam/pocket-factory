#ifndef UTILS_H_
#define UTILS_H_

#include <sstream>
#include <stdlib.h>
#include <stdint.h>
#include <android/log.h>
#include <dlfcn.h>
#include <map>

#include "mcpe/level/Level.h"
#include "mcpe/item/ItemInstance.h"
#include "mcpe/entity/player/Inventory.h"
#include "mcpe/gui/gui/Font.h"
#include "mcpe/gui/gui/Gui.h"


#define MINECRAFT_HIT_RESULT_OFFSET 2680
#define DEBUG 0
#define LOG_TAG "PocketFactory"

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))

class TileTessellator{
	public:
	TileSource* tileSource;
	Tessellator* tessellator;
	
	virtual void appendTessellatedTile(FullTile const&);
	virtual void tessellateTorch(Tile*, float, float, float, float, float);
	virtual void tessellateTorchInWorld(Tile*, TilePos const&);
};
enum ChatColor{
	PREFIX,
	BLACK,
	DARK_BLUE,
	DARK_GREEN,
	DARK_AQUA,
	DARK_RED,
	DARK_PURPLE,
	GOLD,
	GRAY,
	DARK_GRAY,
	BLUE,
	GREEN,
	AQUA,
	RED,
	LIGHT_PURPLE,
	YELLOW,
	WHITE,
};

int getSlotIfExistItemAndNotFull(Inventory* inv, int id, int damage, int maxStack);
void dropItem(TileSource*, ItemInstance*, float, float, float);

void bl_dumpVtable(void** vtable, size_t size);

bool existInVTable(void** vtable, size_t size, std::string symbol);

template<typename Base, typename T>
inline bool instanceof(const T*) {
    return std::is_base_of<Base, T>::value;
}

#endif
