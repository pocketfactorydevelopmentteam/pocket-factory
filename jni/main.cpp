#include <jni.h>
#include <dlfcn.h>
#include <android/log.h>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <sstream>

#include "Substrate.h"
#include "Utils.h"

#include "common/item/Wrench.h"
#include "common/tile/Drill.h"
#include "common/tile/FarmBlock.h"
#include "common/tile/Wire.h"
#include "common/tile/SolarPanel.h"
#include "common/tile/InfiniteEnergy.h"

#include "common/tile/entity/SolarEntity.h"
#include "common/tile/entity/DrillEntity.h"
#include "common/tile/entity/FarmEntity.h"
#include "common/tile/entity/WireEntity.h"
#include "common/tile/entity/InfiniteEnergyEntity.h"
#include "common/tile/SolarPanel.h"
#include "common/tile/TileLandMark.h"

#include "mcpe/Recipe.h"
#include "mcpe/item/TileItem.h"
#include "mcpe/tile/entity/SignTileEntity.h"
#include "mcpe/tile/entity/ChestTileEntity.h"
#include "mcpe/tile/entity/FurnaceTileEntity.h"
#include "mcpe/tile/entity/MobSpawnerTileEntity.h"
#include "mcpe/tile/entity/NetherReactorTileEntity.h"

#include "mcpe/gui/gui/Gui.h"
#include "mcpe/item/ItemRenderer.h"
#include "mcpe/game/MinecraftClient.h"

#define MINECRAFT_HIT_RESULT_OFFSET 2680


bool itemsAdded = false;
bool registered = false;
bool clientMsg = false;

int screen_width = 0;
int screen_height = 0;

std::map <std::string, std::string>* bl_I18n_strings;

static void (*Minecraft_leaveGame_real)(Minecraft*, bool, bool);

static bool(*TileTessellator_tessellateInWorld_real)(TileTessellator*, Tile*, const TilePos&, bool);

std::string (*NameReal)(void* p);
std::string nameHook(void *p){
	return NameReal(p) + " & PocketFactory v1.0";
}

std::string getColor(int pos){ //if this is placed in ChatColor.h, it causes errors: redefinitions
	static const std::string colors[] = {"\u00a7", "\u00a70", "\u00a71",
	"\u00a72", "\u00a73", "\u00a74", "\u00a75", "\u00a76", "\u00a77",
	"\u00a78", "\u00a79", "\u00a7a", "\u00a7b", "\u00a7c", "\u00a7d",
	"\u00a7e", "\u00a7f"};
	return colors[pos];
}

void clientMessage(Gui* gui, int color, std::string s){
	std::stringstream nstr, chat_tag;
	nstr << getColor(color) << s << getColor(ChatColor::WHITE);
	chat_tag << getColor(ChatColor::GOLD) << "Pocket Factory" << getColor(ChatColor::WHITE);
	gui->addMessage(chat_tag.str(), nstr.str(), 95); // ref: addMessage(chat tag, text, duration);
}

static void Minecraft_leaveGame_hook(Minecraft* minecraft, bool saveLevel, bool b2) {
	clientMsg = false;
	Minecraft_leaveGame_real(minecraft, saveLevel, b2);
}

static void(*CreativeInventoryScreen_populateItem_real)(Item*, int, int);
static void CreativeInventoryScreen_populateItem_hook(Item* item, int count, int damage) {
	if(!itemsAdded) {
		//CreativeInventoryScreen_populateItem_real(Item::items[TileLandMark::MARK_ID], 1, 0);
		//this crashes
		itemsAdded = true;
		}
	CreativeInventoryScreen_populateItem_real(item, count, damage);
}

static void (*_Gui$render)(Gui*, float, bool, int, int);
static void Gui$render(Gui* gui, float wtf, bool idk, int idk2, int idk3)
{
	
	_Gui$render(gui, wtf, idk, idk2, idk3);
	if(!clientMsg)
	{
	    clientMessage(gui, ChatColor::GREEN, "PocketFactory Reloaded v1.0");
		clientMessage(gui, ChatColor::AQUA, "by:");
		clientMessage(gui, ChatColor::BLUE, "* artus9033");
		clientMessage(gui, ChatColor::DARK_GREEN, "* InusualZ");
		clientMessage(gui, ChatColor::RED, "* minecraftmuse3");
		clientMessage(gui, ChatColor::DARK_RED, "* 1x3y5");
		clientMessage(gui, ChatColor::GOLD, "* peacestorm");
		clientMsg = true;

		//Why not make a about screen?
	}

	Player* player = gui->minecraft.getPlayer();
	TileSource* tileSource = player->region;

	HitResult* result = (HitResult*) ((uintptr_t) player->level + MINECRAFT_HIT_RESULT_OFFSET);
	Tile* tile = tileSource->getTilePtr(result->tile);
	if(result->type == HitResultType::TILE)
	{
		if(tile == Tile::tiles[Drill::DRILL_ID]){
		DrillEntity* drill = (DrillEntity*)tileSource->getTileEntity(result->tile);
		Color color { (float)(drill->isDrilling ? 0 : 255), (float)(drill->isDrilling ? 255 : 0), 0, (float)255};
		std::string temp("This Laser Drill is ");
		temp += (drill->isDrilling ? "ON" : "OFF");
		gui->minecraft.font->draw(temp, ((screen_width / 2) - (temp.length() * 2)), 10, color);
		}
		/** unneeded wire transmission debug
		//
		if(tile == Tile::tiles[Wire::WIRE_ID]){
		WireEntity* wire = (WireEntity*)tileSource->getTileEntity(result->tile);
		std::stringstream state;
		state << "buffer=" << wire->buffer << ", max power=" << wire->getMaxPowerStored();
		clientMessage(gui, ChatColor::GOLD, state.str());
		}
		//
		**/
	}
}


static void (*_Minecraft$selectLevel)(Minecraft*, std::string const&, std::string const&, LevelSettings const&);
static void Minecraft$selectLevel(Minecraft* minecraft, std::string const& wtf1, std::string const& wtf2, LevelSettings const& settings)
{
	if(!registered)
	{
		registered = true;
		
		(*bl_I18n_strings)["tile.land_mark.name"] = "Land Mark";
		(*bl_I18n_strings)["tile.solar_panel.name"] = "Solar Panel";
		(*bl_I18n_strings)["tile.infinite_energy.name"] = "Infinite Energy";
		(*bl_I18n_strings)["tile.wire.name"] = "Wire";
		(*bl_I18n_strings)["item.wrench.name"] = "Wrench";
		(*bl_I18n_strings)["tile.drill.name"] = "Laser Drill";
		(*bl_I18n_strings)["tile.farmBlock.name"] = "Farmer"; 
		//TODO: add recipes later
	//-------------------------------------------------------------------------------------------

	}
	_Minecraft$selectLevel(minecraft, wtf1, wtf2, settings);
}

static bool TileTessellator_tessellateInWorld_hook(TileTessellator* tt, Tile* t, const TilePos& tp, bool b1) {
	if(t->tileType == TileType::LandMark) {
		TileLandMark* landMark = (TileLandMark*)t;
		landMark->renderLandMark(tt, tt->tessellator, tt->tileSource, tp);
		return true;
	}else{
		return TileTessellator_tessellateInWorld_real(tt, t, tp, b1);
	}
}

static void (*_Screen$setSize)(Screen*, int, int);
static void Screen$setSize(Screen* screen, int width, int height)
{
	_Screen$setSize(screen, width, height);
	screen_width = width;
	screen_height = height;
}

static void (*_Item$initItems)();
static void Item$initItems()
{
	_Item$initItems();
	Item::items[Wrench::WRENCH_ID] = new Wrench(Wrench::WRENCH_ID -256);
}

static void (*_Tile$initTiles)();
static void Tile$initTiles()
{
	_Tile$initTiles();
	Tile::tiles[Drill::DRILL_ID] = new Drill(Drill::DRILL_ID);
	Tile::tiles[FarmBlock::FBLOCK_ID] = new FarmBlock(FarmBlock::FBLOCK_ID);
	Tile::tiles[Wire::WIRE_ID] = new Wire(Wire::WIRE_ID);
	Tile::tiles[InfiniteEnergy::IE_ID] = new InfiniteEnergy(InfiniteEnergy::IE_ID);
	Tile::tiles[SolarPanel::SOLAR_ID] = new SolarPanel(SolarPanel::SOLAR_ID);
	Tile::tiles[TileLandMark::MARK_ID] = new TileLandMark(TileLandMark::MARK_ID);
	
	TileItem* landMark = new TileItem(TileLandMark::MARK_ID - 256);
	TileItem* solarPanel = new TileItem(SolarPanel::SOLAR_ID - 256);
	TileItem* infEnergy = new TileItem(InfiniteEnergy::IE_ID - 256);
	TileItem* wire = new TileItem(Wire::WIRE_ID - 256);
	TileItem* drill = new TileItem(Drill::DRILL_ID - 256);
	TileItem* farm = new TileItem(FarmBlock::FBLOCK_ID - 256);
}

static void (*_TileEntity$initEntities)();
static void TileEntity$initTileEntities()
{
	_TileEntity$initEntities();

	TileEntity::setId(TileEntityType::SolarPanel, SOLAR_STRING_ID);
	TileEntity::setId(TileEntityType::InfiniteEnergy, IE_STRING_ID);
	TileEntity::setId(TileEntityType::Farm, FARM_STRING_ID);
	TileEntity::setId(TileEntityType::Drill, DRILL_STRING_ID);
	TileEntity::setId(TileEntityType::Wire, WIRE_STRING_ID);
}

static TileEntity* (*_TileEntityFactory$createTileEntity)(TileEntityType, const TilePos&);
static TileEntity* TileEntityFactory$createTileEntity(TileEntityType type, const TilePos& pos)
{

	if(type == TileEntityType::Drill) return new DrillEntity(pos);
	else if(type == TileEntityType::Farm) return new FarmEntity(pos);
	else if(type == TileEntityType::Furnace) return new FurnaceTileEntity(pos);
	else if(type == TileEntityType::Chest) return new ChestTileEntity(pos);
	else if(type == TileEntityType::Sign) return new SignTileEntity(pos);
	else if(type == TileEntityType::NetherReactor) return new NetherReactorTileEntity(pos);
	else if(type == TileEntityType::MobSpawner) return new MobSpawnerTileEntity(pos);
	else if(type == TileEntityType::Wire) return new WireEntity(pos);
	else if(type == TileEntityType::InfiniteEnergy) return new InfiniteEnergyEntity(pos);
	else if(type == TileEntityType::SolarPanel) return new SolarEntity(pos);
	return _TileEntityFactory$createTileEntity(type, pos);
}

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
	bl_I18n_strings = (std::map <std::string, std::string> *) dlsym(RTLD_DEFAULT, "_ZN4I18n8_stringsE");

	MSHookFunction((void*)&Item::initItems, (void*)&Item$initItems, (void**)&_Item$initItems);
	MSHookFunction((void*)&Tile::initTiles, (void*)&Tile$initTiles, (void**)&_Tile$initTiles);
	MSHookFunction((void*)&TileEntity::initTileEntities, (void*)&TileEntity$initTileEntities, (void**)&_TileEntity$initEntities);

	void* tileEntityFactory$createTileEntity = dlsym(RTLD_DEFAULT, "_ZN17TileEntityFactory16createTileEntityE14TileEntityTypeRK7TilePos");
	MSHookFunction(tileEntityFactory$createTileEntity, (void*)&TileEntityFactory$createTileEntity, (void**)&_TileEntityFactory$createTileEntity);

	void* screen$setSize = dlsym(RTLD_DEFAULT, "_ZN6Screen7setSizeEii");
	MSHookFunction(screen$setSize, (void*)&Screen$setSize, (void**)&_Screen$setSize);

	void* minecraft$selectLevel = dlsym(RTLD_DEFAULT, "_ZN9Minecraft11selectLevelERKSsS1_RK13LevelSettings");
	MSHookFunction(minecraft$selectLevel, (void*)&Minecraft$selectLevel, (void**)&_Minecraft$selectLevel);

	void* gui$render = dlsym(RTLD_DEFAULT, "_ZN3Gui6renderEfbii");
	MSHookFunction(gui$render, (void*)&Gui$render, (void**)&_Gui$render);
	  
	void* nameh = dlsym(RTLD_DEFAULT,"_ZN6Common20getGameVersionStringEv");
	MSHookFunction(nameh,(void*)&nameHook,(void**)&NameReal);
	
	void* renderHook = dlsym(RTLD_DEFAULT, "_ZN15TileTessellator17tessellateInWorldEP4TileRK7TilePosb");
	MSHookFunction(renderHook, (void*) &TileTessellator_tessellateInWorld_hook, (void**) &TileTessellator_tessellateInWorld_real);
	
	void* CreativeInventoryScreen_populateItem = dlsym(RTLD_DEFAULT, "_ZN23CreativeInventoryScreen12populateItemEP4Itemii");
	MSHookFunction(CreativeInventoryScreen_populateItem, (void*) &CreativeInventoryScreen_populateItem_hook, (void**) &CreativeInventoryScreen_populateItem_real);
	
	void* leaveGame = dlsym(RTLD_DEFAULT, "_ZN9Minecraft9leaveGameEbb");
	MSHookFunction(leaveGame, (void*) &Minecraft_leaveGame_hook, (void**) &Minecraft_leaveGame_real);
	
	return JNI_VERSION_1_2;
}
