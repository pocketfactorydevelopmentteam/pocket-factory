#pragma once

class EnergySource 
{
	virtual int getMaxEnergyStorageCapacity() const = 0;
	
	virtual int getEnergyInStorage() const = 0;

	virtual int receiveEnergy(int energy, bool simulate) = 0;

	virtual int extractEnergy(int energy, bool simulate) = 0;
};