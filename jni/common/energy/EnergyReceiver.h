#pragma once 

class EnergyReceiver 
{
public:
	virtual int getMaxEnergyStorageCapacity() const = 0;
	
	virtual int getEnergyInStorage() const = 0;

	virtual int receiveEnergy(int energy, bool simulate) = 0;
};