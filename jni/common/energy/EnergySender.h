#pragma once


class EnergySender 
{
public:
	virtual int getMaxEnergyStorageCapacity() const = 0;
	
	virtual int getEnergyInStorage() const = 0;

	virtual int extractEnergy(int energy, bool simulate) = 0;
};