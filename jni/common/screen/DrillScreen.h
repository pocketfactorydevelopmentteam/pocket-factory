#ifndef DRILLSCREEN_H_
#define DRILLSCREEN_H_

#include "../../mcpe/gui/screens/Screen.h"
#include "../../mcpe/gui/components/Button.h"
#include "../../mcpe/gui/components/Label.h"
#include "../tile/entity/DrillEntity.h"

class DrillScreen : public Screen
{
private:
	Player* player;
	DrillEntity* drill;

	Touch::THeader* drillHeader;
	Touch::TButton* statusChanger;
	Label* status;
	
	
public:
	DrillScreen(Player*, DrillEntity*);
	
	virtual void init();
	virtual void render(int, int, float);
	virtual void buttonClicked(Button*);
	virtual bool renderGameBehind();
};


#endif