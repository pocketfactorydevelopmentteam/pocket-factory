#include "DrillScreen.h"

DrillScreen::DrillScreen(Player* player, DrillEntity* drill) 
{
	this->drillHeader = new Touch::Header(5, "Drill");
}

virtual void DrillScreen::init()
{
	this->status = new Label((std::string("This Laser Drill is ") + (container->isDrilling ? "ON" : "OFF")), Color::WHITE this->minecraft, 0, 20, 5, true);
	this->statusChanger = new Touch::TButton(10, "ON", this->minecraft, true); 
	
	this->statusChanger->init(this->minecraft);
}

virtual void DrillScreen::render(int width, int height, float i)
{
	this->status->render(this->minecraft, 5, 20);
	this->statusChanger->render(this->minecraft, 30, 20);
	this->drillHeader->render(this->minecraft, 0, 0);
	
}

void DrillScreen::buttonClicked(Button* button)
{
	
}

bool DrillScreen::renderGameBehind()
{
	return false;
}