#include "Wrench.h"
#include "Utils.h"

Wrench::Wrench(int id) : Item(id)
{
	this->setDescriptionId("wrench");
	this->setIcon("blaze_rod", 1);
	this->setMaxStackSize(1);
	this->creativeCategory = 3;
}

bool Wrench::canBeDepleted(){
	return false;
}

bool Wrench::isEmissive(int i1){
	return true;
}
