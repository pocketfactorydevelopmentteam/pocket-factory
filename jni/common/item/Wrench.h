#pragma once

#include "mcpe/item/Item.h"
#include "mcpe/item/ItemInstance.h"
#include "mcpe/item/ItemRenderer.h"
#include "mcpe/util/Random.h"

class Wrench : public Item
{
public:

	static const int WRENCH_ID = 322;

	Wrench(int id);
	
	int creativeCategory;
	
	virtual bool canBeDepleted();
	virtual bool isEmissive(int);
};
