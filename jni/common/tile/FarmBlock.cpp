#include "FarmBlock.h"

FarmBlock::FarmBlock(int id) : EntityTile(id, "log", &Material::stone) 
{
	this->setDescriptionId("farmBlock");
	this->setLightEmission(15);
	this->setDestroyTime(1.3);
	this->tileEntityType = TileEntityType::Farm;
}

int FarmBlock::getColor(TileSource*, int, int, int)
{
	return 0xffffff;
}

int FarmBlock::getColor(int idk)
{
	return 0xffffff;
}

int FarmBlock::getResource(int idk, Random* rand)
{
	return FBLOCK_ID;
}

int FarmBlock::getResourceCount(Random* rand)
{
	return 1;
}

void FarmBlock::onPlace(TileSource* ts, int x, int y, int z)
{
	//TODO
}

void FarmBlock::onRemove(TileSource* ts, int x, int y, int z)
{
	//TODO
}

bool FarmBlock::use(Player* player, int x, int y, int z)
{
	//TODO
}

void FarmBlock::attack(Player* player, int x, int y, int z)
{
	//TODO
}