#include "SolarPanel.h"
#include "entity/SolarEntity.h"
#include "../item/Wrench.h"
#include "Utils.h"

SolarPanel::SolarPanel(int id) : EntityTile(id, "planks", &Material::stone)
{
	this->setDescriptionId("solar_panel");
	this->setDestroyTime(0.5);
	this->tileEntityType = TileEntityType::SolarPanel;
}

int SolarPanel::getColor(TileSource* ts, int x, int y, int z)
{
		return 0xff763500;
}

int SolarPanel::getColor(int data)
{
		return 0xff763500;
}

int SolarPanel::getResource(int idk, Random* rand)
{
	return SOLAR_ID;
}

int SolarPanel::getResourceCount(Random* rand)
{
	return 1;
}

void SolarPanel::onRemove(TileSource* ts, int x, int y, int z)
{
	
}

bool SolarPanel::use(Player* player, int x, int y, int z)
{
	return false;
}

void SolarPanel::onPlace(TileSource* ts, int x, int y, int z){
	
}

void SolarPanel::attack(Player* player, int x, int y, int z)
{
	
}
