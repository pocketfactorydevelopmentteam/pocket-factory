#include "Wire.h"
#include "entity/WireEntity.h"
#include "../item/Wrench.h"
#include "Utils.h"

Wire::Wire(int id) : EntityTile(id, "iron_block", &Material::stone)
{
	this->setDescriptionId("wire");
	this->setDestroyTime(0.5);
	this->tileEntityType = TileEntityType::Wire;
}

int Wire::getColor(TileSource* ts, int x, int y, int z)
{
		return 0xff763500;
}

int Wire::getColor(int data)
{
		return 0xff763500;
}

int Wire::getResource(int idk, Random* rand)
{
	return WIRE_ID;
}

int Wire::getResourceCount(Random* rand)
{
	return 1;
}

void Wire::onRemove(TileSource* ts, int x, int y, int z)
{
	//DrillEntity* container = (DrillEntity*)ts->getTileEntity(x, y, z);
	//if(container == NULL)
		//return;
}

bool Wire::use(Player* player, int x, int y, int z)
{
	WireEntity* container = (WireEntity*)player->region->getTileEntity(x, y, z);
	
	return false;
}

void Wire::onPlace(TileSource* ts, int x, int y, int z){
	
}

void Wire::attack(Player* player, int x, int y, int z)
{
	//DrillEntity* container = (DrillEntity*)player->region->getTileEntity(x, y, z);
	//if(container == NULL)
	//	return;
}
