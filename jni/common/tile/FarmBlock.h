#ifndef FARMBLOCK_H_
#define FARMBLOCK_H_

#include "mcpe/Tile/EntityTile.h"
#include "mcpe/entity/player/Player.h"
#include "mcpe/level/TileSource.h"
#include "mcpe/util/Random.h"

class FarmBlock : public EntityTile
{
public:
	static const int FBLOCK_ID = 207;

	FarmBlock(int id);

	virtual bool use(Player*, int, int, int);
	virtual void attack(Player*, int, int, int);
	virtual void onPlace(TileSource*, int, int, int);
	virtual void onRemove(TileSource*, int, int, int);
	virtual int getColor(TileSource*, int, int, int);
	virtual int getColor(int);
    virtual int getResource(int, Random*); // 32
    virtual int getResourceCount(Random*); // 33
};


#endif