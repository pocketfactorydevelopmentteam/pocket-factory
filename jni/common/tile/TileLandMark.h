#pragma once

#include "../../Utils.h"
#include "mcpe/tile/Tile.h"
#include "mcpe/entity/player/Player.h"
#include "mcpe/level/TileSource.h"
#include "mcpe/util/Random.h"

class TileLandMark : public Tile
{
public:
	static const int MARK_ID = 202;
	
	TileLandMark(int id);
	
	virtual void onPlace(TileSource*, int, int, int);
	virtual bool use(Player*, int, int, int);
	virtual void attack(Player*, int, int, int);
	virtual void onRemove(TileSource*, int, int, int);
	virtual int getResource(int, Random*); // 32
	virtual int getResourceCount(Random*); // 33
	
	virtual bool renderLandMark(TileTessellator*, Tessellator*, TileSource*, const TilePos&);
};
