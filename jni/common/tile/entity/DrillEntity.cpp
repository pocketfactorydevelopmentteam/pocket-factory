#include "DrillEntity.h"
#include "../Drill.h"
#include "Utils.h"
#include "../Wire.h"
#include "WireEntity.h"

#include "mcpe/entity/item/ItemEntity.h"
#include "mcpe/nbt/CompoundTag.h"
#include "mcpe/nbt/IntTag.h"
#include "mcpe/nbt/StringTag.h"
#include "mcpe/nbt/ByteTag.h"

#include "mcpe/tile/entity/ChestTileEntity.h"
#include "mcpe/tile/entity/FurnaceTileEntity.h"

DrillEntity::DrillEntity(TilePos pos) : TileEntity(TileEntityType::Drill, pos, DRILL_STRING_ID)
{
	this->tile = Tile::tiles[Drill::DRILL_ID];
	this->rendererId = TileEntityRendererId::TR_DEFAULT_RENDERER;
	this->currentY  = pos.y;
	this->dtick = 0;
	this->isDrilling = false;
	this->buffer = 0;
	this->currentDelay = BASE_DELAY;
}

void DrillEntity::load(CompoundTag* compoundTag)
{
	this->isDrilling = ((ByteTag*)compoundTag->tags["isDrilling"])->data == 0x01 ? true : false;
	this->currentY = ((IntTag*)compoundTag->tags["currentY"])->data;
	this->dtick = ((IntTag*)compoundTag->tags["tick"])->data;
	this->buffer = ((IntTag*)compoundTag->tags["buffer"])->data;
	this->currentDelay = ((IntTag*)compoundTag->tags["delay"])->data;
	delete compoundTag;
}

bool DrillEntity::save(CompoundTag* compoundTag)
{
	TileEntity::save(compoundTag);
	compoundTag->putByte("isDrilling", (char)this->isDrilling);
	compoundTag->putInt("currentY", this->currentY);
	compoundTag->putInt("tick", this->dtick);
	compoundTag->putInt("buffer", this->buffer);
	compoundTag->putInt("delay", this->currentDelay);
	return true;
}


void DrillEntity::reset()
{
	this->isDrilling = false;
	this->currentY  = this->pos.y;
	this->dtick = 0;
	this->currentDelay = BASE_DELAY;
}

bool DrillEntity::addItem(TileEntity* tileEntity, ItemInstance* instance)
{
	if(existInVTable((((void***) tileEntity)[0]), 4 * 4, "_ZN15ChestTileEntity")) { // Hacky thing!!
		ChestTileEntity* cont = (ChestTileEntity*)(tileEntity);
		return cont->add(instance);
	} else if(existInVTable((((void***) tileEntity)[0]), 8, "_ZN17FurnaceTileEntity")) {
		FurnaceTileEntity* funace = (FurnaceTileEntity*)tileEntity;
		if(FurnaceTileEntity::isFuel(*instance))
		{
			if(funace->getItem(FurnaceTileEntity::SLOT_FUEL) == instance)
			{
				ItemInstance* fuel = funace->getItem(FurnaceTileEntity::SLOT_FUEL);
				if(fuel->count + instance->count > 64) {
					instance->count = (fuel->count + instance->count) - 64;
					funace->setItem(FurnaceTileEntity::SLOT_FUEL, new ItemInstance(instance->getId(), 64, instance->auxValue));
					this->region->getLevel().addEntity(new ItemEntity(*this->region, tileEntity->pos.x, tileEntity->pos.y, tileEntity->pos.z, *instance));
				} else {
					fuel->count += instance->count;
				}
			} else if(funace->getItem(FurnaceTileEntity::SLOT_FUEL) != NULL)
				return false;
			else if(funace->getItem(FurnaceTileEntity::SLOT_FUEL) == NULL)
				funace->setItem(FurnaceTileEntity::SLOT_FUEL, instance);
		} else if(funace->getItem(FurnaceTileEntity::SLOT_INGREDIENT) == instance) {
			ItemInstance* fuel = funace->getItem(FurnaceTileEntity::SLOT_INGREDIENT);
			if(fuel->count + instance->count > 64) {
				instance->count = (fuel->count + instance->count) - 64;
				funace->setItem(FurnaceTileEntity::SLOT_INGREDIENT, new ItemInstance(instance->getId(), 64, instance->auxValue));
				this->region->getLevel().addEntity(new ItemEntity(*this->region, tileEntity->pos.x, tileEntity->pos.y, tileEntity->pos.z, *instance));
			} else {
				fuel->count += instance->count;
			}
		} else if(funace->getItem(FurnaceTileEntity::SLOT_INGREDIENT) == NULL)
			funace->setItem(FurnaceTileEntity::SLOT_INGREDIENT, instance);
		else if(funace->getItem(FurnaceTileEntity::SLOT_INGREDIENT) != NULL)
			return false;
		
		return true;
	}
	return false;
}

void DrillEntity::dig(TileSource* ts)
{
	this->currentDelay = ((this->currentDelay >= 20) ? (this->currentDelay - MULTIPLIER * (this->getMaxEnergyStorageCapacity() / 2250)) : BASE_DELAY);
	
	Tile* tile = ts->getTilePtr(this->pos.x, --this->currentY, this->pos.z);
	if(tile == NULL){
		this->dig(ts);
		return;
	}
	else if(tile == Tile::tiles[7]) { // If hit bedrock. Set finished to true
		this->isDrilling = false;
		this->reset();
		return;
	}

	if(this->isDrilling && this->buffer > POWER_CONSUME)
	{
		Vec3 vec3;
		vec3.x = this->pos.x;
		vec3.y = this->pos.y + 0.3;
		vec3.z = this->pos.z;
		ts->getLevel().addParticle(ParticleType::Lava, vec3, 8, 8, 8, 25);
		ts->getLevel().addParticle(ParticleType::RedDust, vec3, 3, 3, 3, 1);
		this->buffer -= POWER_CONSUME;
		this->setChanged();
		DataID data = ts->getData(this->pos.x, this->currentY, this->pos.z);
		Vec3 vec3_2;
		vec3_2.x = this->pos.x;
		vec3_2.y = this->currentY;
		vec3_2.z = this->pos.z;
		ts->getTilePtr(TilePos(this->pos.x, this->currentY, this->pos.z))->destroyEffect(*ts, TilePos(this->pos.x, this->currentY, this->pos.z), vec3_2); // Why is this necessary?
		ts->getLevel().destroyTile(*ts, TilePos(this->pos.x, this->currentY, this->pos.z), false);
		ts->getLevel().playSound(this->pos.x, this->currentY, this->pos.z, tile->soundType->breakSound, tile->soundType->volume, tile->soundType->pitch);


		ItemInstance* instance = new ItemInstance(tile->getResource(0, &Entity::sharedRandom), tile->getResourceCount(&Entity::sharedRandom), data);
		TileEntity* tileEntity = NULL;
		bool added = false;

		if(((tileEntity = ts->getTileEntity(this->pos.x + 1, this->pos.y, this->pos.z))) != NULL)
			added = this->addItem(tileEntity, instance);
		else if((tileEntity = ts->getTileEntity(this->pos.x - 1, this->pos.y, this->pos.z)) != NULL)
			added = this->addItem(tileEntity, instance);
		else if((tileEntity = ts->getTileEntity(this->pos.x, this->pos.y + 1, this->pos.z)) != NULL)
			added = this->addItem(tileEntity, instance);
		else if((tileEntity = ts->getTileEntity(this->pos.x, this->pos.y, this->pos.z + 1)) != NULL)
			added = this->addItem(tileEntity, instance);
		else if((tileEntity = ts->getTileEntity(this->pos.x, this->pos.y, this->pos.z - 1)) != NULL)
			added = this->addItem(tileEntity, instance);

		if(!added)
			ts->getLevel().addEntity(new ItemEntity(*ts, this->pos.x, this->pos.y, this->pos.z, *instance));
	}
}

void DrillEntity::tick(TileSource* ts) {
	
	TileEntity::tick(ts);
	
	if(!this->isDrilling)
		return;
		
	if(this->dtick == currentDelay){
		this->dig(ts);
		this->dtick = 0;
	}
	this->dtick++;
}

int DrillEntity::getMaxEnergyStorageCapacity() const
{
	return DrillEntity::ENERGY_MAX_CAPACITY;
}

int DrillEntity::getEnergyInStorage() const
{
	return this->buffer;
}

//Return the amount of energy stored
int DrillEntity::receiveEnergy(int energy, bool simulate)
{
	if(simulate)
		return energy;
	
	this->buffer += energy;
	if(this->buffer >= DrillEntity::ENERGY_MAX_CAPACITY)
	{
		int overflow =  this->buffer - DrillEntity::ENERGY_MAX_CAPACITY;
		this->buffer -= overflow;
		return energy - overflow;
	}
	
	return energy;
}
