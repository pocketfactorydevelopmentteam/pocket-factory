#pragma once

#include "mcpe/Tile/entity/TileEntity.h"

#define WIRE_STRING_ID "Wire"
#define WIRE_CAPACITY 200

class WireEntity : public TileEntity
{
public:
	
	int dtick;
	
	WireEntity(TilePos);
			
	int buffer;
	
	virtual void increase(int);
	
	virtual bool canIncrease(int);
	
	virtual void decrease(int);
	
	virtual bool canDecrease(int);
	
	virtual int getMaxPowerStored();

	virtual void load(CompoundTag*);
	virtual bool save(CompoundTag*);
	virtual void tick(TileSource*);
	void reset();
	
	virtual void sendPower(TileSource*, int, int, int);
};
