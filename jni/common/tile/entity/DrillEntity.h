#pragma once

#include "mcpe/Tile/entity/TileEntity.h"
#include "../../energy/EnergyReceiver.h"

#define DRILL_STRING_ID "Drill"

class DrillEntity : public TileEntity, public EnergyReceiver
{
public:
	static const int ENERGY_MAX_CAPACITY = 5000;
	static const int POWER_CONSUME  = 36;
	static const int BASE_DELAY = 25;
	static const int MULTIPLIER = 3.5;

	bool isDrilling;
	bool finished;
	int currentY;
	int currentDelay;
	int dtick;
	int buffer;
	
public:
	DrillEntity(TilePos);

	virtual void load(CompoundTag*);
	virtual bool save(CompoundTag*);
	virtual void tick(TileSource*);
	void reset();
	
	virtual void dig(TileSource*);
	
	int getMaxEnergyStorageCapacity() const;
	int getEnergyInStorage() const;
	
	int receiveEnergy(int energy, bool simulate);
	
private:
	bool addItem(TileEntity*, ItemInstance*);
	virtual void sendPower(TileSource*, int, int, int);
};
