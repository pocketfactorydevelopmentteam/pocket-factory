#include "FarmEntity.h"

#include "../FarmBlock.h"
#include "Utils.h"

#include "mcpe/nbt/CompoundTag.h"
#include "mcpe/nbt/IntTag.h"
#include "mcpe/nbt/StringTag.h"
#include "mcpe/nbt/ByteTag.h"

FarmEntity::FarmEntity(const TilePos& pos) : TileEntity(TileEntityType::Farm, pos, FARM_STRING_ID)
{
	this->tile = Tile::tiles[FarmBlock::FBLOCK_ID];
	this->rendererId = TileEntityRendererId::TR_DEFAULT_RENDERER;
}

void FarmEntity::load(CompoundTag* compoundTag)
{
	//TODO
	delete compoundTag;
}

bool FarmEntity::save(CompoundTag* compoundTag)
{
	TileEntity::save(compoundTag);
	//TODO
	return true;
}