#pragma once

#include "mcpe/Tile/entity/TileEntity.h"

#define SOLAR_STRING_ID "SolarPanel"
#define SOLAR_PRODUCTION 30

class SolarEntity : public TileEntity
{
public:
	
	SolarEntity(TilePos);
	
	int dtick;
	
	virtual void load(CompoundTag*);
	virtual bool save(CompoundTag*);
	virtual void tick(TileSource*);
	
	virtual bool canPower(TileSource*);
	
	virtual void sendPower(TileSource*, int, int, int);
};
