#include "InfiniteEnergyEntity.h"
#include "../InfiniteEnergy.h"
#include "WireEntity.h"
#include "../Wire.h"
#include "DrillEntity.h"
#include "../Drill.h"
#include "Utils.h"

#include "mcpe/entity/item/ItemEntity.h"
#include "mcpe/nbt/CompoundTag.h"
#include "mcpe/nbt/IntTag.h"
#include "mcpe/nbt/StringTag.h"
#include "mcpe/nbt/ByteTag.h"

#include "mcpe/tile/entity/ChestTileEntity.h"
#include "mcpe/tile/entity/FurnaceTileEntity.h"

InfiniteEnergyEntity::InfiniteEnergyEntity(TilePos pos) : TileEntity(TileEntityType::InfiniteEnergy, pos, IE_STRING_ID)
{
	this->tile = Tile::tiles[InfiniteEnergy::IE_ID];
	this->rendererId = TileEntityRendererId::TR_DEFAULT_RENDERER;
}

void InfiniteEnergyEntity::load(CompoundTag* compoundTag)
{
	delete compoundTag;
}

bool InfiniteEnergyEntity::save(CompoundTag* compoundTag)
{
	TileEntity::save(compoundTag);
	return true;
}

void InfiniteEnergyEntity::tick(TileSource* ts) {
	
	TileEntity::tick(ts);

	this->sendPower(ts, this->pos.x, this->pos.y, this->pos.z);
}

void InfiniteEnergyEntity::sendPower(TileSource* ts, int x, int y, int z){
			int chooser = 1 + (rand() % (int)(6 - 1 + 1));
			int mx, my, mz;
			switch(chooser){
				case 1:
					mx = x + 1;
					my = y;
					mz = z;
					break;
				case 2:
					mx = x - 1;
					my = y;
					mz = z;
					break;
				case 3:
					mx = x;
					my = y + 1;
					mz = z;
					break;
				case 4:
					mx = x;
					my = y - 1;
					mz = z;
					break;
				case 5:
					mx = x;
					my = y;
					mz = z + 1;
					break;
				case 6:
					mx = x;
					my = y;
					mz = z - 1;
					break;
			}
			
			//wire

			if(ts->getTilePtr(mx, my, mz) == Tile::tiles[Wire::WIRE_ID]){
				WireEntity* wire = (WireEntity*)ts->getTileEntity(mx, my, mz);
				if(wire != NULL){
				for(int p = wire->getMaxPowerStored(); p >= 1; p--){
					if(wire->canIncrease(p)){
						wire->increase(p);
						break;
					}
				}
				}
			}
			
			//drill
			
				if(ts->getTilePtr(mx, my, mz) == Tile::tiles[Drill::DRILL_ID]){
				DrillEntity* drill = (DrillEntity*)ts->getTileEntity(mx, my, mz);
				if(drill != NULL){
					drill->receiveEnergy(drill->getMaxEnergyStorageCapacity(), false);
				}
			}
}
