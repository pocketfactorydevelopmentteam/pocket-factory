#include "InfiniteEnergyEntity.h"
#include "../InfiniteEnergy.h"
#include "WireEntity.h"
#include "../Wire.h"
#include "DrillEntity.h"
#include "../Drill.h"
#include "Utils.h"
#include "../SolarPanel.h"
#include "SolarEntity.h"

#include "mcpe/entity/item/ItemEntity.h"
#include "mcpe/nbt/CompoundTag.h"
#include "mcpe/nbt/IntTag.h"
#include "mcpe/nbt/StringTag.h"
#include "mcpe/nbt/ByteTag.h"

#include "mcpe/tile/entity/ChestTileEntity.h"
#include "mcpe/tile/entity/FurnaceTileEntity.h"

SolarEntity::SolarEntity(TilePos pos) : TileEntity(TileEntityType::SolarPanel, pos, SOLAR_STRING_ID)
{
	this->tile = Tile::tiles[SolarPanel::SOLAR_ID];
	this->rendererId = TileEntityRendererId::TR_DEFAULT_RENDERER;
	this->dtick = 0;
}

void SolarEntity::load(CompoundTag* compoundTag)
{
	this->dtick = ((IntTag*)compoundTag->tags["tick"])->data;
	delete compoundTag;
}

bool SolarEntity::save(CompoundTag* compoundTag)
{
	TileEntity::save(compoundTag);
	compoundTag->putInt("tick", this->dtick);
	return true;
}

void SolarEntity::tick(TileSource* ts) {
	
	TileEntity::tick(ts);
	
	if(dtick == 25){
	this->sendPower(ts, this->pos.x, this->pos.y, this->pos.z);
	dtick = 0;
	}
	dtick++;
}

bool SolarEntity::canPower(TileSource* ts){
	bool r = false;
	TilePos npos = this->pos;
	npos.y = this->pos.y + 1;
	return (ts->getBrightness(npos) >= 0.45);
}

void SolarEntity::sendPower(TileSource* ts, int x, int y, int z){
			int chooser = 1 + (rand() % (int)(6 - 1 + 1));
			int mx, my, mz;
			switch(chooser){
				case 1:
					mx = x + 1;
					my = y;
					mz = z;
					break;
				case 2:
					mx = x - 1;
					my = y;
					mz = z;
					break;
				case 3:
					mx = x;
					my = y + 1;
					mz = z;
					break;
				case 4:
					mx = x;
					my = y - 1;
					mz = z;
					break;
				case 5:
					mx = x;
					my = y;
					mz = z + 1;
					break;
				case 6:
					mx = x;
					my = y;
					mz = z - 1;
					break;
			}
			
			bool d1 = false;
			bool d2 = false;
			
			//wire

			if(ts->getTilePtr(mx, my, mz) == Tile::tiles[Wire::WIRE_ID]){
				WireEntity* wire = (WireEntity*)ts->getTileEntity(mx, my, mz);
				if(wire != NULL){
				for(int p = SOLAR_PRODUCTION; p >= 1 && this->canPower(ts) && !d1; p--){
					if(wire->canIncrease(p)){
						wire->increase(p);
						d1 = true;
						break;
					}
				}
				}
			}
			
			//drill
			
				if(ts->getTilePtr(mx, my, mz) == Tile::tiles[Drill::DRILL_ID]){
				DrillEntity* drill = (DrillEntity*)ts->getTileEntity(mx, my, mz);
				if(drill != NULL){
					drill->receiveEnergy(SOLAR_PRODUCTION, false);
				}
			}
}
