#ifndef FARMENTITY_H_
#define FARMENTITY_H_

#include "mcpe/Tile/entity/TileEntity.h"

#define FARM_STRING_ID "FarmBlock"

class FarmEntity : public TileEntity
{
public:

	FarmEntity(const TilePos&);

	virtual void load(CompoundTag*);
	virtual bool save(CompoundTag*);
};


#endif