#include "WireEntity.h"
#include "../Wire.h"
#include "DrillEntity.h"
#include "../Drill.h"
#include "Utils.h"

#include "mcpe/entity/item/ItemEntity.h"
#include "mcpe/nbt/CompoundTag.h"
#include "mcpe/nbt/IntTag.h"
#include "mcpe/nbt/StringTag.h"
#include "mcpe/nbt/ByteTag.h"

#include "mcpe/tile/entity/ChestTileEntity.h"
#include "mcpe/tile/entity/FurnaceTileEntity.h"

WireEntity::WireEntity(TilePos pos) : TileEntity(TileEntityType::Wire, pos, WIRE_STRING_ID)
{
	this->tile = Tile::tiles[Wire::WIRE_ID];
	this->rendererId = TileEntityRendererId::TR_DEFAULT_RENDERER;
	this->dtick = 0;
	this->buffer = 0;
}

void WireEntity::load(CompoundTag* compoundTag)
{
	this->dtick = ((IntTag*)compoundTag->tags["tick"])->data;
	this->buffer = ((IntTag*)compoundTag->tags["buffer"])->data;
	delete compoundTag;
}

bool WireEntity::save(CompoundTag* compoundTag)
{
	TileEntity::save(compoundTag);
	compoundTag->putInt("tick", this->dtick);
	compoundTag->putInt("buffer", this->buffer);
	return true;
}


void WireEntity::reset()
{
	this->dtick = 0;
}

void WireEntity::tick(TileSource* ts) {
	
	TileEntity::tick(ts);
	
	if(dtick == 15){
		this->sendPower(ts, this->pos.x, this->pos.y, this->pos.z);
		this->dtick = 0;
	}
	this->dtick++;
}

void WireEntity::increase(int a){
		this->buffer += a;
}
	
bool WireEntity::canIncrease(int q){
	return (this->buffer + q <= this->getMaxPowerStored());
}
	
void WireEntity::decrease(int a){
	this->buffer -= a;
}
	
bool WireEntity::canDecrease(int q){
	return (this->buffer - q >= 0);
}

int WireEntity::getMaxPowerStored(){
	return WIRE_CAPACITY;
}

void WireEntity::sendPower(TileSource* ts, int x, int y, int z){
			int chooser = 1 + (rand() % (int)(6 - 1 + 1));
			int mx, my, mz;
			switch(chooser){
				case 1:
					mx = x + 1;
					my = y;
					mz = z;
					break;
				case 2:
					mx = x - 1;
					my = y;
					mz = z;
					break;
				case 3:
					mx = x;
					my = y + 1;
					mz = z;
					break;
				case 4:
					mx = x;
					my = y - 1;
					mz = z;
					break;
				case 5:
					mx = x;
					my = y;
					mz = z + 1;
					break;
				case 6:
					mx = x;
					my = y;
					mz = z - 1;
					break;
			}
			
			bool d1 = false;
			bool d2 = false;
			
			//wire
			
			if(ts->getTilePtr(mx, my, mz) == Tile::tiles[Wire::WIRE_ID]){
				WireEntity* wire = (WireEntity*)ts->getTileEntity(mx, my, mz);
				if(wire != NULL && this->buffer != 0){
					for(int p = this->buffer; p >= 1 && !d1; p--){
						if(wire->canIncrease(p) && this->canDecrease(p)){
							this->decrease(p);
							wire->increase(p);
							d1 = true;
							break;
						}
					}
				}
			}
			
			//drill
			
				if(ts->getTilePtr(mx, my, mz) == Tile::tiles[Drill::DRILL_ID]){
				DrillEntity* drill = (DrillEntity*)ts->getTileEntity(mx, my, mz);
				if(drill != NULL && this->buffer != 0){
					for(int p = this->buffer; p >= 1 && !d2; p--){
						if(this->canDecrease(p)){
							this->decrease(p);
							int i = drill->receiveEnergy(p, false);
							if(i > 0)
								this->increase(i);
							d2 = true;
							break;
						}
					}
				}
			}
}
