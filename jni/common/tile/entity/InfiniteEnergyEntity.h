#pragma once

#include "mcpe/Tile/entity/TileEntity.h"

#define IE_STRING_ID "InfiniteEnergyEntityIdString"

class InfiniteEnergyEntity : public TileEntity
{
public:
	
	InfiniteEnergyEntity(TilePos);

	virtual void load(CompoundTag*);
	virtual bool save(CompoundTag*);
	virtual void tick(TileSource*);
	
	virtual void sendPower(TileSource*, int, int, int);
};
