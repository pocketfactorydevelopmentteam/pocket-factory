#include "TileLandMark.h"
#include "../item/Wrench.h"
#include "Utils.h"
#include "mcpe/texture/TextureTessellator.h"
#include "mcpe/texture/TextureData.h"

TileLandMark::TileLandMark(int id) : Tile(id, "planks", &Material::stone)
{
	this->setDescriptionId("land_mark");
	this->setDestroyTime(0.5);
	this->tileType = TileType::LandMark;
}

int TileLandMark::getResource(int idk, Random* rand)
{
	return MARK_ID;
}

int TileLandMark::getResourceCount(Random* rand)
{
	return 1;
}

void TileLandMark::onRemove(TileSource* ts, int x, int y, int z)
{
	
}

bool TileLandMark::use(Player* player, int x, int y, int z)
{
	return false;
}

void TileLandMark::onPlace(TileSource* ts, int x, int y, int z){
	
}

void TileLandMark::attack(Player* player, int x, int y, int z)
{
	
}

bool TileLandMark::renderLandMark(TileTessellator* tt, Tessellator* t, TileSource* ts, const TilePos& tp){
	TextureTessellator* tex = new TextureTessellator((*t));
	TextureData* td = new TextureData(0, 1);
	int x = tp.x;
	int y = tp.y;
	int z = tp.z;
	//below needs workaround
//	tex->tessellate(td, x, y, z, 0, false, false);
	return true;
}
