#include "Drill.h"
#include "entity/DrillEntity.h"
#include "../item/Wrench.h"
#include "Utils.h"

#include "../screen/DrillScreen.h"

Drill::Drill(int id) : EntityTile(id, "cobblestone", &Material::stone)
{
	this->setDescriptionId("drill");
	this->setDestroyTime(0.5);
	this->tileEntityType = TileEntityType::Drill;
}

int Drill::getColor(TileSource* ts, int x, int y, int z)
{
		return 0xff763500;
}

int Drill::getColor(int data)
{
		return 0xff763500;
}

int Drill::getResource(int idk, Random* rand)
{
	return DRILL_ID;
}

int Drill::getResourceCount(Random* rand)
{
	return 1;
}

void Drill::onRemove(TileSource* ts, int x, int y, int z)
{
	//DrillEntity* container = (DrillEntity*)ts->getTileEntity(x, y, z);
	//if(container == NULL)
		//return;
}

bool Drill::use(Player* player, int x, int y, int z)
{
	DrillEntity* container = (DrillEntity*)player->region->getTileEntity(x, y, z);
	if(player->getSelectedItem() != NULL && player->getSelectedItem()->getId() == Wrench::WRENCH_ID) {
		if(!container->isDrilling)
			container->reset();
			
		container->isDrilling = !container->isDrilling;
	}
	return false;
}

void Drill::onPlace(TileSource* ts, int x, int y, int z){
	
}

void Drill::attack(Player* player, int x, int y, int z)
{
	//DrillEntity* container = (DrillEntity*)player->region->getTileEntity(x, y, z);
	//if(container == NULL)
	//	return;
}
