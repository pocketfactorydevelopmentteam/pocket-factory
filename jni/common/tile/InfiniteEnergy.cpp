#include "InfiniteEnergy.h"
#include "entity/DrillEntity.h"
#include "../item/Wrench.h"
#include "Utils.h"

#include "../screen/DrillScreen.h"

InfiniteEnergy::InfiniteEnergy(int id) : EntityTile(id, "cauldron_side", &Material::stone)
{
	this->setDescriptionId("infinite_energy");
	this->setDestroyTime(0.5);
	this->tileEntityType = TileEntityType::InfiniteEnergy;
}

int InfiniteEnergy::getColor(TileSource* ts, int x, int y, int z)
{
		return 0xff763500;
}

int InfiniteEnergy::getColor(int data)
{
		return 0xff763500;
}

int InfiniteEnergy::getResource(int idk, Random* rand)
{
	return IE_ID;
}

int InfiniteEnergy::getResourceCount(Random* rand)
{
	return 1;
}

void InfiniteEnergy::onRemove(TileSource* ts, int x, int y, int z)
{
	
}

bool InfiniteEnergy::use(Player* player, int x, int y, int z)
{
	return false;
}

void InfiniteEnergy::onPlace(TileSource* ts, int x, int y, int z){
	
}

void InfiniteEnergy::attack(Player* player, int x, int y, int z)
{
	
}
