#pragma once
#include <string>
#include <memory>
#include "../CommonType.h"
#include <vector>
enum TextureFormat { TEXF_UNCOMPRESSED_8888, TEXF_UNCOMPRESSED_888, TEXF_UNCOMPRESSED_565, TEXF_UNCOMPRESSED_5551, TEXF_UNCOMPRESSED_4444, TEXF_COMPRESSED_PVRTC_565, TEXF_COMPRESSED_PVRTC_5551, TEXF_COMPRESSED_PVRTC_4444 };













struct ImageData {
	int w, h;
	std::string data;
	TextureFormat format;
	int mipLevel;

	ImageData(int);




	const unsigned char* getData() const;



	unsigned char* getDataModify();




	const char* getPixel(int, int) const;




	bool isValid();



	void _loadTexData() const;
};



class TextureData : public ImageData {


public:
	static TextureId lastBoundTexture[];
	static int lastBoundTextureUnit;
	bool bilinear;

	TextureData();
	TextureData(int, int);

	~TextureData();

	TextureData(const TextureData&);
	TextureData& operator=(const TextureData&);

	TextureData(TextureData&&);

	TextureData& operator=(TextureData&&);

	void bind(int) const;

	TextureId load();

	void loadMipmap(const ImageData&);

	TextureId getID() const;


	void sync();

	void unload();

	void clear();

	bool isLoaded() const;



protected:
	TextureId textureID;
	std::vector<ImageData, std::allocator<ImageData> > mipmaps;


	void _move(TextureData&);

	void _loadMipmap(const ImageData&);
};
