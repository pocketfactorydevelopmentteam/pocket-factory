#pragma once

enum class TextureFile : int
{
	TILES,
	ITEM
};

struct TextureUVCoordinateSet
{
	float _u0;
	float _v0;

	float _u1;
	float _v1;

	float _texSizeW;
	float _texSizeH;

	int _index;

	TextureFile _file;
};
