#pragma once
#include <atomic>
#include <string>
#include <memory>
#include "TextureData.h"
#include <functional>
#include <utility>
class Options;
class AppPlatform;
class TickingTexture;
#include <vector>
#include <array>
#include "../CommonType.h"
enum class DynamicTexture : int { BrightnessTexture, _count };




	typedef std::map<std::string, TextureData, std::less<std::basic_string<char> >, std::allocator<std::pair<const std::basic_string<char>, TextureData> > > TextureImageMap;





class Textures {

public:
	Textures(Options*, AppPlatform*);
	~Textures();

	void addTickingTexture(TickingTexture*);

	void loadList(const std::string&, bool);

	void loadDynamicTextures();

	TextureId loadTexture(const std::string&, bool, bool);
	TextureId bindTexture(const std::string&);
	void bindDynamicTexture(DynamicTexture, int);

	const TextureData& getTextureData(const std::string&);
	const TextureData& getDynamicTextureData(DynamicTexture) const;
	TextureData& getDynamicTextureDataModify(DynamicTexture);

	void uploadTexture(const std::string&, TextureData&);

	void tick(bool);

	void clear();

	void unloadAll();
	void reloadAll();

	void syncToBackgroundLoading();


	static bool isTextureIdValid(TextureId);

private:
	std::atomic<int> toLoadInBackground;

	TextureImageMap loadedImages;

	Options* options;
	AppPlatform* platform;

	std::vector<TickingTexture *, std::allocator<TickingTexture *> > tickingTextures;
	std::array<TextureData, 1> dynamicTextures;
};
