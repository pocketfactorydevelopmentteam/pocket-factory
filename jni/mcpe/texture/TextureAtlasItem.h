#pragma once
#include <string>
#include <memory>
#include "TextureUVCoordinateSet.h"
#include <vector>
#include <iterator>

class TextureAtlasTextureItem {


	typedef std::vector<TextureUVCoordinateSet, std::allocator<TextureUVCoordinateSet> > SetList;

public:
	TextureAtlasTextureItem();
	TextureAtlasTextureItem(const std::string&, const std::vector<TextureUVCoordinateSet, std::allocator<TextureUVCoordinateSet> >&);
	const std::string& getName() const;
	const TextureUVCoordinateSet& operator[](const int) const;
	int uvCount() const;

	std::vector<TextureUVCoordinateSet, std::allocator<TextureUVCoordinateSet> >::iterator begin();



	std::vector<TextureUVCoordinateSet, std::allocator<TextureUVCoordinateSet> >::iterator end();



protected:
	std::string _name;
	TextureAtlasTextureItem::SetList _textureUVs;
	int _uvCount;
};
