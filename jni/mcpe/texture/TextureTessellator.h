#pragma once
#include "../util/MemoryTracker.h"
#include "../phys/Vec3.h"
#include "../renderer/Color.h"
#include <memory>
#include <vector>
class Tessellator;
class TextureData;
#include "../renderer/Mesh.h"


class TextureTessellator : public MemoryTracker {




public:
	Vec3 inverseLightDirection;
	Color ambientColor;

	TextureTessellator();
	TextureTessellator(Tessellator&);

	void tessellate(TextureData&, int, int, int, int, bool, bool);

	Mesh end();

	virtual MemoryStats getStats() const;


protected:
	std::vector<bool, std::allocator<bool> > quads;
	Tessellator& t;

	void _addLighting(const TextureData&, const Vec3&, const unsigned char*, int, int, bool);
};
