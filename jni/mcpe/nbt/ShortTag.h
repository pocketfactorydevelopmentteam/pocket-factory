#pragma once
#include "Tag.h"
#include <string>
#include <memory>
class IDataOutput;
class IDataInput;




class ShortTag : public Tag {

public:
	short data;

	ShortTag(const std::string&);




	ShortTag(const std::string&, short);





	virtual void write(IDataOutput*);



	virtual void load(IDataInput*);



	virtual char getId() const;



	virtual std::string toString() const;






	virtual Tag* copy() const;




	virtual bool equals(const Tag&) const;
};
