#pragma once
#include "Tag.h"
#include <memory>
#include <vector>
#include <string>
class IDataOutput;
class IDataInput;
class PrintStream;

class ListTag : public Tag {


	typedef Tag* T;
	typedef std::vector<Tag *, std::allocator<Tag *> > List;

public:
	ListTag();




	ListTag(const std::string&);




	virtual void write(IDataOutput*);












	virtual void load(IDataInput*);











	virtual char getId() const;



	virtual std::string toString() const;





	virtual void print(const std::string&, PrintStream&) const;












	void add(ListTag::T);




	ListTag::T get(int) const;






	float getFloat(int);










	int size() const;




	virtual Tag* copy() const;












	virtual bool equals(const Tag&) const;













	virtual void deleteChildren();







private:
	ListTag::List list;
	char type;
};


class ListTagFloatAdder {
public:
	ListTagFloatAdder();


	ListTagFloatAdder(float);




	ListTagFloatAdder(ListTag*);



	ListTagFloatAdder& operator()(const std::string&, float);




	ListTagFloatAdder& operator()(float);



	ListTag* tag;
};
