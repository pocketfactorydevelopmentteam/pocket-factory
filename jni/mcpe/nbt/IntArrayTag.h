#pragma once
#include "Tag.h"
#include "ByteArrayTag.h"
#include <string>
#include <memory>
class IDataOutput;
class IDataInput;



class IntArrayTag : public Tag {


public:
	TagMemoryChunk data;

	IntArrayTag(const std::string&);




	IntArrayTag(const std::string&, TagMemoryChunk);





	virtual char getId() const;



	virtual std::string toString() const;






	virtual bool equals(const Tag&) const;











	virtual Tag* copy() const;






	virtual void write(IDataOutput*);







	virtual void load(IDataInput*);
};
