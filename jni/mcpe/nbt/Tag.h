#pragma once
#include <string>
#include <memory>
class IDataOutput;
class IDataInput;
class PrintStream;






class Tag {

public:
	virtual ~Tag();
	virtual void deleteChildren();

	static const char TAG_End = 0;
	static const char TAG_Byte = 1;
	static const char TAG_Short = 2;
	static const char TAG_Int = 3;
	static const char TAG_Long = 4;
	static const char TAG_Float = 5;
	static const char TAG_Double = 6;
	static const char TAG_Byte_Array = 7;
	static const char TAG_String = 8;
	static const char TAG_List = 9;
	static const char TAG_Compound = 10;
	static const char TAG_Int_Array = 11;

	static const std::string NullString;

	virtual void write(IDataOutput*) = 0;
	virtual void load(IDataInput*) = 0;

	virtual std::string toString() const = 0;
	virtual char getId() const = 0;

	virtual bool equals(const Tag&) const;

	virtual void print(PrintStream&) const;
	virtual void print(const std::string&, PrintStream&) const;

	virtual Tag* setName(const std::string&);
	virtual std::string getName() const;

	static Tag* readNamedTag(IDataInput*);
	static void writeNamedTag(Tag*, IDataOutput*);

	static Tag* newTag(char, const std::string&);

	static std::string getTagName(char);

	virtual Tag* copy() const = 0;

	int errorState;
	static const int TAGERR_OUT_OF_BOUNDS = 1;
	static const int TAGERR_BAD_TYPE = 2;
protected:
	Tag(const std::string&);
private:
	std::string name;
};
