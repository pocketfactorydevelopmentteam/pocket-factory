#pragma once
#include "Tag.h"
#include <string>
#include <memory>
#include <functional>
#include <utility>
#include <map>
class IDataOutput;
class IDataInput;
#include <vector>
#include "ByteArrayTag.h"
class ListTag;
class PrintStream;







class CompoundTag : public Tag {


	typedef std::map<std::basic_string<char>, Tag *, std::less<std::basic_string<char> >, std::allocator<std::pair<const std::basic_string<char>, Tag *> > > TagMap;
public:
	CompoundTag();


	CompoundTag(const std::string&);


	virtual ~CompoundTag();


	virtual void write(IDataOutput*);

	virtual void load(IDataInput*);


	virtual char getId() const;


	void put(const std::string&, Tag*);

	void putByte(const std::string&, char);

	void putShort(const std::string&, short);

	void putInt(const std::string&, int);

	void putLong(const std::string&, long long int);

	void putString(const std::string&, const std::string&);




	std::string getString(const std::string&) const;

	CompoundTag* getCompound(const std::string&) const;


	ListTag* getList(const std::string&) const;


	virtual std::string toString() const;


	virtual void print(const std::string&, PrintStream&) const;


	virtual Tag* copy() const;

	virtual bool equals(const Tag&) const;

	CompoundTag::TagMap tags;
};
