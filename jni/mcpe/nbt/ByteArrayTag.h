#pragma once
#include "Tag.h"
#include <string>
#include <memory>
class IDataOutput;
class IDataInput;




struct TagMemoryChunk {
	TagMemoryChunk();



	void* data;
	int len;
};

class ByteArrayTag : public Tag {


public:
	TagMemoryChunk data;

	ByteArrayTag(const std::string&);




	ByteArrayTag(const std::string&, TagMemoryChunk);





	virtual char getId() const;



	virtual std::string toString() const;






	virtual bool equals(const Tag&) const;











	virtual Tag* copy() const;






	virtual void write(IDataOutput*);




	virtual void load(IDataInput*);
};
