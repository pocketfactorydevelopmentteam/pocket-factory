#pragma once
#include "Tag.h"
#include <string>
#include <memory>
class IDataOutput;
class IDataInput;




class ByteTag : public Tag {


public:
	char data;

	ByteTag(const std::string&);



	ByteTag(const std::string&, char);





	virtual void write(IDataOutput*);



	virtual void load(IDataInput*);



	virtual char getId() const;



	virtual std::string toString() const;




	virtual bool equals(const Tag&) const;







	virtual Tag* copy() const;
};
