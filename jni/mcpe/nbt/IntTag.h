#pragma once
#include "Tag.h"
#include <string>
#include <memory>
class IDataOutput;
class IDataInput;



class IntTag : public Tag {

public:
	int data;

	IntTag(const std::string&);




	IntTag(const std::string&, int);





	virtual void write(IDataOutput*);



	virtual void load(IDataInput*);



	virtual char getId() const;



	virtual std::string toString() const;






	virtual Tag* copy() const;




	virtual bool equals(const Tag&) const;
};
