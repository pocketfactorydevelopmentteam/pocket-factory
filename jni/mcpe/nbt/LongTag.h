#pragma once
#include "Tag.h"
#include <string>
#include <memory>
class IDataOutput;
class IDataInput;

class LongTag : public Tag {

public:
	long long int data;

	LongTag(const std::string&);




	LongTag(const std::string&, long long int);





	virtual void write(IDataOutput*);



	virtual void load(IDataInput*);



	virtual char getId() const;



	virtual std::string toString() const;






	virtual Tag* copy() const;




	virtual bool equals(const Tag&) const;
};
