#pragma once
#include "Tag.h"
class IDataInput;
class IDataOutput;
#include <string>
#include <memory>

class EndTag : public Tag {

public:
	EndTag();




	virtual void load(IDataInput*);


	virtual void write(IDataOutput*);


	virtual char getId() const;



	virtual std::string toString() const;




	virtual Tag* copy() const;




	virtual bool equals(const Tag&) const;
};
