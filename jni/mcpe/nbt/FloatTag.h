#pragma once
#include "Tag.h"
#include <string>
#include <memory>
class IDataOutput;
class IDataInput;




class FloatTag : public Tag {

public:
	float data;

	FloatTag(const std::string&);




	FloatTag(const std::string&, float);





	virtual void write(IDataOutput*);



	virtual void load(IDataInput*);



	virtual char getId() const;



	virtual std::string toString() const;






	virtual Tag* copy() const;




	virtual bool equals(const Tag&) const;
};
