#pragma once
#include "Tag.h"
#include <string>
#include <memory>
class IDataOutput;
class IDataInput;


class StringTag : public Tag {


public:
	std::string data;
	int len;

	StringTag(const std::string&);





	StringTag(const std::string&, const std::string&);






	virtual void write(IDataOutput*);



	virtual void load(IDataInput*);



	virtual char getId() const;



	virtual std::string toString() const;






	virtual Tag* copy() const;




	virtual bool equals(const Tag&) const;
};
