#pragma once
class RestService;//#include "../../platform/http/RestService.h"
#include <memory>
class ThreadCollection;//#include "../../platform/threading/old/ThreadCollection.h"
#include "../mco/MCODataStructs.h"
#include <string>
#include "../mco/MCOParser.h"
class Minecraft;
#include <functional>
#include <utility>
#include <unordered_map>
#include "../../util/Random.h"
enum class MojangConnectionStatus : int { NOT_LOGGED_IN, LOGGING_IN, LOGGED_IN };











class MojangConnector {
public:
	MojangConnector(Minecraft*);
	std::shared_ptr<RestService> getAccountService();
	std::shared_ptr<RestService> getMCOService();
	std::shared_ptr<ThreadCollection> getThreadCollection();
	std::shared_ptr<MCOParser> getMCOParser();
	MojangConnectionStatus getConnectionStatus();
	void updateUIThread() const;
	void setMCOServerList(std::shared_ptr<std::unordered_map<long long, MCOServerListItem, std::hash<long long>, std::equal_to<long long>, std::allocator<std::pair<const long long, MCOServerListItem> > > >);
	std::shared_ptr<std::unordered_map<long long, MCOServerListItem, std::hash<long long>, std::equal_to<long long>, std::allocator<std::pair<const long long, MCOServerListItem> > > > getMCOServerList();
	bool isMCOCreateServersEnabled();
	void setMCOCreateServersEnabled(bool);
	std::shared_ptr<LoginInformation> getLoginInformation();
	void clearLoginInformation();
	void setLoginInformation(const LoginInformation&);
	std::string getEncryptedJoinDataString(long long int, const std::string&, const std::string&);
	void setPayload(const std::string&);
	const std::string& getJoinMCOPayload() const;
	std::string urlEncode(std::string) const;
	void setServerKey(const std::string&);
	const std::string& getServerKey();
	void setMCOServiceEnabled(bool);
	bool isServiceEnabled() const;




protected:
	void setStatus(MojangConnectionStatus);


	std::shared_ptr<RestService> _accountService;
	std::shared_ptr<RestService> _MCOService;
	std::shared_ptr<ThreadCollection> _threadCollection;
	std::shared_ptr<LoginInformation> _loginInformation;
	std::string _versionString;
	MojangConnectionStatus _connectionStatus;
	std::shared_ptr<MCOParser> _mcoParser;
	Minecraft* _minecraft;
	std::shared_ptr<std::unordered_map<long long, MCOServerListItem, std::hash<long long>, std::equal_to<long long>, std::allocator<std::pair<const long long, MCOServerListItem> > > > _mcoServerList;
	bool _mcoCreateServersEnabled;
	std::shared_ptr<Random> _random;
	std::string _joinMCOPayload;
	std::string _serverKey;
	bool _mcoServiceEnabled;
};
