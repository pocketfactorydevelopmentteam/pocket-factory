#pragma once
#include "../../platform/threading/old/Job.h"
#include <memory>
#include <string>
#include "../../platform/http/RestService.h"
#include "../mco/MCODataStructs.h"
class Minecraft;
#include "../../platform/threading/old/ThreadCollection.h"

enum class RestRequestType : int { GET, POST, PUT, DEL };










	typedef std::function<void (int, const std::basic_string<char> &, const RestCallTagData &, std::shared_ptr<RestRequestJob>)> RestSuccessResponse;

	typedef std::function<void (bool, bool, int, const std::basic_string<char> &, const RestCallTagData &, std::shared_ptr<RestRequestJob>)> RestFailedResponse;
	typedef std::shared_ptr<RestRequestJob> RestRequestJobPtr;
class RestRequestJob : public Job, public std::enable_shared_from_this<RestRequestJob> {
public:
	static RestRequestJobPtr CreateJob(RestRequestType, std::shared_ptr<RestService>, Minecraft*);
	virtual ~RestRequestJob();
	virtual void stop();
	static void launchRequest(RestRequestJobPtr, std::shared_ptr<ThreadCollection>, RestSuccessResponse, RestFailedResponse);


	void setBody(const std::string&);
	void setTagData(const RestCallTagData&);
protected:
	RestRequestJob();
	RestRequestJob(const RestRequestJob&);
	const RestRequestJob& operator=(const RestRequestJob&);


	virtual void run();
	virtual void finish();


	RestSuccessResponse _successFunc;
	RestFailedResponse _failedFunc;
	std::string _methodPath;
	std::string _httpBody;
	std::shared_ptr<RestService> _restService;
	RestRequestType _requestType;
	RestCallTagData _tag;



public:
	void setMethod<std::__1::basic_string<char>, std::__1::basic_string<char>, unsigned long>(const std::string&, std::basic_string<char>, std::basic_string<char>, long unsigned int);	void setMethod<long long, std::__1::basic_string<char>, unsigned long, std::__1::basic_string<char> >(const std::string&, long long int, std::basic_string<char>, long unsigned int, std::basic_string<char>);
};
