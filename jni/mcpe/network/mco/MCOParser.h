#pragma once
#include "MCODataStructs.h"
#include <functional>
#include <utility>
#include <memory>
#include <unordered_map>
#include <string>
class MCOParser {
public:
	std::shared_ptr<std::unordered_map<long long, MCOServerListItem, std::hash<long long>, std::equal_to<long long>, std::allocator<std::pair<const long long, MCOServerListItem> > > > parseServerList(const std::string&);
	void parseJoinWorld(const std::string&, std::string&, unsigned short&, std::string&);
	LoginInformation parseMCOAccountValidSessionReturnValue(const std::string&);
	void parseStatus(const std::string&, bool&, bool&, bool&);
	void parseErrorMessage(const std::string&, std::string&, int&);
};
