#pragma once
#include "MCODataStructs.h"
#include <mutex>
class RestCaller;
#include <functional>
#include <memory>
#include "MCOParser.h"
#include <string>
#include "../../util/Random.h"




class MCOConnector {
public:
	MCOConnector(std::Minecraft*);
	~MCOConnector();
	void setSessionInformation(LoginInformation);
	void setKey(const std::string&);
	void clearSessionInformation();

	void callUpdateServerList();
	void callJoinServer(long long int);
	void callCloseServer(long long int);
	void localSetServerOpen(long long int, bool);
	void callValidateSession();
	void callLogoutSession();
	void callGetStatus();
	void callVerifyPlayer(const std::string, const std::string, uint64_t);
	void callServerHeartbeat(unsigned short);
	int getServerListSize();
	MCOServerListItem getFirstServerListItem();
	std::string getCurrentServerIp();
	unsigned short getCurrentServerPort();
	MCOServerList getServerListCopy();

	void updateListSuccessful(int, std::string, const RestCallTagData&);
	void pingSuccessful(int, std::string, const RestCallTagData&);
	void joinServerSucessful(int, std::string, const RestCallTagData&);
	void joinServerFailed(bool, bool, int, std::string, const RestCallTagData&);
	void closeServerSucessful(int, std::string, const RestCallTagData&);
	void openServerSucessful(int, std::string, const RestCallTagData&);
	void changeServerNameSuccessful(int, std::string, const RestCallTagData&);
	void getStatusSucessful(int, std::string, const RestCallTagData&);

	void createWorldSucessful(int, std::string, const RestCallTagData&);
	void createWorldFailed(bool, bool, int, std::string, const RestCallTagData&);

	void resetWorldSucessful(int, std::string, const RestCallTagData&);
	void resetWorldFailed(bool, bool, int, std::string, const RestCallTagData&);

	void nonHandledSucessful(int, std::string, const RestCallTagData&);
	void nonHandledFailed(bool, bool, int, std::string, const RestCallTagData&);

	void sessionValidationSuccessful(int, std::string, const RestCallTagData&);
	void sessionValidationFailed(bool, bool, int, std::string, const RestCallTagData&);

	void logoutSessionSuccessful(int, std::string, const RestCallTagData&);
	void logoutSessionFailed(bool, bool, int, std::string, const RestCallTagData&);

	void verifyPlayerSuccessful(int, std::string, const RestCallTagData&);
	void verifyPlayerFailed(bool, bool, int, std::string, const RestCallTagData&);

	bool isConnectedToMCO();
	bool isWaitingForAccountService();
	std::string getUsername();
	void localSetServerName(long long int, const std::string&);
	void localRemoveFromInvite(long long int, const std::string&);
	void localAddFriendToServer(long long int, const std::string&);
	void callCreateServer(const std::string&, const std::string&, int);
	void callResetServer(long long int, const std::string&, const std::string&, int);
	bool hasFailedToConnectToServer();
	std::string getEncryptedJoinDataString(long long int, const std::string&, const std::string&);
	const std::string& getPayload();
	bool isCreateMCOServerEnabled();
	void setIgnoreJoin();
	void update();


	std::Minecraft* minecraft;
private:
	LoginInformation loginInformation;
	bool waitingForRequest;
	std::mutex serverListLock;
	std::CThread* asyncThread;
	RestCaller* statusCaller;
	RestCaller* updateCallerList;
	RestCaller* accountCaller;
	MCOServerList serverList;
	MCOParser parser;
	std::string currentServerIp;
	long long int currentServerId;
	unsigned short currentServerPort;
	bool waitingForAccountService;
	bool connectedToMCO;
	bool failedToConnectTOServer;
	bool buyServerEnabled;
	bool createServerEnabled;
	bool ignoreJoin;
	std::string encodedJoinKey;
	std::string payload;
	Random random;
};
