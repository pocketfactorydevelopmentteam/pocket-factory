#pragma once
#include <string>
#include <memory>

class Base64 {
public:
	static std::string base64Encode(const std::string&);
	static std::string base64Decode(const std::string&);
protected:
	static const char reverse_table[];
	static const char b64_table[];
};
