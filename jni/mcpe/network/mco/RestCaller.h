#pragma once
#include "MCODataStructs.h"
#include <string>
#include <memory>
#include <mutex>
class MCOConnector;


	typedef function * SuccessMethod;
	typedef function * FailedMethod;
struct RestCallerObject {
	std::RestCallerInterup interuptOp;
	RestCaller* caller;
	std::RestCallerType opterationType;
	std::string methodURI;
	std::string parameters;
	SuccessMethod successMethod;
	FailedMethod failedMethod;
	int timeoutBeforeStartMS;
	RestCallTagData tag;
};

class RestCaller {
public:
	static RestCaller* create(MCOConnector*, const std::string&);
	RestCaller(MCOConnector*, const std::string&);
	virtual ~RestCaller();

	virtual void requestStop() = 0;

	void get(std::RestCallerInterup, const char*, SuccessMethod, FailedMethod, const RestCallTagData&, int);
	void post(std::RestCallerInterup, const char*, const char*, SuccessMethod, FailedMethod, const RestCallTagData&, int);
	void put(std::RestCallerInterup, const char*, const char*, SuccessMethod, FailedMethod, const RestCallTagData&, int);
	void del(std::RestCallerInterup, const char*, SuccessMethod, FailedMethod, const RestCallTagData&, int);
	void call(std::RestCallerType, std::RestCallerInterup, const char*, const char*, SuccessMethod, FailedMethod, const RestCallTagData&, int);
	virtual std::string urlEscape(const std::string&);
	void setKey(const std::string&);
	void setSidAndUser(const std::string&, const std::string&);
	virtual void update();
	static void globalInit();
	static void globalCleanUp();
protected:
	virtual void makeRequest(RestCallerObject*) = 0;
	void addToCookieData(std::string&, const std::string&, const std::string&);
	std::string getCookieData();


	std::mutex mutex;
	std::string serviceUrl;
	MCOConnector* mcoConnector;
	std::string key;
	std::string userName;
	std::string sid;
};
