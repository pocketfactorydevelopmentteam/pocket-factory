#pragma once
#include <string>
#include <memory>
#include <vector>
#include <functional>
#include <set>
enum RestCallerInterup { RESTCALLER_TERMINATE, RESTCALLER_IGNORE, RESTCALLER_WAIT };






enum RestCallerType { RESTCALLER_GET, RESTCALLER_POST, RESTCALLER_PUT, RESTCALLER_DELETE, RESTCALLER_NONE };






struct RestCallTagData {
	uint64_t uIntData;
	std::string returnData;
};

enum MCOEventType {  };



class MCOEvent {
public:
	MCOEvent(const MCOEventType&);


	MCOEvent(const MCOEventType&, const RestCallTagData&);




	MCOEvent& operator=(const MCOEventType&);



	bool operator==(const MCOEventType&);


	MCOEventType operationType;
	RestCallTagData tag;
};

struct MCOServerListItem {
	long long int id;
	std::string name;
	bool open;
	std::string ownerName;
	bool myWorld;
	int mxNrPlayers;
	std::string gameType;
	std::vector<std::basic_string<char>, std::allocator<std::basic_string<char> > > playerNames;
	std::set<std::basic_string<char>, std::less<std::basic_string<char> >, std::allocator<std::basic_string<char> > > invitedNames;
	MCOServerListItem();
};



	typedef std::unordered_map<long long, MCOServerListItem, std::hash<long long>, std::equal_to<long long>, std::allocator<std::pair<const long long, MCOServerListItem> > > MCOServerList;








struct LoginInformation {
	std::string accessToken;
	std::string clientId;
	std::string profileId;
	std::string profileName;
	LoginInformation();
	LoginInformation(const std::string&, const std::string&, const std::string&, const std::string&);
};





	typedef std::shared_ptr<LoginInformation> LoginInformationPtr;
