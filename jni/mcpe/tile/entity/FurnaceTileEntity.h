#pragma once
#include "TileEntity.h"
#include "../../inventory/Container.h"
#include "../../item/ItemInstance.h"
class TilePos;
#include <string>
#include <memory>
class Player;
class CompoundTag;
class TileSource;


class FurnaceTileEntity : public TileEntity, public Container {



	static const int BURN_INTERVAL = 200;
	static const int NumItems = 3;
public:
	bool doNotRemove;

	FurnaceTileEntity(const TilePos&);
	virtual ~FurnaceTileEntity();


	virtual ItemInstance* getItem(int);
	virtual void setItem(int, ItemInstance*);
	virtual ItemInstance removeItem(int, int);

	virtual std::string getName() const;
	virtual int getMaxStackSize() const;
	virtual int getContainerSize() const;

	virtual bool stillValid(Player*);

	virtual void startOpen();
	virtual void stopOpen();

	void setContainerChanged();


	virtual void load(CompoundTag*);
	virtual bool save(CompoundTag*);

	int getBurnProgress(int);
	int getLitProgress(int);

	bool isLit();
	virtual bool isFinished();
	bool isSlotEmpty(int);

	virtual void tick(TileSource*);

	void burn();

	virtual void setRemoved();

	static bool isFuel(const ItemInstance&);
	static int getBurnDuration(const ItemInstance&);
private:
	bool canBurn();

public:
	int litTime;
	int litDuration;
	int tickCount;
	ItemInstance items[3];

	static const int SLOT_INGREDIENT = 0;
	static const int SLOT_FUEL = 1;
	static const int SLOT_RESULT = 2;
private:
	bool _canBeFinished;
	bool finished;
};
