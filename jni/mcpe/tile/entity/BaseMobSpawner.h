#pragma once
#include <memory>

#include "../../entity/Mob.h"
#include "../../util/WeighedRandom.h"

class CompoundTag;
class Level;
class TileSource;
class TilePos;

class SpawnData : public WeighedRandom::WeighedRandomItem {



public:
	SpawnData();
	SpawnData(CompoundTag*);
	SpawnData(int, int);
	SpawnData(int, int, CompoundTag*);

	CompoundTag* save();

	virtual ~SpawnData();




	int entityId;
	CompoundTag* tag;
};

	typedef std::vector<SpawnData, std::allocator<SpawnData> > SpawnDataList;






class BaseMobSpawner {


	static const int EVENT_SPAWN = 1;


public:
	BaseMobSpawner(int);
	virtual ~BaseMobSpawner();

	int getEntityId() const;
	void setEntityId(int);
	SpawnData* getNextSpawnData() const;
	void setNextSpawnData(SpawnData*);

	virtual void tick();

	virtual void load(CompoundTag*);
	virtual void save(CompoundTag*);

	bool isNearPlayer();
	void loadDataAndAddEntity(Mob*);

	Level* getLevel();
	virtual TileSource& getTileSource() = 0;
	virtual const TilePos& getPos() = 0;
	virtual void broadcastEvent(int) = 0;

	Mob* getDisplayEntity(TileSource&);
	bool onEventTriggered(int);

	int spawnDelay;
	float spin, oSpin;


protected:
	void delay();

	int entityId;
	SpawnDataList spawnPotentials;
	SpawnData* nextSpawnData;
	int minSpawnDelay;
	int maxSpawnDelay;
	int spawnCount;
	std::unique_ptr<Mob, std::default_delete<Mob> > displayEntity;
	int maxNearbyEntities;
	int requiredPlayerRange;
	int spawnRange;
};
