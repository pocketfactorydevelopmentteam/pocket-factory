#pragma once

enum class TileEntityType
{
	None,
	Furnace,
	Chest,
	NetherReactor,
	Sign,
	MobSpawner,
	SolarPanel,
	InfiniteEnergy = 9,
	Drill = 20,
	Farm = 21,
	Wire = 22
};


enum TileEntityRendererId
{
	TR_DEFAULT_RENDERER,
	TR_CHEST_RENDERER,
	TR_SIGN_RENDERER,
	TR_MOBSPAWNER_RENDERER
};
