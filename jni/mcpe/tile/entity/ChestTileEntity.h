#pragma once
#include "TileEntity.h"
#include "../../inventory/FillingContainer.h"
class Player;
class TilePos;
#include <string>
#include <memory>
class ItemInstance;
class CompoundTag;
class TileSource;
class Packet;




class ChestTileEntity : public TileEntity, public FillingContainer {




public:
	ChestTileEntity(const TilePos&);
	virtual ~ChestTileEntity();

	virtual int getContainerSize() const;
	virtual int getMaxStackSize() const;
	virtual std::string getName() const;

	virtual ItemInstance* getItem(int);
	virtual void setItem(int, ItemInstance*);



	virtual void load(CompoundTag*);
	virtual bool save(CompoundTag*);

	virtual bool stillValid(Player*);

	virtual void clearCache();


	virtual void tick(TileSource*);

	virtual void triggerEvent(int, int);

	bool canOpen();
	virtual void startOpen();
	virtual void stopOpen();


	virtual void setRemoved();

	bool canPairWith(TileEntity*);
	void pairWith(ChestTileEntity*, bool);
	void unpair();

	virtual Packet* getUpdatePacket();
	virtual void onUpdatePacket(CompoundTag*);

	virtual void onNeighborChanged(TileSource&, int, int, int);

	bool isLargeChest() const;



	ChestTileEntity* getPairedChest();



	bool isMainSubchest();



	float getModelOffsetX();

	void openBy(Player*);

	float openness, oOpenness;
	int openCount;

private:
	static const int ItemsSize = 27;
	int tickInterval;

	ChestTileEntity* largeChestPaired;
	bool pairLead;
	int deferredPairX, deferredPairZ;
	bool deferredPairLoad;
	bool alongX;

	int tickCountdownToOpen;
	Player* openingPlayer;

	bool _saveClientSideState(CompoundTag*);

	void _unpair();
	bool _canOpenThis();

	void _getCenter(float&, float&, float&);
};
