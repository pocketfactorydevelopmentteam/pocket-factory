#pragma once

#include <string>
#include <memory>

#include "TileEntity.h"
#include "../../util/Random.h"
#include "../../item/ItemInstance.h"
#include "../../phys/Vec3.h"
#include "../../CommonType.h"

class TilePos;
class TileSource;
class CompoundTag;
class Level;

class NetherReactorTileEntity : public TileEntity {

	static const int NUM_PIG_ZOMBIE_SLOTS = 3;
public:
	NetherReactorTileEntity(const TilePos&);
	void lightItUp(TileSource&, int, int, int);

	void buildDome(int, int, int);
	void clearDomeSpace(int, int, int);

	virtual void tick(TileSource*);

	void finishReactorRun(TileSource&);

	virtual bool save(CompoundTag*);
	virtual void load(CompoundTag*);
	int getNumEnemiesPerLevel(int);
	int getNumItemsPerLevel(int);
	void spawnItems(Level&, int);
	std::string getName() const;
	void spawnEnemy(Level&);
	void spawnItem(Level&);
	ItemInstance getSpawnItem();

	ItemInstance GetLowOddsSpawnItem();
	bool checkLevelChange(int);
	int numOfFreeEnemySlots();
	void trySpawnPigZombies(Level&, int, int);
	void tickGlowingRedstoneTransformation(int);
	void turnLayerToGlowingObsidian(int, const int);
	void turnGlowingObsidianLayerToObsidian(int);
	Vec3 getSpawnPosition(float, float, float);
	void buildHollowedVolume(int, int, int, int, int, const TileID, const TileID);
	void buildFloorVolume(int, int, int, int, int, const TileID);
	void buildCrockedRoofVolume(bool, int, int, int, int, int, const TileID);

	bool isEdge(int, int, int);
	void deterioateDome(int, int, int);
	void deterioateCrockedRoofVolume(bool, int, int, int, int, int, TileID);
	void deterioateHollowedVolume(int, int, int, int, int, TileID);
	bool playersAreCloseBy();
	void killPigZombies();
private:
	bool isInitialized;
	bool hasFinished;
	int curLevel;
	short progress;
	Random random;
};
