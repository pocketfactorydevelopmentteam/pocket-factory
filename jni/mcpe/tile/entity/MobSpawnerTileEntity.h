#pragma once

#include <memory>

#include "TileEntity.h"
#include "BaseMobSpawner.h"

class TilePos;
class CompoundTag;
class TileSource;


class MobSpawnerTileEntity : public TileEntity {




public:
	MobSpawnerTileEntity(const TilePos&);

	virtual void load(CompoundTag*);
	virtual bool save(CompoundTag*);

	virtual void tick(TileSource*);

	BaseMobSpawner& getSpawner();





protected:
	std::unique_ptr<BaseMobSpawner, std::default_delete<BaseMobSpawner> > spawner;
};


class DefaultMobSpawner : public BaseMobSpawner {


public:
	DefaultMobSpawner(MobSpawnerTileEntity*);
	virtual ~DefaultMobSpawner();

	virtual TileSource& getTileSource();
	virtual const TilePos& getPos();
	virtual void broadcastEvent(int);


protected:
	MobSpawnerTileEntity* owner;
};
