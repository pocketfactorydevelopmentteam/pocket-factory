#pragma once
#include "TileEntity.h"
#include <string>
#include <memory>
class TilePos;
class CompoundTag;
class Packet;
class TileSource;



class SignTileEntity : public TileEntity {


public:
	static const int MAX_LINE_LENGTH = 15;
	static const int NUM_LINES = 4;

	SignTileEntity(const TilePos&);


	virtual bool save(CompoundTag*);

	virtual void load(CompoundTag*);

	bool isEditable();
	void setEditable(bool);

	virtual Packet* getUpdatePacket();
	virtual void onUpdatePacket(CompoundTag*);

	std::string messages[4];
	int selectedLine;

	virtual float getShadowRadius(TileSource&) const;

private:
	bool editable;
};
