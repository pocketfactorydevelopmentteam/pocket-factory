#pragma once

#include "Tile.h"
#include "entity/TileEntity.h"

class EntityTile : public Tile
{
public:
	virtual TileEntity* newTileEntity(const TilePos&);

	virtual void neighborChanged(TileSource*, int, int, int, int, int, int);

	virtual void triggerEvent(TileSource*, int, int, int, int, int);

protected:
	EntityTile(int, const Material*);
	EntityTile(int, const std::string&, const Material*);
};
