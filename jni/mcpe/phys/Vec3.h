#pragma once

class Vec3{
public:
	static const Vec3 ZERO, ONE;

	float x;
	float y;
	float z;
};
