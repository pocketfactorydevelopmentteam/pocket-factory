#pragma once
#include "TilePos.h"
#include "Vec3.h"
class Entity;






enum HitResultType { TILE, ENTITY, NO_HIT };





class HitResult {

public:
	HitResult();
	HitResult(int, int, int, int, const Vec3&);
	HitResult(Entity*);
	HitResult(const HitResult&);

	float distanceTo(Entity*) const;

	bool isHit() const;

	HitResultType type;
	int f;
	TilePos tile;
	Vec3 pos;
	Entity* entity;
	bool indirectHit;
};
