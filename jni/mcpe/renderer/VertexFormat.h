#pragma once
#include "../util/ByteMask.h"
#include "VertexField.h"







class VertexFormat {

public:
	static const VertexFormat EMPTY;

	VertexFormat();





	void enableField(VertexField);

	bool hasField(VertexField) const;



	void* getFieldOffset(VertexField, void*) const;



	unsigned char getVertexSize() const;



	unsigned char getID() const;



	bool operator!=(const VertexFormat&) const;



protected:
	ByteMask fieldMask;

	unsigned char _fieldOffset[5];
	unsigned char vertexSize;

	static const unsigned char FieldSize[];
};
