#pragma once
#include "../CommonType.h"
#include "PrimitiveMode.h"
#include "VertexFormat.h"
#include "GLBuffer.h"
class MaterialPtr;



class Mesh {





public:
	unsigned int indexCount;
	Index vertexCount;
	PrimitiveMode mode;
	VertexFormat format;
	unsigned char indexSize;

	Mesh();

	Mesh(const VertexFormat&, Index, int, int, PrimitiveMode, byte*, bool, const char*);

	~Mesh();

	Mesh(Mesh&&);
	Mesh(const Mesh&);

	bool hasIndices() const;



	Mesh& operator=(Mesh&&);
	Mesh& operator=(const Mesh&&);

	bool isValid() const;



	void reset();


	void render(const MaterialPtr&, Index, Index) const;

private:
	GLBuffer glBuffer, indexBuffer;

	void* rawData;

	void _move(Mesh&);

	bool _load(byte*);

	GLBuffer _loadBuffer(int, byte*);

	const char* _getCreator();
};
