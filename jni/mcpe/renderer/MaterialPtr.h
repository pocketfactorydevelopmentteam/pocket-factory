#pragma once
class RenderMaterialGroup;
class RenderMaterial;
#include <string>
#include <memory>



class MaterialPtr {

public:
	static const MaterialPtr NONE;

	MaterialPtr();

	MaterialPtr(RenderMaterialGroup&, const std::string&);

	MaterialPtr(MaterialPtr&&);

	~MaterialPtr();

	MaterialPtr& operator=(MaterialPtr&&);


	MaterialPtr(const MaterialPtr&);
	MaterialPtr& operator=(const MaterialPtr&);






	RenderMaterial& operator*();









	RenderMaterial* operator->();





	void onGroupReloaded();

	void onGroupDestroyed();




protected:
	RenderMaterialGroup* group;
	RenderMaterial* ptr;
	std::string name;

	MaterialPtr& _move(MaterialPtr&&);

	void _deref();
};
