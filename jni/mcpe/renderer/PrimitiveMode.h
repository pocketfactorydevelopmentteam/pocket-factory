#pragma once


enum class PrimitiveMode : int { None, QuadList, TriangleList, TriangleStrip, TriangleFan, LineList, LineStrip };
