#pragma once
#include "../util/MemoryTracker.h"
#include "../CommonType.h"
#include <memory>
#include <vector>
#include <iterator>

class GLBuffer {

public:
class Pool : public MemoryTracker {



public:
	BufferId boundBuffers[2];

	void _bufferResized(const GLBuffer&);

	Pool(size_t);

	virtual ~Pool();


	void expandBy(int);


	GLBuffer get(int, const char*);

	void release(GLBuffer&);


	void clear();


	bool trim();

	void onAppResumed();

	virtual MemoryStats getStats() const;


	typedef std::vector<GLBuffer, std::allocator<GLBuffer> > FreeList;
protected:	GLBuffer::Pool::FreeList freeBuffers;





	void _localExpandBy(int);

	int reserveSize;

	std::vector<GLBuffer, std::allocator<GLBuffer> >::iterator _findPlaceInList(int);
	std::vector<GLBuffer, std::allocator<GLBuffer> >::iterator _findBestFit(int);
};



	static GLBuffer::Pool glBufferPool;
	static GLBuffer EMPTY;


	GLBuffer();





	GLBuffer(BufferId, int);





	GLBuffer(GLBuffer&&);







	GLBuffer& operator=(GLBuffer&&);






	int size() const;



	void bind(unsigned int) const;

	void release();






	void setSize(int);

protected:
	int byteSize;
	BufferId handle;
};
