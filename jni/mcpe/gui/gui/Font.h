#pragma once
class Tessellator;
#include "../../renderer/Mesh.h"
class TextureData;
#include <memory>
#include <vector>
#include "../../renderer/MaterialPtr.h"//class MaterialPtr;
#include "../../AppPlatformListener.h"
#include <string>
class Textures;
class Options;
#include <functional>
#include <utility>
#include <map>
class Color;

class Font : public AppPlatformListener {

public:
struct GlyphQuad {
	float x, y, size, s, ix, iy;

	void append(Tessellator&) const;
};


	Font(Options*, const std::string&, Textures*);

	virtual ~Font();

	void init(Options*);

	void draw(const std::string&, float, float, const Color&);
	void draw(const std::string&, float, float, const Color&, bool);
	void drawShadow(const std::string&, float, float, const Color&);
	void drawShadow(const std::string&, float, float, const Color&, const Color&);
	int drawWordWrap(const std::string&, float, float, float, const Color&, bool, bool);

	void drawTransformed(const std::string&, float, float, const Color&, float, float, bool, float);

	std::vector<std::vector<std::basic_string<char>, std::allocator<std::basic_string<char> > >, std::allocator<std::vector<std::basic_string<char>, std::allocator<std::basic_string<char> > > > > getParagraphs(const std::string&);

	int width(const std::string&);
	int height(const std::string&, int);

	float getPixelLength(const std::string&) const;

	const float* getCharWidths();



	void drawCached(const std::string&, float, float, const Color&, bool, MaterialPtr*);

	virtual void onAppSuspended();


private:
	bool containsUnicodeChar(const std::string&);
	void setDefaultTexture();
	void setUnicodeTexture(int);
	float _buildChar(std::vector<Font::GlyphQuad, std::allocator<Font::GlyphQuad> >&, int, float, float, bool);

	int charWidth(int, bool);
public:
	int fontTexture;
	int lineHeight;
	static const int DefaultLineHeight = 10;
private:
	int charWidths[256];
	float fcharWidths[256];
	std::string unicodeWidth;
	int listPos;

	int index;
	int count;

	std::string fontName;
	Textures* _textures;
	MaterialPtr fontMat;

	Options* options;
public:
struct TextObject {

struct Page {
	Mesh mesh;
	const TextureData* texture;

	Page();



	Page(Font::TextObject::Page&&);





	Font::TextObject::Page& operator=(Font::TextObject::Page&&);
};






	std::vector<Font::TextObject::Page, std::allocator<Font::TextObject::Page> > pages;

	void render(const MaterialPtr&) const;

	TextObject();

	TextObject(Font::TextObject&&);





	Font::TextObject& operator=(Font::TextObject&&);




	TextObject(const Font::TextObject&);
	Font::TextObject& operator=(const Font::TextObject&);
};

private:
	std::map<std::basic_string<char>, Font::TextObject, std::less<std::basic_string<char> >, std::allocator<std::pair<const std::basic_string<char>, Font::TextObject> > > stringCache;

	int _x, _y;
	int _cols;
	int _rows;
	unsigned char _charOffset;
	bool enforceUnicodeSheet;

	Font::TextObject _makeTextObject(const std::string&);
};
