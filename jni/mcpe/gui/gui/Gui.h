#pragma once
#include "GuiComponent.h"
#include "../../game/IConfigListener.h"
#include "../../AppPlatformListener.h"
#include <string>
#include <memory>
#include "GuiMessage.h"
#include "../../util/Random.h"
class MinecraftClient;
class Font;
#include "../../renderer/Mesh.h"
#include "../components/ControllerButtonRenderer.h"
#include "../../renderer/MaterialPtr.h"
#include "../../input/TouchAreaModel.h"
class ItemInstance;







class Gui : public GuiComponent, private IConfigListener, private AppPlatformListener {




public:
	static const int TIP_DURATION_TICKS = 40;

	Gui(MinecraftClient&);
	virtual ~Gui();

	static const int MAX_NUM_CHATLINES = 10;
	int getSlotIdAt(int, int);
	void flashSlot(int);
	bool isInside(int, int);
	RectangleArea getRectangleArea(int);
	void getSlotPos(int, int&, int&);
	int getNumSlots();

	void handleClick(int, int, int);
	void handleKeyPressed(int);
	void handleControllerPressed();

	void tick();
	void render(float, bool, int, int);

	void renderToolBar(float, float);

	void renderChatMessages(const int, const int, unsigned int, bool, Font*);

	void renderOnSelectItemNameText(const int, Font*, int);

	void renderSleepAnimation(const int, const int);

	void renderBubbles();
	void renderHearts();

	void renderProgressIndicator(const bool, const int, const int, float);

	void addMessage(const std::string&, const std::string&, int);
	void clearMessages();
	void postError(int);

	virtual void onAppSuspended();

	void inventoryUpdated();

	void showTipMessage(const std::string&);

	void setNowPlaying(const std::string&);
	void displayClientMessage(const std::string&);
	void renderSlotText(const ItemInstance*, float, float, bool, bool, bool);

	const GuiMessageList& getMessageList();
	void setIsChatting(bool);

	virtual void onConfigChanged(const Config&);
	void onLevelGenerated();

	static float floorAlignToScreenPixel(float);
	static int itemCountItoa(char*, int);

	void showPopupNotice(const std::string&);
private:
	void renderVignette(float, int, int);
	void renderSlot(int, int, int, float);
	void tickItemDrop();
	float cubeSmoothStep(float, float, float);
	void processLeftShoulder(int);
	void processRightShoulder(int);
public:
	float progress;
	std::string selectedName;
	static float InvGuiScale;
	static float GuiScale;

private:
	std::string lastPopupText;
	int maxMessageWidth;

	GuiMessageList guiMessages;
	Random random;
public:
	MinecraftClient& minecraft;
private:
	int tickCount;
	float itemNameOverlayTime;
	std::string overlayMessageString;
	int overlayMessageTime;
	bool animateOverlayMessageColor;

	float tbr;

	bool _inventoryNeedsUpdate;

	int _flashSlotId;
	double _flashSlotStartTime;

	Font* _slotFont;
	int _numSlots;

	Mesh rcFeedbackOuter, rcFeedbackInner, vignette;
	float lastVignetteOpacity;



	static const float DropTicks;
	float _currentDropTicks;
	int _currentDropSlot;

	std::string tipMessage;
	float tipMessageTime;
	float tipMessageLength;

	bool isChatting;

	std::unique_ptr<ControllerButtonRenderer, std::default_delete<ControllerButtonRenderer> > cbr;

	MaterialPtr invFillMat, crosshairMat;
};
