#pragma once
#include <string>
#include <memory>
class Color;



struct GuiMessage {


	GuiMessage(const std::string&, const std::string&, int);











	int ticks;
	int maxTicks;

	bool isCommand() const;



	const std::string& getString() const;



	const std::string& getUser() const;



	const std::string& getMessage() const;



	const Color& getTextColor() const;



protected:
	std::string message, username, fullString;
};

	typedef std::vector<GuiMessage, std::allocator<GuiMessage> > GuiMessageList;
