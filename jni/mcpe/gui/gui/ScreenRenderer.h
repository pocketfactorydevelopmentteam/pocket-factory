#pragma once
#include <memory>
#include "../../renderer/MaterialPtr.h"
class Color;
class Font;
#include <string>
struct IntRectangle;




class ScreenRenderer {

	static std::unique_ptr<ScreenRenderer, std::default_delete<ScreenRenderer> > singletonPtr;

public:
	static ScreenRenderer& singleton();

	ScreenRenderer();

	void fill(int, int, int, int, const Color&);
	void fill(float, float, float, float, const Color&);
	void fillGradient(int, int, int, int, const Color&, const Color&);
	void fillGradient(float, float, float, float, const Color&, const Color&);
	void fillHorizontalGradient(int, int, int, int, const Color&, const Color&);
	void fillHorizontalGradient(float, float, float, float, const Color&, const Color&);
	void drawRect(int, int, int, int, const Color&, int);
	void drawRect(int, int, int, int, const Color&, const Color&, const Color&, const Color&, int);

	void drawString(Font*, const std::string&, int, int, const Color&);
	void drawCenteredString(Font*, const std::string&, int, int, const Color&);

	void blit(const IntRectangle&);
	void blit(int, int, int, int, int, int, int, int, MaterialPtr*);
	void blit(float, float, int, int, float, float, int, int);
	void blitRaw(float, float, float, float, float, float, float, float, float);


	MaterialPtr blitMat, fillMat, fillGradientMat, colorBlitMat, blitGLColorMat;
};
