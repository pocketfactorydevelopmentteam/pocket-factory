#pragma once










class PixelCalc {

public:
	PixelCalc(float);



	float millimetersToPixels(float) const;


	float pixelsToMillimeters(float) const;



	void setPixelsPerMillimeter(float);




private:
	float _pixelsPerMillimeter;
	float _millimetersPerPixel;
};
