#pragma once
#include "Screen.h"
#include "../../../world/level/LevelListener.h"
class Button;
class Label;
class PackedScrollContainer;
class Entity;
#include "../../../platform/input/Controller.h"
class Player;
#include <memory>
#include <functional>
namespace __1 { class unordered_set<Player *, std::__1::hash<Player *>, std::__1::equal_to<Player *>, std::__1::allocator<Player *> >; };

class PauseScreen : public Screen, public LevelListener {




public:
	PauseScreen(bool);
	virtual ~PauseScreen();

	virtual void init();
	virtual void setupPositions();

	virtual void tick();
	virtual void render(int, int, float);

	virtual void onEntityAdded(Entity&);
	virtual void onEntityRemoved(Entity&);

	virtual bool handleBackEvent(bool);

protected:
	virtual void buttonClicked(Button*);

	virtual bool renderGameBehind();



	virtual void controllerDirectionChanged(int, Controller::StickDirection);
private:
	void rebuildPlayerList(PlayerList&);
	int saveStep;
	int visibleTime;
	bool wasBackPaused;

	Button* bContinue;
	Button* bQuit;
	Button* bQuitAndSaveLocally;
	Button* bOptions;
	Label* lTitleHeader;
	PackedScrollContainer* playerList;
};
