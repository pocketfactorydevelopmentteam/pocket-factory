#pragma once
#include "BaseContainerScreen.h"
#include "../components/InventoryPane.h"
#include <string>
#include <memory>
#include "../components/ImageButton.h"
#include "../components/Button.h"
class ItemInstance;
#include <vector>
class NinePatchLayer;
class Player;
class ChestTileEntity;
#include "../../renderer/renderer/Color.h"
#include "../../../platform/input/Controller.h"
class Tessellator;
class FillingContainer;
namespace Touch { class InventoryPane; };
struct FlyingItem {
	ItemInstance item;
	double startTime;
	float sx, sy;
	float dx, dy;
	Touch::InventoryPane* toPane;
};	typedef FlyingItem FlyingItem;

class ChestScreen : public BaseContainerScreen, public Touch::IInventoryPaneCallback {





public:
	ChestScreen(Player*, ChestTileEntity*);
	virtual ~ChestScreen();

	virtual void init();
	virtual void setupPositions();

	virtual void tick();
	virtual void render(int, int, float);
	virtual bool renderGameBehind();
	virtual void buttonClicked(Button*);


	virtual bool addItem(const Touch::InventoryPane*, int);
	virtual bool isAllowed(int);
	virtual std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > getItems(const Touch::InventoryPane*);

	virtual void onInternetUpdate();

	virtual bool handleBackEvent(bool);



protected:
	virtual void controllerDirectionChanged(int, Controller::StickDirection);
	virtual void controllerDirectionHeld(int, Controller::StickDirection);
	virtual bool shouldSendAllKeyStates();
	virtual void keyPressed(int);

private:
	void setupPane();

	void drawSlotItemAt(Tessellator&, const ItemInstance*, int, int, bool);
	bool handleAddItem(FillingContainer*, FillingContainer*, int);
	void handleRenderPane(Touch::InventoryPane*, Tessellator&, int, int, float);
	void handleBulkItemMovementRequest(Touch::InventoryPane*);

	void updateSelectedIndexes(Controller::StickDirection);

	std::string currentItemDesc;
	ImageButton btnClose;
	Touch::THeader bHeader;
	Touch::THeader bHeaderChest;

	Touch::InventoryPane* inventoryPane;
	Touch::InventoryPane* chestPane;
	IntRectangle panesBbox;

	std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > inventoryItems;
	std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > chestItems;
	bool doRecreatePane;

	int playerSelectedSlot;
	int chestSelectedSlot;
	bool playerPaneActive;


	NinePatchLayer* guiBackground;
	NinePatchLayer* guiSlot;
	NinePatchLayer* guiSlotMarked;
	NinePatchLayer* guiSlotMarker;
	NinePatchLayer* guiPaneFrame;
	Player* player;
	ChestTileEntity* chest;
	bool initilized;

	std::vector<FlyingItem, std::allocator<FlyingItem> > flyingItems;

	const int borderX, borderY;

	const int descFrameWidth;


	const Color rgbActive;
	const Color rgbInactive;
	const Color rgbInactiveShadow;


	const float BorderPixels;



	const float BlockPixels;






	const int ItemSize;
};
