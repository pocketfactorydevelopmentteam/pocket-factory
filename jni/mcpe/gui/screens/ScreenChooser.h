#pragma once
class MinecraftClient;
class Screen;
enum ScreenId { SCREEN_NONE, SCREEN_STARTMENU, SCREEN_PAUSE, SCREEN_PAUSEPREV, SCREEN_BLOCKSELECTION, SCREEN_CHAT, SCREEN_CONSOLE };












class ScreenChooser {

public:
	ScreenChooser(MinecraftClient*);



	Screen* createScreen(ScreenId);
	Screen* setScreen(ScreenId);
private:
	MinecraftClient* _mc;
};
