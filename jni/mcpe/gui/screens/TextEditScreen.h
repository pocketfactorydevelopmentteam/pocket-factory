#pragma once
#include "Screen.h"
#include "../../../world/level/TileSourceListener.h"
#include "../../../world/level/TilePos.h"
#include "../components/ImageButton.h"
class SignTileEntity;
#include "../../../world/level/tile/entity/TileEntity.h"
#include <memory>
#include <string>
class Button;
class TileSource;


class TextEditScreen : public Screen, public TileSourceListener {




public:
	TextEditScreen(SignTileEntity*);
	virtual ~TextEditScreen();
	virtual void init();
	virtual void tick();
	virtual bool handleBackEvent(bool);
	virtual void render(int, int, float);

	virtual void keyPressed(int);
	virtual void keyboardNewChar(const std::string&, bool);
	virtual void setupPositions();
	virtual void buttonClicked(Button*);
	void failedToFindSignEntity();
	virtual void setTextboxText(const std::string&);

	virtual void onTileEntityRemoved(TileSource&, std::unique_ptr<TileEntity, std::default_delete<TileEntity> >&);

protected:
	bool isShowingKeyboard;
	TilePos signPos;
	int frame;
	int line;
private:
	ImageButton btnClose;
	SignTileEntity* sign;
	std::unique_ptr<TileEntity, std::default_delete<TileEntity> > pendingDeleteSign;
};
