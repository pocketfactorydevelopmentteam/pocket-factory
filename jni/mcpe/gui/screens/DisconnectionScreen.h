#pragma once
#include "Screen.h"
#include <string>
#include <memory>
class Button;




class DisconnectionScreen : public Screen {


public:
	DisconnectionScreen(const std::string&);




	virtual ~DisconnectionScreen();



	virtual void init();















	virtual void render(int, int, float);










	virtual void buttonClicked(Button*);




	virtual bool isInGameScreen();

private:
	std::string _msg;
	Button* _back;
};
