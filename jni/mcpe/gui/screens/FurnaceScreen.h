#pragma once
#include "BaseContainerScreen.h"
#include "../components/InventoryPane.h"
class CItem;
#include <memory>
#include <vector>
#include <string>
#include "../../../world/item/ItemInstance.h"
#include "../components/ImageButton.h"
#include "../components/Button.h"
class NinePatchLayer;
class Player;
class FurnaceTileEntity;
#include "../../renderer/renderer/Color.h"
#include "../../../platform/input/Controller.h"
class Tessellator;
class FurnaceScreen : public BaseContainerScreen, public Touch::IInventoryPaneCallback {



	typedef std::vector<CItem *, std::allocator<CItem *> > ItemList;
public:
	FurnaceScreen(Player*, FurnaceTileEntity*);
	virtual ~FurnaceScreen();

	virtual void init();
	virtual void setupPositions();

	virtual void tick();
	virtual void render(int, int, float);
	virtual bool renderGameBehind();
	virtual void buttonClicked(Button*);


	virtual bool addItem(const Touch::InventoryPane*, int);
	virtual bool isAllowed(int);
	virtual std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > getItems(const Touch::InventoryPane*);

protected:
	virtual void keyPressed(int);
	virtual void controllerDirectionChanged(int, Controller::StickDirection);
	virtual void controllerDirectionHeld(int, Controller::StickDirection);
	virtual bool shouldSendAllKeyStates();


private:
	void recheckRecipes();

	void clearItems();
	void updateResult(const ItemInstance*);
	void handleDualNavigation(int, Controller::StickDirection);
	void setupInventoryPane();
	void updateItems();

	void drawSlotItemAt(Tessellator&, const ItemInstance*, int, int, bool);
	ItemInstance moveOver(const ItemInstance*, int);
	void takeAndClearSlot(int);
	bool handleAddItem(int, const ItemInstance*);
	void handleBulkItemMovementRequest(Touch::InventoryPane*);
	void handleRenderPane(Touch::InventoryPane*, Tessellator&, int, int, float);
	bool canMoveToFurnace(int, const ItemInstance*);
	void setFuelIngredientSelectedTile(Controller::StickDirection);
	FurnaceScreen::ItemList _items;

	std::string currentItemDesc;
	ItemInstance burnResult;
	float descWidth;
	ImageButton btnClose;
	BlankButton btnIngredient;
	BlankButton btnFuel;
	BlankButton btnResult;
	Touch::THeader bHeader;

	Touch::InventoryPane* inventoryPane;
	IntRectangle inventoryPaneRect;

	FurnaceScreen::ItemList listFuel;
	FurnaceScreen::ItemList listIngredient;
	std::vector<int, std::allocator<int> > inventorySlots;
	std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > inventoryItems;
	std::vector<BlankButton *, std::allocator<BlankButton *> > ingredientResultButtons;
	bool doRecreatePane;

	int selectedSlot;
	int lastBurnTypeId;
	int ingredientResultIndex;


	NinePatchLayer* guiBackground;
	NinePatchLayer* guiSlot;
	NinePatchLayer* guiSlotMarked;
	NinePatchLayer* guiSlotMarker;
	NinePatchLayer* guiPaneFrame;
	Player* player;
	FurnaceTileEntity* furnace;
	bool initilized;

	int inventorySelectedSlot;
	int furnaceSelectedSlot;
	bool furnacePaneActive;

	const int borderX, borderY;

	int heldMs;
	int percent;
	int downMs;
	const float MaxHoldMs;
	const int MinChargeMs;

	static void setIfNotSet(bool&, bool);



	const int descFrameWidth;


	const Color rgbActive;
	const Color rgbInactive;
	const Color rgbInactiveShadow;


	const float BorderPixels;



	const float BlockPixels;






	const int ItemSize;
};
