#pragma once
#include "Screen.h"
#include <string>
#include <memory>
#include <deque>
class Button;
class ImageWithBackground;
#include "../gui/GuiMessage.h"



class ChatScreen : public Screen {



	static std::deque<std::basic_string<char>, std::allocator<std::basic_string<char> > > sentMessages;

public:
	static const int MAX_SAVED_MESSAGES = 100;

	static ChatScreen instance;

	static const int MAX_CHAT_MESSAGE_LENGTH = 160;
	ChatScreen(bool);
	virtual ~ChatScreen();

	virtual void init();

	virtual void setupPositions();
	virtual void removed();

	virtual void render(int, int, float);

	virtual void tick();

	virtual void buttonClicked(Button*);

	virtual bool handleBackEvent(bool);

	void closeWindow();

	virtual void keyPressed(int);
	virtual void keyboardNewChar(const std::string&, bool);

	virtual bool renderGameBehind();
	virtual bool isPauseScreen();
	virtual bool isErrorScreen();
	virtual bool isInGameScreen();
	virtual bool closeOnPlayerHurt();
	void updateKeyboardVisibility();
	void updateToggleKeyboardButton();
	void sendChatMessage();
	bool guiMessagesUpdated();
	void updateGuiMessages();
	void drawChatMessages(int);
	virtual void setTextboxText(const std::string&);
private:
	std::string currentMessage;
	bool keyboardVisible;
	bool isImeEditing;

	Button* btnClose;
	Button* btnDeactiviatedWriteArea;
	ImageWithBackground* btnToggleKeyboard;
	ImageWithBackground* btnSendMessage;
	GuiMessageList currentGuiMessages;

	int currentSentMessageIdx;
};
