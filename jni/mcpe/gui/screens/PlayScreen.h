#pragma once
#include "Screen.h"
#include "../../../network/mco/MCODataStructs.h"
#include <functional>
#include <utility>
#include <memory>
#include <unordered_map>
#include <string>
namespace Touch { class THeader; };
namespace Touch { class TButton; };
class NinePatchLayer;
class Spinner;
class ImageButton;
#include "../../../network/RakNetInstance.h"
#include "../../../network/http/RestRequestJob.h"
class Button;
#include "../components/GuiElement.h"
#include "../../../network/http/MojangConnector.h"
#include "../../../platform/input/Controller.h"
#include "../components/PackedScrollContainer.h"
enum class PlayScreenState : int { Uninitialized, RealmsNotLoggedIn, RealmsWaitingForAccount, RealmsReadyCreateServer, RealmsReadyCreateServerEmptyList, RealmsReadyEditServers, RealmsReadyNoServerSlots, RealmsReadyNoServerSlotsEmptyList, RealmsWrongVersion, RealmsJoiningServer, RealmsJoinFailed, LocalPlayer, LocalPlayerEditMode, num_values };
















enum class PlayScreenPanel : int { Uninitialized, Message, LocalServerList, RealmsServerList, JoiningRealmsServer, FailedToJoinRealmsServer };








struct PlayScreenStateSettings {
	bool _editServerVisible;
	bool _editServerEditMode;
	bool _showLogout;
	bool _showLogin;
	bool _canCreateServer;
	bool _showAddExternalServer;
	PlayScreenPanel _panel;
	std::string _messageText;
	PlayScreenStateSettings(bool, bool, bool, bool, bool, bool, PlayScreenPanel, std::string);
















	PlayScreenStateSettings();
};











class PlayScreen : public Screen {


public:
	PlayScreen(bool);

	virtual ~PlayScreen();

	virtual void init();

	virtual void setupPositions();

	virtual void render(int, int, float);

	virtual void buttonClicked(Button*);

	void signOut();

	virtual bool handleBackEvent(bool);

	void closeScreen();


	const PlayScreenState getState() const;


	virtual void tick();
	void onMCOServerlistUpdated();

	bool isEditMode();

	virtual void onMojangConnectorStatus(MojangConnectionStatus);	virtual void onMojangConnectorStatus(MojangConnectionStatus);

	void updateMCOServerList();

	void updateMCOStatus();

	void joinMCOServer(MCOServerListItem);
	void resetCurrentWaitingMCOCancelButton();
	void setMainPanel(PlayScreenPanel);

protected:
	void setPlayScreenSate(PlayScreenState, bool);
	void resetBaseButtons();
	void renderControllerButtons();
	void updateHeaderItems(PlayScreenState);
	PlayScreenStateSettings& getStateData(PlayScreenState);
	GuiElementPtr buildLocalServerList();
	GuiElementPtr buildMCOServerList();
	GuiElementPtr buildMessageScreen();
	GuiElementPtr buildJoinRealmsScreen(bool);
	void updateRealmsState();
	void setPlayScreenStateSetting(PlayScreenState, bool, bool, bool, bool, bool, bool, PlayScreenPanel, const std::string&);
	bool isLocalPlayScreen();
	virtual void controllerDirectionChanged(int, Controller::StickDirection);
	virtual void keyPressed(int);

public:
	bool reloadLocalList;

private:
	bool abortJoinRequest;
	std::shared_ptr<std::unordered_map<long long, MCOServerListItem, std::hash<long long>, std::equal_to<long long>, std::allocator<std::pair<const long long, MCOServerListItem> > > > serverList;
	std::string levelName;
	Touch::THeader* bHeader;
	Touch::TButton* btnClose;
	Touch::TButton* btnCreateWorld;


	Touch::TButton* btnShowAddExternalServer;
	NinePatchLayer* listBackground;
	NinePatchLayer* buttonBackground;
	NinePatchLayer* buttonSelectedBackground;
	Spinner* spLoggingIntoRealms;

	ImageButton* btnEdit;
	ImageButton* btnEditPressed;
	ServerList currentLocalServerList;
	std::shared_ptr<RestRequestJob> listRequest;
	std::shared_ptr<RestRequestJob> statusRequest;
	std::shared_ptr<RestRequestJob> signoutRequest;
	std::shared_ptr<RestRequestJob> joinServerRequest;
	Button* btnCurrentCancelWaiting;
	double lastUpdate;
	std::string joinFailedMessage;
	MCOServerListItem currentServerListItem;

	PlayScreenState _playScreenState;
	PlayScreenState _pendingPlayScreenState;
	PlayScreenStateSettings _playScreenStateSettings[13];

	GuiElementPtr _messageScreen;
	GuiElementPtr _localServerListScreen;
	GuiElementPtr _realmsServerListScreen;
	GuiElementPtr _realmsJoiningServerScreen;
	GuiElementPtr _realmsJoinServerFailedScreen;

	GuiElementPtr _activePanel;
	int mSelectedLevelIndex;
	bool initialized;
	void iterateAndHighlightChildren(std::shared_ptr<PackedScrollContainer>);	void iterateAndHighlightChildren(std::shared_ptr<PackedScrollContainer>);
};
