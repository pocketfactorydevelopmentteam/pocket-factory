#pragma once
#include "Screen.h"
class Button;
#include "../../../platform/input/Controller.h"


class DeathScreen : public Screen {


public:
	DeathScreen();

	virtual ~DeathScreen();

	virtual void init();

	virtual void setupPositions();

	virtual void tick();
	virtual void render(int, int, float);

	virtual void buttonClicked(Button*);

	virtual bool renderGameBehind();



	virtual void controllerDirectionChanged(int, Controller::StickDirection);

protected:
	virtual void keyPressed(int);
private:
	Button* bRespawn;
	Button* bTitle;
	bool _hasChosen;
	int _tick;

	const int WAIT_TICKS;
};
