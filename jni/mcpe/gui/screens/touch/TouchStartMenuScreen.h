#pragma once
#include "../Screen.h"
#include "../../components/Button.h"
class ImageWithBackground;
#include <string>
#include <memory>
#include "../../components/ImageButton.h"
#include "../../../../platform/input/Controller.h"


namespace Touch {
class StartMenuScreen : public Screen {



public:
	static void chooseRandomSplash();

	StartMenuScreen();
	virtual ~StartMenuScreen();

	virtual void init();
	virtual void setupPositions();

	void setupPlayButtons(bool);


	virtual void tick();
	virtual void render(int, int, float);

	virtual void buttonClicked(Button*);
	virtual bool handleBackEvent(bool);
	virtual bool isInGameScreen();

protected:
	static int currentSplash;

	void _updateLicense();

	virtual void controllerDirectionChanged(int, Controller::StickDirection);


	Touch::TButton bPlay;
	Touch::TButton bPlayOnRealms;

	ImageWithBackground* bOptions;

	std::string copyright;

	std::string version;

	IntRectangle logoRect;

	double elapsedTime;
};
};
