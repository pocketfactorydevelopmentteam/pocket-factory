#pragma once
#include "../ConfirmScreen.h"
#include "../../../../world/level/storage/LevelStorageSource.h"







namespace Touch {
class TouchDeleteWorldScreen : public ConfirmScreen {

public:
	TouchDeleteWorldScreen(const LevelSummary&);
protected:
	virtual void postResult(bool);
private:
	LevelSummary _level;
};
};
