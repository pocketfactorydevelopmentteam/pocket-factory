#pragma once
#include "Screen.h"
#include <string>
#include <memory>

class RenameMPLevelScreen : public Screen {

public:
	RenameMPLevelScreen(const std::string&);

	virtual void init();
	virtual void render(int, int, float);

private:
	std::string _levelId;
};
