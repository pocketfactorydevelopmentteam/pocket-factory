#pragma once
#include "Screen.h"
#include "../../../network/mco/MCODataStructs.h"
namespace Touch { class THeader; };
class Button;
class OptionButton;
class Label;
#include "../components/ImageWithBackground.h"
#include <memory>
class TextBox;
class PackedScrollContainer;
class NinePatchLayer;
#include <string>
#include "../components/MCOInviteListItemElement.h"
#include <functional>
#include <__hash_table>
#include <unordered_map>
#include "../../../network/http/RestRequestJob.h"




class ManageMCOServerScreen : public Screen {


public:
	ManageMCOServerScreen(const MCOServerListItem&);

	virtual ~ManageMCOServerScreen();

	virtual void init();

	virtual void setupPositions();

	virtual void render(int, int, float);

	virtual void buttonClicked(Button*);
	virtual bool handleBackEvent(bool);
	void closeScreen();

	virtual void tick();

	virtual void mouseClicked(int, int, int);
	virtual void mouseReleased(int, int, int);
	virtual void onTextBoxUpdated(int);
	virtual void onFriendItemRemoved(const std::string&);
private:
	MCOServerListItem serverListItem;


	Touch::THeader* bHeader;
	Button* btnClose;
	Button* btnReset;
	OptionButton* serverOpen;
	Label* serverOpenLabel;
	Label* serverNameLabel;
	Label* invitePeopleLabel;
	std::shared_ptr<ImageWithBackground> bInvite;
	TextBox* serverName;
	TextBox* inviteFriendHiddenTextBox;
	PackedScrollContainer* inviteListContainer;
	NinePatchLayer* guiInviteFrame;
	NinePatchLayer* guiFrame;
	Label* errorLine;

	std::unordered_map<std::basic_string<char>, std::shared_ptr<MCOInviteListItemElement>, std::hash<std::basic_string<char> >, std::equal_to<std::basic_string<char> >, std::allocator<std::pair<const std::basic_string<char>, std::shared_ptr<MCOInviteListItemElement> > > > nameToInviteElementMap;

	RestRequestJobPtr request;

	MCOInviteListItemElement* _addInviteElement(const std::string&);
	bool _removeInviteElement(const std::string&);

	void _queryUsernameAndUpdateElement(const std::string&);
};
