#pragma once
#include "Screen.h"
class Button;



class InBedScreen : public Screen {

public:
	InBedScreen();

	virtual ~InBedScreen();

	virtual void init();

	virtual void setupPositions();

	virtual bool renderGameBehind();



	virtual void render(int, int, float);

	virtual bool handleBackEvent(bool);

	virtual void buttonClicked(Button*);

private:
	Button* bWakeUp;

	const int WAIT_TICKS;
};
