#pragma once
#include "Screen.h"
#include "../../../world/level/storage/LevelStorageSource.h"
#include <memory>
#include "../../../CommonTypes.h"
#include "../components/ImageButton.h"
#include <vector>
#include "../components/Button.h"
#include "../components/TextBox.h"
#include "../components/Label.h"
#include "../components/NinePatch.h"
#include "../../../network/mco/MCODataStructs.h"
#include "../../../network/http/RestRequestJob.h"
class GuiElement;
#include <string>
#include "../../../platform/input/Controller.h"
#include "../../../world/level/GeneratorType.h"
enum CreateWorldScreenType { CreateWorldScreenLocal, CreateWorldScreenMCO, CreateWorldScreenMCOReset };





class CreateWorldScreen : public Screen {


public:
	CreateWorldScreen(CreateWorldScreenType, const MCOServerListItem&);

	virtual ~CreateWorldScreen();

	virtual void init();

	virtual void setupPositions();

	virtual void render(int, int, float);

	virtual void buttonClicked(Button*);

	virtual bool guiElementClicked(GuiElement*);

	void closeScreen();

	virtual bool handleBackEvent(bool);

	virtual void tick();

	virtual void keyPressed(int);

	virtual void mouseClicked(int, int, int);

	virtual void mouseReleased(int, int, int);

	virtual void keyboardNewChar(const std::string&, bool);
	void generateLocalGame();
	void generateMCOGame(bool);

	void waitForMCO();

	virtual void feedMCOEvent(MCOEvent);
	RandomSeed getSeed();
	std::string getLevelName();
	std::string getUniqueLevelName(const std::string&);
	virtual void setTextboxText(const std::string&);

protected:
	virtual void controllerDirectionChanged(int, Controller::StickDirection);
	bool isAdvanced();
	bool isFlat();
private:
	bool advanced;
	bool worldIsFlat;
	LevelSummaryList levels;

	RandomSeed defaultSeed;
	int gameMode;
	int generator;

	std::vector<std::unique_ptr<ImageButton, std::default_delete<ImageButton> >, std::allocator<std::unique_ptr<ImageButton, std::default_delete<ImageButton> > > > bMode, bType;
	ImageButton* selectedModeButton, selectedTypeButton;
	std::unique_ptr<Button, std::default_delete<Button> > bCreateWorld, bClose, bHeader, bAdvanced, bLimitedWorldsErrorMessage;
	std::unique_ptr<TextBox, std::default_delete<TextBox> > tbGameName, tbSeed;

	std::unique_ptr<Label, std::default_delete<Label> > lGameName, lSeedName, lGameTypeDescription, lmcoCreatingWorld, lMode, lType;
	std::unique_ptr<NinePatchLayer, std::default_delete<NinePatchLayer> > guiFrame, gameTypeButtonBackground, selectedGameTypeButttonBackground;

	bool currentlyCreatingMCOWorld;

	CreateWorldScreenType createWorldScreenType;
	MCOServerListItem serverListItem;
	std::shared_ptr<RestRequestJob> createResetRequest;

	GeneratorType _getDefaultGenerator() const;

	bool _isOneOf(Button*, std::vector<std::unique_ptr<ImageButton, std::default_delete<ImageButton> >, std::allocator<std::unique_ptr<ImageButton, std::default_delete<ImageButton> > > >&, int&) const;
	bool _isOneOfGuiElements(GuiElement*, std::vector<std::unique_ptr<ImageButton, std::default_delete<ImageButton> >, std::allocator<std::unique_ptr<ImageButton, std::default_delete<ImageButton> > > >&, int&) const;
	void _updateDescription();

	int _getGameMode();
	bool limitWorldSize;

	float warningBoxYO;
	float warningBoxY;
	int warningBoxTick;
};
