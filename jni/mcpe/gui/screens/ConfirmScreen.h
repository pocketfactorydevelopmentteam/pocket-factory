#pragma once
#include "Screen.h"
#include <string>
#include <memory>
class Button;
#include "../../../platform/input/Controller.h"



class ConfirmScreen : public Screen {


public:
	ConfirmScreen(Screen*, const std::string&, const std::string&, int);
	ConfirmScreen(Screen*, const std::string&, const std::string&, const std::string&, const std::string&, int);
	virtual ~ConfirmScreen();

	virtual void init();
	virtual void setupPositions();

	virtual bool handleBackEvent(bool);
	virtual void render(int, int, float);
protected:
	virtual void buttonClicked(Button*);

	virtual void controllerDirectionChanged(int, Controller::StickDirection);

	virtual void postResult(bool);

	Screen* parent;
	int id;
private:
	std::string title1;
	std::string title2;

	std::string yesButtonText;
	std::string noButtonText;

	Button* yesButton;
	Button* noButton;
};
