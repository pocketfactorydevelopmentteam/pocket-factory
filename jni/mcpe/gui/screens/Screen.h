#pragma once
#include "../gui/GuiComponent.h"
class MinecraftClient;
class Button;
#include <memory>
#include <vector>
class TextBox;
class GuiElement;
#include "../components/ControllerButtonRenderer.h"
class Font;
#include "../../platform/input/Controller.h"
#include <functional>
#include <utility>
#include <unordered_map>
#include "../../renderer/MaterialPtr.h"
struct IntRectangle;
#include "../../network/mco/MCODataStructs.h"
#include <string>


class Screen : public GuiComponent {


public:
	Screen();

	virtual ~Screen();

	virtual void render(int, int, float);

	void init(MinecraftClient&, int, int);
	virtual void init();

	void setSize(int, int);
	virtual void setupPositions();

	virtual void updateEvents();
	virtual void mouseEvent();
	virtual void keyboardEvent();
	virtual void keyboardTextEvent();
	virtual void controllerEvent();
	virtual bool handleBackEvent(bool);

	virtual void tick();

	virtual void removed();

	virtual void renderBackground(int);
	virtual void renderDirtBackground();
	virtual void renderMenuBackground(float);

	virtual bool renderGameBehind();
	virtual bool hasClippingArea(IntRectangle&);

	virtual bool isPauseScreen();
	virtual bool isErrorScreen();
	virtual bool isInGameScreen();
	virtual bool closeOnPlayerHurt();

	virtual void confirmResult(bool, int);
	virtual void lostFocus();
	virtual void toGUICoordinate(int&, int&);
	virtual void feedMCOEvent(MCOEvent);
	virtual bool supppressedBySubWindow();
	virtual void onTextBoxUpdated(int);
	//virtual void onMojangConnectorStatus(MojangConnectionStatus);
	virtual void setTextboxText(const std::string&);

	virtual void onInternetUpdate();



protected:
	virtual void updateTabButtonSelection();

	virtual void buttonClicked(Button*);



	virtual bool guiElementClicked(GuiElement*);

	virtual void mouseClicked(int, int, int);
	virtual void mouseReleased(int, int, int);
	virtual void controllerDirectionChanged(int, Controller::StickDirection);
	virtual void controllerDirectionHeld(int, Controller::StickDirection);

	virtual void keyPressed(int);
	virtual void keyboardNewChar(const std::string&, bool);
	virtual bool shouldSendAllKeyStates();


	void tabNext();
	void tabPrev();

	int getCursorMoveThrottle();


public:
	int width;
	int height;
	bool passEvents;

protected:
	MinecraftClient* minecraft;
	std::vector<Button *, std::allocator<Button *> > buttons;
	std::vector<TextBox *, std::allocator<TextBox *> > textBoxes;
	std::vector<Button *, std::allocator<Button *> > tabButtons;

	bool getPassGuiEvents();
	void setPassGuiEvents(bool);




	std::vector<GuiElement *, std::allocator<GuiElement *> > tabElements;

	std::vector<GuiElement *, std::allocator<GuiElement *> > guiElements;
	int tabButtonIndex;
	int tabElementIndex;
	std::unique_ptr<ControllerButtonRenderer, std::default_delete<ControllerButtonRenderer> > cbr;
	Font* font;

private:
	Button* clickedButton;

	bool passGuiEvents;
	void processControllerDirection(int);
	std::unordered_map<int, Controller::StickDirection, std::hash<int>, std::equal_to<int>, std::allocator<std::pair<const int, Controller::StickDirection> > > mControllerStickDirections;

	MaterialPtr cubeMat;
};
