#pragma once
#include "../components/ImageButton.h"
#include <memory>
#include "Screen.h"
#include "../components/InventoryPane.h"
#include "../../../world/item/ItemInstance.h"
#include <vector>
#include "../components/Button.h"
#include "../components/NinePatch.h"
#include "../../../world/Pos.h"
#include "../../../platform/input/Controller.h"
class Tile;
class Item;
class CreativeInventoryScreen : public Screen, public Touch::IInventoryPaneCallback {
public:
struct TabButtonWithMeta {
	int type;
	std::shared_ptr<ImageButton> button;
	TabButtonWithMeta(bool, int, std::shared_ptr<ImageButton>);
};

private:
	int tabSide;
	const int tabSpacing;
public:
	CreativeInventoryScreen();
	virtual ~CreativeInventoryScreen();

	virtual void init();

	virtual void setupPositions();

	virtual void tick();
	virtual void render(int, int, float);

	virtual bool renderGameBehind();

	virtual void mouseReleased(int, int, int);
	virtual void buttonClicked(Button*);
	virtual void keyPressed(int);

	virtual void mouseClicked(int, int, int);
	virtual void updateTabButtonSelection();
	virtual void controllerDirectionChanged(int, Controller::StickDirection);
	virtual void controllerDirectionHeld(int, Controller::StickDirection);
	std::shared_ptr<ImageButton> createInventoryTabButton(int, const int);
	void drawIcon(int, std::shared_ptr<ImageButton>, bool, bool);
	ItemInstance getItemFromType(int);


	virtual bool addItem(const Touch::InventoryPane*, int);

	virtual bool isAllowed(int);
	virtual std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > getItems(const Touch::InventoryPane*);

	virtual bool handleBackEvent(bool);
protected:
	int getCategoryFromPanel(const Touch::InventoryPane*);
	static std::vector<ItemInstance, std::allocator<ItemInstance> > items;
	static std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > filteredItems[];
	static void populateItems();
	static void populateItem(Tile*, int, int);
	static void populateItem(Item*, int, int);
	static void populateFilteredItems();
	void closeWindow();

	std::shared_ptr<Button> closeButton;

	std::shared_ptr<NinePatchLayer> windowBackground;
	std::shared_ptr<NinePatchLayer> inventoryPaneBorder;
	std::shared_ptr<NinePatchLayer> tabButtonBackground;
	std::shared_ptr<Touch::InventoryPane> inventoryPanes[4];

	std::vector<CreativeInventoryScreen::TabButtonWithMeta, std::allocator<CreativeInventoryScreen::TabButtonWithMeta> > filterButtons;
	ImageButton* selectedFilterButton;
	Pos windowPos;
	int currentFilter;
	int inventoryDarkBorderWidth;
	bool initilized;

	void _putItemInToolbar(const ItemInstance*);
};
