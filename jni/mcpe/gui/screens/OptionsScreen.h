#pragma once
#include "Screen.h"
class OptionsPane;
#include <memory>
#include <vector>
class NinePatchLayer;
namespace Touch { class THeader; };
class Button;
class ImageButton;
#include <string>

class OptionsScreen : public Screen {


	virtual void init();

	virtual void generateOptionScreens();

public:
	OptionsScreen(bool);
	virtual ~OptionsScreen();
	virtual void setupPositions();

	virtual void createCategoryButtons();

	int createCategoryButton(int, int, _Iter&, int, int, int, int);

	virtual void buttonClicked(Button*);

	void closeScreen();

	virtual void render(int, int, float);
	virtual void renderBgFill();
	virtual void removed();
	void selectCategory(int);

	virtual void mouseClicked(int, int, int);
	virtual void mouseReleased(int, int, int);
	virtual void tick();
	virtual bool handleBackEvent(bool);

	virtual void keyPressed(int);

	virtual void keyboardNewChar(const std::string&, bool);

	virtual bool renderGameBehind();



	virtual void setTextboxText(const std::string&);

protected:
	bool inGame;
	std::vector<OptionsPane *, std::allocator<OptionsPane *> > optionPanes;
	NinePatchLayer* guiCategory;
	NinePatchLayer* guiCategorySelected;
	int selectedCategory;
	int categoryButtonHeight;
	Touch::THeader* bHeader;


private:
	Button* btnClose;
	ImageButton* selectedButton;
	std::vector<ImageButton *, std::allocator<ImageButton *> > categoryButtons;

	OptionsPane* currentOptionPane;
};
