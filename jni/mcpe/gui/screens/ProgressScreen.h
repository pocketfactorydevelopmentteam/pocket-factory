#pragma once
#include "Screen.h"
#include "../components/Button.h"
#include <memory>
#include <string>
#include "../../renderer/renderer/MaterialPtr.h"
#include "../../../network/mco/MCODataStructs.h"
#include "../../player/LoadingState.h"

class ProgressScreen : public Screen {



public:
	ProgressScreen();

	virtual void init();
	virtual void render(int, int, float);
	virtual bool isInGameScreen();

	virtual void tick();

	virtual void feedMCOEvent(MCOEvent);

	virtual void setupPositions();

	virtual bool renderGameBehind();



	virtual bool handleBackEvent(bool);

	virtual void buttonClicked(Button*);
	void exitScreen();

	const std::string& getProgressMessage();

private:
	int ticks;
	std::unique_ptr<Button, std::default_delete<Button> > cancelButton;

	static std::string progressMessages[];

	LoadingState _loadingState() const;
	bool _isInCancellableState() const;
	MaterialPtr colorBlit;
	int locatingTicks;
};
