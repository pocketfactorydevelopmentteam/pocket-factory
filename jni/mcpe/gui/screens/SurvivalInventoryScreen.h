#pragma once
#include "../../../world/item/ItemInstance.h"
#include <memory>
#include "../components/ImageButton.h"
#include "Screen.h"
#include "../components/InventoryPane.h"
#include "../components/Button.h"
#include "../components/NinePatch.h"
#include <vector>
#include <array>
#include "../../../world/Pos.h"
#include "../components/ItemPane.h"
#include "../components/Label.h"
#include <string>
#include "../../../platform/input/Controller.h"
class Tessellator;
#include "../../../world/item/crafting/Recipe.h"
class SurvivalInventoryScreen : public Screen, public Touch::IInventoryPaneCallback {
	typedef std::vector<Recipe *, std::allocator<Recipe *> > RecipeList;
public:enum class IngredientSlotStatus : int { None, Missing, Found };




struct IngredientFoundPosition {
	SurvivalInventoryScreen::IngredientSlotStatus status;
	std::shared_ptr<ItemInstance> itemInstance;
	IngredientFoundPosition();
	IngredientFoundPosition(SurvivalInventoryScreen::IngredientSlotStatus, std::shared_ptr<ItemInstance>);
};


enum class InventoryPaneType : int { Inventory, Crafting, Armor };



private:
	static const int NUM_ARMORBUTTONS = 4;
public:
struct TabButtonWithMeta {
	SurvivalInventoryScreen::InventoryPaneType type;
	std::shared_ptr<ImageButton> button;
	TabButtonWithMeta(bool, SurvivalInventoryScreen::InventoryPaneType, std::shared_ptr<ImageButton>);
};

private:
	int tabSide;
	const int tabSpacing;
public:
enum class CraftingType : int { Inventory, WorkBench, StoneCutter };




	SurvivalInventoryScreen(SurvivalInventoryScreen::CraftingType);

	virtual void init();

	virtual void setupPositions();

	virtual void tick();
	virtual void render(int, int, float);

	virtual bool renderGameBehind();

	virtual void mouseReleased(int, int, int);
	virtual void buttonClicked(Button*);
	virtual void keyPressed(int);

	virtual void mouseClicked(int, int, int);
	std::shared_ptr<ImageButton> createInventoryTabButton(int);
	void drawIcon(SurvivalInventoryScreen::InventoryPaneType, std::shared_ptr<ImageButton>, bool, bool);
	ItemInstance getItemFromType(SurvivalInventoryScreen::InventoryPaneType);
	virtual void updateTabButtonSelection();
	virtual void controllerDirectionChanged(int, Controller::StickDirection);
	virtual void controllerDirectionHeld(int, Controller::StickDirection);

	virtual bool addItem(const Touch::InventoryPane*, int);


	virtual bool isAllowed(int);

	virtual bool handleBackEvent(bool);
protected:
	void closeWindow();
	void renderPlayer(float, float, float);
	void drawSlotItemAt(Tessellator&, int, const ItemInstance*, int, int, bool);
	bool addItemForInventoryPane(int&);
	void updateArmorItems();

	void _putItemInToolbar(const ItemInstance*);

	virtual std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > getItems(const Touch::InventoryPane*);
	void renderArmorScreen(Tessellator&, int, int, float);
	bool addItemArmorScreen(int);
	void takeAndClearArmorSlot(int);
	void initCraftingCategories();
	void updateCraftableItems(bool);

	void refreshCraftingItemList(const SurvivalInventoryScreen::RecipeList&);

	void updateCraftingButtonVisiblity();

	ItemPack getPlayerInventoryItemPack();

	int getNumberOfIngreedients(ItemInstance&, ItemPack&);

	SurvivalInventoryScreen::RecipeList filterRecipeList(const SurvivalInventoryScreen::RecipeList&);

	void renderCraftingScreen(Tessellator&, int, int, float);
	bool addItemCraftingScreen(int);
	void setCurrentRecipe(Recipe*);
	void refreshCraftingGrid(Recipe*);

	void updateIngredientCountFromRecipe(Recipe*, bool, ItemPack&);
	void renderGridItem(ItemInstance*, SurvivalInventoryScreen::IngredientSlotStatus, int, int);
	std::shared_ptr<Button> closeButton;

	std::shared_ptr<NinePatchLayer> windowBackground;
	std::shared_ptr<NinePatchLayer> tabButtonBackground;
	std::shared_ptr<NinePatchLayer> guiArmorPlayerBg;
	std::shared_ptr<NinePatchLayer> guiArmorPaneFrame;
	std::shared_ptr<NinePatchLayer> guiSlot;
	std::shared_ptr<NinePatchLayer> guiSlotMarker;
	std::shared_ptr<NinePatchLayer> inventoryPaneBorder;

	std::shared_ptr<Touch::InventoryPane> inventoryPane;
	std::shared_ptr<Touch::InventoryPane> armorInventoryScreen;
	std::shared_ptr<Touch::InventoryPane> craftingInventoryScreen;

	std::shared_ptr<BlankButton> btnArmor0;
	std::shared_ptr<BlankButton> btnArmor1;
	std::shared_ptr<BlankButton> btnArmor2;
	std::shared_ptr<BlankButton> btnArmor3;
	std::shared_ptr<Button> btnCraftItem;
	IntRectangle craftingActionPane;
	BlankButton* armorButtons[4];

	IntRectangle armorItemBackgroundRect;
	IntRectangle armorPlayerBackgroundRect;

	std::vector<SurvivalInventoryScreen::TabButtonWithMeta, std::allocator<SurvivalInventoryScreen::TabButtonWithMeta> > panelTabButtons;
	std::array<std::shared_ptr<Button>, 9> craftingGridButtons;
	ImageButton* selectedPanelTabButton;
	Pos windowPos;
	SurvivalInventoryScreen::InventoryPaneType currentFilter;
	int inventoryDarkBorderWidth;
	bool initilized;

	bool doRecreateArmorPane;

	std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > armorItems;
	std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > craftingItems;
	std::vector<std::shared_ptr<CItem>, std::allocator<std::shared_ptr<CItem> > > cItems;
	std::shared_ptr<Label> recipeItemName;
	std::shared_ptr<Label> noRecipesLabel;
	SurvivalInventoryScreen::CraftingType craftingType;
	std::array<SurvivalInventoryScreen::IngredientFoundPosition, 9> currentRecipeLayout;
	std::shared_ptr<CItem> curretRecipe;

	const float BorderPixels;
	const float BlockPixels;
	const int ItemSize;

	std::string strCraftingCannotCreate;
private:
	void handleArmorJoyStickNavigation(Controller::StickDirection);
	void handleArmorSlotNavigation(Controller::StickDirection);
	void handleArmorInventoryNavigation(Controller::StickDirection);
	bool armorInventoryPaneActive;
	int armorInventorySelectedIndex;
	int playerArmorSelectedIndex;
};
