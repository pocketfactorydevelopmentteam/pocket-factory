#pragma once
#include "Screen.h"
#include "../components/Button.h"
#include <memory>
#include "../components/Label.h"
#include "../components/TextBox.h"
#include "../components/NinePatch.h"
class GuiElement;
#include "../../../platform/input/Controller.h"


class AddExternalServerScreen : public Screen {


public:
	virtual void init();

	virtual void setupPositions();

	virtual void render(int, int, float);

	virtual void buttonClicked(Button*);

	void closeScreen();

	virtual bool handleBackEvent(bool);

protected:
	virtual bool guiElementClicked(GuiElement*);
	virtual void controllerDirectionChanged(int, Controller::StickDirection);

private:
	std::shared_ptr<Button> bClose;
	std::shared_ptr<Button> bHeader;
	std::shared_ptr<Button> bAddServer;
	std::shared_ptr<Label> lbServerName;
	std::shared_ptr<TextBox> tbServerName;
	std::shared_ptr<Label> lbServerAddress;
	std::shared_ptr<TextBox> tbServerAddress;
	std::shared_ptr<Label> lbServerPort;
	std::shared_ptr<TextBox> tbServerPort;
	std::shared_ptr<Label> lbDescription;
	std::shared_ptr<NinePatchLayer> guiFrame;
};
