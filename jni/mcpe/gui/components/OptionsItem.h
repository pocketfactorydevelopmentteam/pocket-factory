#pragma once
#include "GuiElementContainer.h"
#include "../../options/Options.h"
#include "GuiElement.h"
class MinecraftClient;







class OptionsItem : public GuiElementContainer {


public:
	OptionsItem(const Options::Option*, GuiElementPtr);
	virtual void render(MinecraftClient*, int, int);
	virtual void setupPositions();

	virtual void onSelectedChanged();

private:
	const Options::Option* option;
};

	typedef std::shared_ptr<OptionsItem> OptionsItemPtr;
