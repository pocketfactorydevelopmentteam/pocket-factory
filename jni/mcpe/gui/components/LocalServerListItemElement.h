#pragma once
#include "GuiElement.h"
class Button;
class ImageButton;
struct PingedCompatibleServer;
struct LevelSummary;
#include <string>
#include <memory>
class PlayScreen;
class MinecraftClient;
#include "../../game/ExternalServersFile.h"






class LocalServerListItemElement : public GuiElement {


public:
	LocalServerListItemElement(MinecraftClient*, ExternalServer, bool, PlayScreen*);

	LocalServerListItemElement(MinecraftClient*, const LevelSummary&, bool);

	LocalServerListItemElement(const PingedCompatibleServer&, bool);
	virtual ~LocalServerListItemElement();
	void init(MinecraftClient*);

	virtual void render(MinecraftClient*, int, int);
	virtual void mouseClicked(MinecraftClient*, int, int, int);
	virtual void mouseReleased(MinecraftClient*, int, int, int);
	virtual void keyPressed(MinecraftClient*, int);
	std::string getLastPlayedString();

	virtual void drawSelected();

	virtual void tick(MinecraftClient*);

private:
	float buttonDownX;
	float buttonDownY;
	Button* mainButton;
	Button* pressedButton;
	ImageButton* detailButton;
	ExternalServer* externalServer;
	PingedCompatibleServer* remoteServer;
	LevelSummary* localServer;

	std::string seedString;
	int seedStringWidth;
	bool inEditMode;

	int animationTick;
	PlayScreen* _screen;

	std::string worldSize;
	void serverMainPressed(MinecraftClient*);
	void serverRemovePressed(MinecraftClient*);
};

	typedef std::shared_ptr<LocalServerListItemElement> LocalServerListItemElementPtr;
