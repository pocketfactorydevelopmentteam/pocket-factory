#pragma once
class ItemInstance;
#include <memory>
#include <vector>
#include "ScrollingPane.h"
#include "../../../AppPlatformListener.h"
#include "ImageButton.h"
class IArea;
class MinecraftClient;
#include "../../renderer/renderer/Mesh.h"
#include "../../renderer/renderer/MaterialPtr.h"
#include "ControllerButtonRenderer.h"
#include "../../../platform/input/Controller.h"
class Tessellator;
#include "../../renderer/entity/ItemRenderer.h"



namespace Touch {
class InventoryPane : public ScrollingPane, public AppPlatformListener {




public:
	InventoryPane(Touch::IInventoryPaneCallback*, MinecraftClient&, const IntRectangle&, int, float, int, int, int, bool, bool, bool);
	virtual ~InventoryPane();

	void tick();

	void setControllerDirection(Controller::StickDirection);
	virtual void renderBatch(std::vector<ScrollingPane::GridItem, std::allocator<ScrollingPane::GridItem> >&, float, float, float);

	void renderSelectedItem(std::vector<ScrollingPane::GridItem, std::allocator<ScrollingPane::GridItem> >&, std::vector<const ItemInstance *, std::allocator<const ItemInstance *> >, Tessellator&, ScrollingPane::GridItem*&, float&, float&);	void renderSelectedItem(std::vector<ScrollingPane::GridItem, std::allocator<ScrollingPane::GridItem> >&, std::vector<const ItemInstance *, std::allocator<const ItemInstance *> >, Tessellator&, ScrollingPane::GridItem*&, float&, float&);

	virtual bool onSelect(int, bool);
	void onSelectItem();
	void drawScrollBar(ScrollBar&);

	void setRenderDecorations(bool);

	virtual void refreshItems();
	void buildInventoryItemsChunk(std::vector<const ItemInstance *, std::allocator<const ItemInstance *> >&, ItemRenderChunkType);
	IntRectangle rect;
	int paneWidth;
	IArea* _clickArea;
	Touch::IInventoryPaneCallback* screen;
	MinecraftClient& mc;

	int fillMarginX;
	int fillMarginY;

	int markerType;
	int markerIndex;
	float markerShare;

	virtual void onAppSuspended();
	virtual void onAppResumed();

private:
	void drawRectangleOnSelectedItem(ScrollingPane::GridItem&);
	int lastItemIndex;
	int lastItemTicks;
	int BorderPixels;
	bool renderDecorations;
	Mesh _chunkTiles;
	Mesh _chunkTileItems;
	Mesh _chunkItems;
	IntRectangle bg;
	bool drawGradients;
	bool drawBackground;

	MaterialPtr itemMat;

	const int By;
	bool alwaysShowItem;
	std::unique_ptr<ControllerButtonRenderer, std::default_delete<ControllerButtonRenderer> > cbr;
};
};
namespace Touch {
class IInventoryPaneCallback {

public:
	virtual ~IInventoryPaneCallback();
	virtual bool addItem(const Touch::InventoryPane*, int) = 0;
	virtual bool isAllowed(int) = 0;
	virtual std::vector<const ItemInstance *, std::allocator<const ItemInstance *> > getItems(const Touch::InventoryPane*) = 0;
};
};
