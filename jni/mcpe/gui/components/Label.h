#pragma once
#include "GuiElement.h"
#include <string>
#include <memory>
class MinecraftClient;
#include "../../renderer/Color.h"



class Label : public GuiElement {

public:
	Label(std::string, MinecraftClient*, const Color&, int, int, int, bool);

	void setCentered(bool);

	virtual std::string getText() const;
	virtual void setText(std::string);
	virtual void render(MinecraftClient*, int, int);
	virtual void setupPositions();
	virtual void setWidth(int);
	virtual void setColor(const Color&);
private:
	std::string text;
	MinecraftClient* minecraft;
	Color color;
	int padX;
	int padY;
	bool fixedWidth;
	bool hasShadow;
	bool centered;
};

	typedef std::shared_ptr<Label> LabelPtr;
