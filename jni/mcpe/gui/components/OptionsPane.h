#pragma once
#include "PackedScrollContainer.h"
#include "ImageButton.h"
#include <memory>
#include <string>












class OptionsPane : public PackedScrollContainer {


public:
	OptionsPane();
	std::default_delete<OptionButton> createOptionsGroup(std::string);
	virtual void setupPositions();
};
