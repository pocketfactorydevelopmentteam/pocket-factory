#pragma once


class ControllerButtonCoordinates {

public:
	ControllerButtonCoordinates();
	~ControllerButtonCoordinates();

	void fill(int, int, int, int, int, int);

	int getSX() const;
	int getSY() const;
	int getW() const;
	int getH() const;
	int getSW() const;
	int getSH() const;

private:
	int sx;
	int sy;
	int w;
	int h;
	int sw;
	int sh;
};
