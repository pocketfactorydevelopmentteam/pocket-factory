#pragma once
#include "GuiElementContainer.h"
class ScrollingPane;
class MinecraftClient;
#include <string>
#include <memory>


class PackedScrollContainer : public GuiElementContainer {

public:
	PackedScrollContainer(bool, int, int);
	virtual ~PackedScrollContainer();
	virtual void render(MinecraftClient*, int, int);

	virtual void setupPositions();

	virtual void tick(MinecraftClient*);

	virtual void mouseClicked(MinecraftClient*, int, int, int);

	virtual void setTextboxText(const std::string&);

private:
	ScrollingPane* scrollPane;
	int oldHeight;
	bool lockY;
	int padX;
	int padY;
};

	typedef std::shared_ptr<PackedScrollContainer> PackedScrollContainerPtr;
