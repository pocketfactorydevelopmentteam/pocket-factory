#pragma once
#include "ImageButton.h"
#include <string>
#include <memory>
class MinecraftClient;
class LargeImageButton : public ImageButton {


public:
	LargeImageButton(int, const std::string&);
	LargeImageButton(int, const std::string&, ImageDef&);

	virtual void render(MinecraftClient*, int, int);

protected:
	virtual void setupDefault();

	float _buttonScale;
};
