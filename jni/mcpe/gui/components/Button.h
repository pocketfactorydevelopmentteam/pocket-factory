#pragma once
#include "GuiElement.h"
#include <string>
#include <memory>
class MinecraftClient;
class NinePatchLayer;
struct IntRectangle;





class Button : public GuiElement {

public:
	Button(int, const std::string&, bool);
	Button(int, int, int, const std::string&);
	Button(int, int, int, int, int, const std::string&);
	virtual ~Button();
	virtual void render(MinecraftClient*, int, int);

	virtual bool clicked(MinecraftClient*, int, int);
	virtual void released(int, int);
	virtual void setPressed();

	bool isInside(int, int);
	void setMsg(const std::string&);
	bool isPressed(int, int);
	bool isPressed();


	void setOverrideScreenRendering(bool);
	bool isOveridingScreenRendering();

	virtual void mouseReleased(MinecraftClient*, int, int, int);
protected:
	virtual int getYImage(bool);
	virtual void renderBg(MinecraftClient*, int, int);

	virtual void renderFace(MinecraftClient*, int, int);

	bool hovered(MinecraftClient*, int, int);
public:
	std::string msg;
	int id;


	const bool flip;
protected:
	bool _currentlyDown;
	bool overrideScreenRendering;
};



class BlankButton : public Button {


public:
	BlankButton(int);
	BlankButton(int, int, int, int, int);
};


namespace Touch {
class TButton : public Button {


public:
	TButton(int, const std::string&, MinecraftClient*, bool);
	TButton(int, int, int, const std::string&, MinecraftClient*);
	TButton(int, int, int, int, int, const std::string&, MinecraftClient*);
	void init(MinecraftClient*);
	void init(MinecraftClient*, const std::string&, const IntRectangle&, const IntRectangle&, int, int, int, int);
	virtual ~TButton();
protected:
	virtual void renderBg(MinecraftClient*, int, int);
	NinePatchLayer* guiBackground;
	NinePatchLayer* guiActiveBackground;
};
};
namespace Touch {
class THeader : public Button {

public:
	THeader(int, const std::string&);
	THeader(int, int, int, const std::string&);
	THeader(int, int, int, int, int, const std::string&);
	virtual void render(MinecraftClient*, int, int);
protected:
	virtual void renderBg(MinecraftClient*, int, int);

public:
	int xText;
};
};
	typedef std::shared_ptr<Button> ButtonPtr;
