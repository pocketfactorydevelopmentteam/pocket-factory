#pragma once
#include "GuiElement.h"
#include <memory>
#include <vector>
class MinecraftClient;
#include <string>



class GuiElementContainer : public GuiElement {

public:
	GuiElementContainer(bool, bool, int, int, int, int);
	virtual ~GuiElementContainer();
	virtual void render(MinecraftClient*, int, int);
	virtual void setupPositions();
	virtual void addChild(GuiElementPtr);
	virtual void removeChild(GuiElementPtr);

	virtual void tick(MinecraftClient*);

	virtual void mouseClicked(MinecraftClient*, int, int, int);

	virtual void mouseReleased(MinecraftClient*, int, int, int);
	virtual void clearAll();

	virtual bool suppressOtherGUI();

	virtual void topRender(MinecraftClient*, int, int);

	virtual void focusuedMouseClicked(MinecraftClient*, int, int, int);

	virtual void focusuedMouseReleased(MinecraftClient*, int, int, int);

	virtual void keyPressed(MinecraftClient*, int);

	virtual void keyboardNewChar(MinecraftClient*, std::string, bool);

	virtual bool backPressed(MinecraftClient*, bool);

	virtual void setTextboxText(const std::string&);

	const std::vector<std::shared_ptr<GuiElement>, std::allocator<std::shared_ptr<GuiElement> > > getChildren();

protected:
	std::vector<std::shared_ptr<GuiElement>, std::allocator<std::shared_ptr<GuiElement> > > _children;
	int lastX;
	int lastY;
};

	typedef std::shared_ptr<GuiElementContainer> GuiElementContainerPtr;
