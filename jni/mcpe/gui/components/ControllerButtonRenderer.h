#pragma once
#include "GuiElement.h"
#include <string>
#include <memory>
#include <functional>
#include <utility>
#include <map>
class MinecraftClient;
class ControllerButtonCoordinates;
class ControllerButtonRenderer : public GuiElement {

public:
	ControllerButtonRenderer(MinecraftClient&);
	virtual ~ControllerButtonRenderer();
enum ButtonIcon { A, B, X, Y, DPAD, LT, RT, CONTROLLER, JOYSTICK, GENERIC_BUTTON };
	void renderControllerButtons(int, int, ControllerButtonRenderer::ButtonIcon, const std::string&);
	void renderTriggerButton(int, int, ControllerButtonRenderer::ButtonIcon, const std::string&);
	void renderControllerDiagram(int, int);
	void renderJoystick(int, int, const std::string&);
	void renderDPadIcon(int, int, const std::string&);
	void renderInventoryCursor(int, int);

private:
	void renderStandardControllerButtons();
	void renderStandardBottomLeftABButtons();
	void renderControllerButtonB(int);
	void renderControllerButtonA();
	void renderControllerButtonX();
	void renderControllerButtonY();
	void renderImage(int, int);
	std::map<ControllerButtonRenderer::ButtonIcon, std::basic_string<char>, std::less<ControllerButtonRenderer::ButtonIcon>, std::allocator<std::pair<const ControllerButtonRenderer::ButtonIcon, std::basic_string<char> > > > buttonStringValues;
	void retrieveCoordinatesForIcon(ControllerButtonRenderer::ButtonIcon, ControllerButtonCoordinates&);
	MinecraftClient& _minecraft;
};
