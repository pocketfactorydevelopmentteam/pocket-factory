#pragma once
#include "Button.h"
#include "../../renderer/renderer/Color.h"
#include <string>
#include <memory>
class MinecraftClient;
#include "../../options/Options.h"

struct IntRectangle {
	IntRectangle();





	IntRectangle(int, int, int, int);






	int x, y;
	int w, h;
};	typedef IntRectangle IntRectangle;

struct ImageDef {
	ImageDef();







	std::string name;
	int x;
	int y;
	float width;
	float height;

	_Iter& setSrc(const IntRectangle&);




	IntRectangle* getSrc();


protected:
	IntRectangle src;
	bool hasSrc;
	typedef _Iter ImageDef;};


class ImageButton : public Button {


public:
	ImageButton(int, const std::string&);
	ImageButton(int, const std::string&, const ImageDef&);
	void setImageDef(const ImageDef&, bool);

	virtual void render(MinecraftClient*, int, int);
	virtual void renderBg(MinecraftClient*, int, int);
	virtual void setYOffset(int);

	void setOverlayColor(const Color&);



protected:
	virtual void setupDefault();
	virtual bool isSecondImage(bool);

	ImageDef _imageDef;
	Color overlay;
public:
	bool scaleWhenPressed;
	int yOffset;
};






class OptionButton : public ImageButton {


public:
	OptionButton(const Options::Option*);
	OptionButton(const Options::Option*, float, float);
	OptionButton(bool);

	void toggle(Options*);
	void updateImage(Options*);
	bool isSet(Options*);

	virtual void tick(MinecraftClient*);
	static const int ButtonId = 9999999;

	virtual void keyPressed(MinecraftClient*, int);

protected:
	virtual bool isSecondImage(bool);

	virtual void mouseClicked(MinecraftClient*, int, int, int);


private:
	const Options::Option* _option;
	bool _secondImage;


	bool _isFloat;
	float _onValue;
	float _offValue;
	float _current;
};
