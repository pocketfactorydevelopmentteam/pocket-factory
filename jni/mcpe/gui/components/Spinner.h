#pragma once
#include "GuiElement.h"
class MinecraftClient;


class Spinner : public GuiElement {
public:
	Spinner();
	virtual void render(MinecraftClient*, int, int);
};
	typedef std::shared_ptr<Spinner> SpinnerPtr;
