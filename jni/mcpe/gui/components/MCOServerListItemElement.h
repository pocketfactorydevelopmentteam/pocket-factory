#pragma once
#include "GuiElement.h"
class Button;
#include "../../../network/mco/MCODataStructs.h"
class ImageButton;
class MinecraftClient;



	typedef std::function<void (MCOServerListItem &, bool)> MCOServerListItemCallbackFunc;





class MCOServerListItemElement : public GuiElement {

public:
	MCOServerListItemElement(MinecraftClient*, const MCOServerListItem&, bool, MCOServerListItemCallbackFunc);
	virtual ~MCOServerListItemElement();
	virtual void render(MinecraftClient*, int, int);
	virtual void mouseClicked(MinecraftClient*, int, int, int);
	virtual void mouseReleased(MinecraftClient*, int, int, int);

	virtual void tick(MinecraftClient*);


private:
	float buttonDownX;
	float buttonDownY;
	Button* pressedButton;
	MCOServerListItem item;
	ImageButton* detailButton;
	Button* mainButton;
	MCOServerListItemCallbackFunc _joinServerFunc;
	bool startingServer;
};

	typedef std::shared_ptr<MCOServerListItemElement> MCOServerListItemElementPtr;
