#pragma once
#include "../gui/GuiComponent.h"
#include "../../renderer/Color.h"
class NinePatchLayer;
class MinecraftClient;
#include <string>
#include <memory>
struct IntRectangle;






class GuiElement : public GuiComponent {
public:
	GuiElement(bool, bool, int, int, int, int);
	virtual ~GuiElement();
	virtual void tick(MinecraftClient*);
	virtual void render(MinecraftClient*, int, int);
	virtual void topRender(MinecraftClient*, int, int);
	virtual void setupPositions();

	virtual void mouseClicked(MinecraftClient*, int, int, int);
	virtual void mouseReleased(MinecraftClient*, int, int, int);

	virtual void focusuedMouseClicked(MinecraftClient*, int, int, int);
	virtual void focusuedMouseReleased(MinecraftClient*, int, int, int);

	virtual void keyPressed(MinecraftClient*, int);
	virtual void keyboardNewChar(MinecraftClient*, std::string, bool);
	virtual bool backPressed(MinecraftClient*, bool);

	virtual bool pointInside(int, int);
	virtual bool suppressOtherGUI();
	virtual void setTextboxText(const std::string&);

	void setVisible(bool);
	void clearBackground();
	void setBackground(const Color&);
	void setBackground(MinecraftClient*, const std::string&, const IntRectangle&, int, int);
	void setActiveAndVisibility(bool, bool);
	void setActiveAndVisibility(bool);
	void setSelected(bool);
	bool isSelected();
	virtual void drawSelected();
	virtual void drawSliderSelected();
	virtual void onSelectedChanged();
	bool active;
	bool visible;

	int x;
	int y;
	int width;
	int height;
private:
	Color guiElementBackgroundColor;
	NinePatchLayer* guiElementBackgroundLayer;
	bool selected;
};

	typedef std::shared_ptr<GuiElement> GuiElementPtr;
