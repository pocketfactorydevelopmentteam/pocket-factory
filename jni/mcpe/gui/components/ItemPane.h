#pragma once
#include "../../../world/item/ItemInstance.h"
class Recipe;
#include <string>
#include <memory>
#include <vector>








class CItem {

public:
	CItem(const ItemInstance&, Recipe*, const std::string&);









	CItem();








struct ReqItem {
	ReqItem();
	ReqItem(const ItemInstance&, int);

	int getBuildCount();


	ItemInstance item;
	int has;
	bool enough();
};

	bool canCraft() const;


	void setCanCraft(bool);



	ItemInstance item;
	Recipe* recipe;
	std::string text;
	std::string sortText;
	int maxBuildCount;
	int inventoryCount;
	std::vector<CItem::ReqItem, std::allocator<CItem::ReqItem> > neededItems;
private:
	bool _canCraft;
};
