#pragma once
#include "ImageButton.h"
class NinePatchLayer;
#include <string>
#include <memory>
class MinecraftClient;
class CategoryButton : public ImageButton {

public:
	CategoryButton(int, const const ImageButton**, NinePatchLayer*, NinePatchLayer*);





	CategoryButton(int, std::string, const const ImageButton**, NinePatchLayer*, NinePatchLayer*);






	virtual void renderBg(MinecraftClient*, int, int);










	virtual bool isSecondImage(bool);

private:
	const const ImageButton** selectedPtr;
	NinePatchLayer* stateNormal;
	NinePatchLayer* statePressed;
};
