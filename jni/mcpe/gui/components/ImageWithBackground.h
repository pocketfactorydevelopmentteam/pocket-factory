#pragma once
#include "LargeImageButton.h"
class NinePatchLayer;
class Textures;
#include "ImageButton.h"
#include <string>
#include <memory>
class MinecraftClient;
class ImageWithBackground : public LargeImageButton {


public:
	ImageWithBackground(int);
	virtual ~ImageWithBackground();

	void init(Textures*, int, int, IntRectangle, IntRectangle, int, int, const std::string&);



	void setSize(float, float);

	virtual void renderBg(MinecraftClient*, int, int);

	virtual void render(MinecraftClient*, int, int);

private:
	NinePatchLayer* bg;
	NinePatchLayer* bgSelected;
};
