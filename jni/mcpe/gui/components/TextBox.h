#pragma once
#include "GuiElement.h"
namespace Touch { class TButton; };
#include "../../options/Options.h"
#include <string>
#include <memory>
class Screen;
class MinecraftClient;





	typedef function * OnTextBoxDone;
class TextBox : public GuiElement {

public:
	static const char* extendedAcsii;
	static const char* numberChars;

	TextBox(MinecraftClient*, const Options::Option*, const std::string&);
	TextBox(MinecraftClient*, const std::string&, int, const char*, int, Screen*, OnTextBoxDone, int);
	virtual ~TextBox();

	virtual void setFocus(MinecraftClient*);
	virtual bool loseFocus(MinecraftClient*);

	virtual void render(MinecraftClient*, int, int);

	virtual void mouseClicked(MinecraftClient*, int, int, int);

	virtual void mouseReleased(MinecraftClient*, int, int, int);

	virtual void tick(MinecraftClient*);

	virtual void topRender(MinecraftClient*, int, int);

	virtual bool suppressOtherGUI();

	virtual void focusuedMouseClicked(MinecraftClient*, int, int, int);

	virtual void focusuedMouseReleased(MinecraftClient*, int, int, int);

	virtual void keyPressed(MinecraftClient*, int);
	void setValidChars(const char*, size_t);

	virtual void keyboardNewChar(MinecraftClient*, std::string, bool);
	void updateText(MinecraftClient*);
	void setText(const std::string&);
	const std::string& getText() const;

	virtual bool backPressed(MinecraftClient*, bool);

	int getKey();

	virtual void setTextboxText(const std::string&);


	int _key;
	Touch::TButton* doneButton;
	Touch::TButton* pressedButton;
	bool pressedTextBox;
	const Options::Option* option;
	std::string text;
	std::string descriptiveName;
	bool focused;
	int maxLength;
	const char* validChars;
	int numInvalidChars;
	OnTextBoxDone _fnOnTextBoxDone;
	Screen* _parrentScreen;
	bool canModify;

private:
	bool imeEditing;
};

	typedef std::shared_ptr<TextBox> TextBoxPtr;
