#pragma once
#include "GuiElementContainer.h"
#include <string>
#include <memory>
class ImageButton;
class Button;
class ManageMCOServerScreen;
class MinecraftClient;

	typedef function * OnFriendItemRemoved;
class MCOInviteListItemElement : public GuiElementContainer {



public:
enum State { MS_SEARCHING, MS_FOUND };





	MCOInviteListItemElement(const std::string, ManageMCOServerScreen&, OnFriendItemRemoved);
	virtual ~MCOInviteListItemElement();

	void onFriendSearchCompleted(const std::string&);

	virtual void render(MinecraftClient*, int, int);

	virtual void mouseClicked(MinecraftClient*, int, int, int);

	virtual void mouseReleased(MinecraftClient*, int, int, int);

	virtual void setupPositions();

private:
	MCOInviteListItemElement::State buttonState;

	std::string _friendUserName;
	ImageButton* btRemoveInvite;
	Button* btPressed;
	ManageMCOServerScreen* parentScreen;
	OnFriendItemRemoved onItemRemoved;
};
