#pragma once
#include "GuiElement.h"
#include "ImageButton.h"
#include "../../input/TouchAreaModel.h"
#include "../../../world/phys/Vec3.h"
#include <memory>
#include <vector>
#include "../../../util/Timer.h"
class MinecraftClient;
















struct ScrollBar {
	ScrollBar();



	float x;
	float y;
	float w;
	float h;

	float alpha;
	int fading;
	typedef ScrollBar ScrollBar;};

class ScrollingPane : public GuiElement {
public:
struct GridItem {
	int id;
	int x, y;

	float xf, yf;
	bool selected;
};	typedef ScrollingPane::GridItem GridItem;

	ScrollingPane(int, const IntRectangle&, const IntRectangle&, int, int, float, const IntRectangle&);

	void recalculateRows(int);


	void tick();
	void render(int, int, float, MinecraftClient*);

	bool getGridItemFor_slow(int, ScrollingPane::GridItem&);

	void setSelected(int, bool);

	int getSelectedItemId();



	virtual void renderBatch(std::vector<ScrollingPane::GridItem, std::allocator<ScrollingPane::GridItem> >&, float, float, float);
	virtual void renderItem(ScrollingPane::GridItem&, float);


	bool queryHoldTime(int*, int*);

	void scrollDownBy(float);
	void scrollUpBy(float);

	Vec3& getContentOffset();
	ScrollBar& getVerticleScrollBar();
	void refreshPane();

	int getColumns();
	int getRows();
	int getNumItems();
	void setNumItems(int);

	void onNavigate(int);

	bool shouldRenderSelected();
	void setRenderSelected(bool);

protected:
	ScrollingPane::GridItem getItemForPos(float, float, bool);
	void handleUserInput();
	void addDeltaPos(float, float, float, int);

	void translate(float, float);

	int flags;

	int columns;
	int rows;
	int numItems;

	int px, py;
	float fpx, fpy;

	float screenScale;
	float invScreenScale;


	IntRectangle bbox;
	IntRectangle itemRect;
	IntRectangle itemBbox;
	RectangleArea area;
	RectangleArea bboxArea;


	_Allocator dragDeltas;
	int dragState;
	Vec3 dragBeginPos;
	Vec3 dragBeginScreenPos;
	int dragTicks;
	float dragLastDeltaTimeStamp;
	Vec3 dragLastPos;

	float dx, dy;
	float friction;

	float dstx, dsty;


	bool dragging;
	bool decelerating;
	bool tracking;

	bool pagingEnabled;

	Vec3 _contentOffset;
	Vec3 _contentOffsetBeforeDeceleration;

	int lastEventTime;

	Vec3 decelerationVelocity;
	Vec3 minDecelerationPoint;
	Vec3 maxDecelerationPoint;

	float penetrationDeceleration;
	float penetrationAcceleration;

	Vec3 minPoint;
	Vec3 startPosition;
	Vec3 startTouchPosition;
	Vec3 startTimePosition;

	bool wasDeceleratingWhenTouchesBegan;
	bool firstDrag;

	float startTime;


	IntRectangle size;
	int lastFrame;

	bool _scrollEnabled;
	bool touchesHaveMoved;

	virtual void didEndDragging();
	virtual void didEndDecelerating();
	virtual void willBeginDecelerating();
	virtual void willBeginDragging();

	int te_moved;
	int te_ended;
	int te_highlight;
	int highlightTimer;
	int highlightStarted;
	ScrollingPane::GridItem highlightItem;

	std::vector<bool, std::allocator<bool> > selected;
	int selectedId;
	int _previousSelectedId;

	ScrollBar vScroll, hScroll;

	IntRectangle adjustedContentSize;
	void touchesBegan(float, float, int);
	void touchesMoved(float, float, int);
	void touchesEnded(float, float, int);
	void touchesCancelled(float, float, int);
	void beginTracking(float, float, int);
	void onHoldItem();

	void _onSelect(int);
	virtual bool onSelect(int, bool);


	Vec3& contentOffset();

	void startDecelerationAnimation(bool);
	void stopDecelerationAnimation();
	void stepThroughDecelerationAnimation(bool);

	void setContentOffset(float, float);
	void setContentOffset(Vec3);
	void setContentOffsetWithAnimation(Vec3, bool);
	void snapContentOffsetToBounds(bool);
	void adjustContentSize();


	bool isAllSet(int);
	bool isSet(int);
	bool isNotSet(int);

	void updateHorizontalScrollIndicator();
	void updateVerticalScrollIndicator();
	void hideScrollIndicators();
	void updateScrollFade(ScrollBar&);

	virtual void refreshItems();
private:
	Timer _timer;
	bool _doStepTimer;
	bool _wasDown;
	float _lx;
	float _ly;
	bool _refreshItems;
	bool _shouldRenderSelected;
};
