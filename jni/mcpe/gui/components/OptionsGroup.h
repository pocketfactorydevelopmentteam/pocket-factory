#pragma once
#include "GuiElementContainer.h"
#include <string>
#include <memory>
class MinecraftClient;
#include "ImageButton.h"
#include "../../options/Options.h"





class OptionsGroup : public GuiElementContainer {

public:
	OptionsGroup(std::string);
	virtual void setupPositions();
	virtual void render(MinecraftClient*, int, int);
	virtual std::default_delete<OptionButton> addOptionItem(const Options::Option*, MinecraftClient*);
	void addLoginItem(MinecraftClient*);
protected:
	virtual void createToggle(const Options::Option*, MinecraftClient*);
	virtual void createProgressSlider(const Options::Option*, MinecraftClient*);
	virtual void createStepSlider(const Options::Option*, MinecraftClient*);
	void createTextBox(const Options::Option*, MinecraftClient*);
	std::string label;
};

	typedef std::shared_ptr<OptionsGroup> OptionsGroupPtr;
