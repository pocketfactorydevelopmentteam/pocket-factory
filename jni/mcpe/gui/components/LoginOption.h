#pragma once
#include "GuiElementContainer.h"
#include "Button.h"
#include <memory>
#include "Label.h"
class MinecraftClient;

class LoginOption : public GuiElementContainer {
public:
	LoginOption(MinecraftClient*);

	virtual void tick(MinecraftClient*);

	virtual void setupPositions();

	virtual void mouseClicked(MinecraftClient*, int, int, int);

	virtual void mouseReleased(MinecraftClient*, int, int, int);

protected:
	std::shared_ptr<Button> _loginButton;
	std::shared_ptr<Button> _logoutButton;
	std::shared_ptr<Label> _statusMsg;
	Button* _selectedButton;
};
