#pragma once
#include "GuiElement.h"
#include <string>
#include <memory>
class Textures;
class Tessellator;
class TextureData;
struct IntRectangle;


class NinePatchDescription {
public:
	NinePatchDescription& transformUVForImage(const TextureData&);
	NinePatchDescription& transformUVForImageSize(int, int);

	float u0, u1, u2, u3;
	float v0, v1, v2, v3;
	float w, e, n, s;

	static NinePatchDescription createSymmetrical(int, int, const IntRectangle&, int, int);
private:
	NinePatchDescription(float, float, float, float, float, float, float, float, float, float, float, float);


	int imgW;
	int imgH;
};

class NinePatchLayer : public GuiElement {


public:
	NinePatchLayer(const NinePatchDescription&, const std::string&, Textures*, float, float);
	virtual ~NinePatchLayer();
	void setSize(float, float);

	void draw(Tessellator&, float, float);

	NinePatchLayer* exclude(int);
	NinePatchLayer* setExcluded(int);

	float getWidth();
	float getHeight();

private:
	void buildQuad(int);
	void getPatchInfo(int, int, float&, float&, float&, float&);

	void d(Tessellator&, const NinePatchLayer::CachedQuad&);

	float w, h;
	NinePatchDescription desc;
	std::string imageName;
	Textures* textures;
	int excluded;
public:
struct CachedQuad {
	float x0, x1, y0, y1, z;
	float u0, u1, v0, v1;
};	typedef NinePatchLayer::CachedQuad CachedQuad;
private:	NinePatchLayer::CachedQuad quads[9];
};

class NinePatchFactory {
public:
	NinePatchFactory(Textures*, const std::string&);

	NinePatchLayer* createSymmetrical(const IntRectangle&, int, int, float, float);

private:
	Textures* textures;
	std::string imageName;
	int width;
	int height;
};
