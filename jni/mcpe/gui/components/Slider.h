#pragma once
#include "GuiElement.h"
#include <memory>
#include <vector>
#include "../../options/Options.h"
#include "../../../platform/input/Controller.h"
class MinecraftClient;
enum SliderType { SliderProgress, SliderStep };



class Slider : public GuiElement {


public:
	Slider(MinecraftClient*, const Options::Option*, float, float);
	Slider(MinecraftClient*, const Options::Option*, const std::vector<int, std::allocator<int> >&);
	virtual void render(MinecraftClient*, int, int);

	virtual void mouseClicked(MinecraftClient*, int, int, int);

	virtual void mouseReleased(MinecraftClient*, int, int, int);

	virtual void tick(MinecraftClient*);

private:
	virtual void setOption(MinecraftClient*);


	SliderType sliderType;
	std::vector<int, std::allocator<int> > sliderSteps;
	bool mouseDownOnElement;
	float percentage;
	int curStepValue;
	int curStep;
	int numSteps;
	float progressMin;
	float progressMax;
	float speed;
	const Options::Option* option;
	Controller::StickDirection prevDirection;

	void updateStepPercentage();
	void processControllerInput(MinecraftClient*, int);
};

	typedef std::shared_ptr<Slider> SliderPtr;
