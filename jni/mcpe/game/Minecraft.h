#pragma once
#include "../App.h"
class GameMode;
#include "../network/http/MojangConnector.h"
#include <memory>
//#include "../network/RakNetInstance.h"
//#include "../network/NetEventCallback.h"
class User;
#include "../level/Level.h"
#include "../gui/screens/Screen.h"
#include <string>
#include "../util/Timer.h"
class LevelStorageSource;
//#include "../server/ServerCommandParser.h"
#include "../level/storage/LevelStorageState.h"
class LevelSettings;
class Player;
class Mob;
class ICreator;
class ServerCommandParser;





















class Minecraft : public App {

public:
	Minecraft(int, char**);
	virtual ~Minecraft();

	virtual void teardown();

	virtual void init();
	void init(const std::string&);

	bool supportNonTouchScreen();
	void toggleDimension();
	bool isCreativeMode();
	void setIsCreativeMode(bool);

	virtual LevelStorageState selectLevel(const std::string&, const std::string&, const LevelSettings&);
	virtual void setLevel(std::unique_ptr<Level, std::default_delete<Level> >&, const std::string&, Player*);

	LevelStorageSource* getLevelSource();

	bool isLookingForMultiplayer;
	void locateMultiplayer();
	void cancelLocateMultiplayer();
	void hostMultiplayer(int);
	void doActuallyRespawnPlayer();

	virtual void update();

	virtual void startFrame();
	virtual void updateGraphics();
	virtual void endFrame();

	virtual void tick(int, int);

	void tickInput();

	bool isOnlineClient();
	bool isOnline();

	virtual void leaveGame(bool, bool);

	double getLastFrameDuration();



	ICreator* getCreator();
	ServerCommandParser& getCommandParser();

	bool isPowerVR();
	bool isKindleFire(int);
	bool transformResolution(int*, int*);
	void lockForControl();
	void unlockControl();
	void rebuildLevel();

	virtual void play(const std::string&, float, float, float, float, float);
	virtual void playUI(const std::string&, float, float);
	virtual void updateSoundLocation(Mob*, float);

	static void resetInput();

	virtual bool isServerVisible();

	std::string getServerName();
	virtual void sendLocalMessage(const std::string&, const std::string&);

	virtual Player* getPlayer();
	virtual void onInternetUpdate();

	virtual void onPlayerLoaded(Player&);

private:
	static void* prepareLevel_tspawn(void*);

public:
	int width;
	int height;


	int commandPort;
	int reserved_d1, reserved_d2;
	float reserved_f1, reserved_f2;

	volatile bool pause;

	GameMode* gameMode;

	std::shared_ptr<MojangConnector> mojangConnector;

	std::unique_ptr<IRakNetInstance, std::default_delete<IRakNetInstance> > raknetInstance;
	std::unique_ptr<NetEventCallback, std::default_delete<NetEventCallback> > netCallback;

	int lastTime;
	int lastTickTime;
	float ticksSinceLastUpdate;
	int clientRandomId;

	User* user;
	std::unique_ptr<Level, std::default_delete<Level> > level;

	std::shared_ptr<Screen> screen;
	static int customDebugId;

	static const int CDI_NONE = 0;
	static const int CDI_GRAPHICS = 1;

	bool mouseGrabbed;



	std::string externalStoragePath;
	std::string externalCacheStoragePath;

	void setLeaveGame();
	void removeAllPlayers();

	void onUpdatedClient(int, int, int, int);

	virtual void vibrate(int);


protected:
	std::unique_ptr<Timer, std::default_delete<Timer> > timer;

	LevelStorageSource* storageSource;
	bool _running;
	bool _powerVr;

	std::string homeFolder;


	int ticks;

	bool _isCreativeMode;

	bool _shouldLeaveGame;


	std::unique_ptr<ServerCommandParser, std::default_delete<ServerCommandParser> > commandParser;

	double frameDuration, lastFrameStart;
};
