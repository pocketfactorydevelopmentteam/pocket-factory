#pragma once
#include "../gui/gui/PixelCalc.h"
class MinecraftClient;
class Options;



class Config {

public:
	void setScreenSize(int, int, float);








	int width;
	int height;

	float guiScale;
	float invGuiScale;
	int guiWidth;
	int guiHeight;

	PixelCalc pixelCalc;
	PixelCalc pixelCalcUi;

	MinecraftClient* minecraft;
	Options* options;
};







class IConfigListener {

public:
	virtual ~IConfigListener();
	virtual void onConfigChanged(const Config&) = 0;
};
