#pragma once
#include <functional>
#include <utility>
#include <memory>
#include <unordered_map>
#include <string>


struct ExternalServer {
	const int _id;
	std::string _name;
	std::string _address;
	int _port;
	ExternalServer();
	ExternalServer(const int, const std::string&, const std::string&, int);
};



class ExternalServerFile {
public:
	ExternalServerFile(const std::string&);
	void load();
	void save();
	const std::unordered_map<int, ExternalServer, std::hash<int>, std::equal_to<int>, std::allocator<std::pair<const int, ExternalServer> > >& getExternalServers();
	void addServer(const std::string&, const std::string&, int);
	void editServer(int, const std::string&, const std::string&, int);
	void removeSevrer(int);

protected:
	std::unordered_map<int, ExternalServer, std::hash<int>, std::equal_to<int>, std::allocator<std::pair<const int, ExternalServer> > > _externalServers;
	std::string _externalServersFilePath;
};
