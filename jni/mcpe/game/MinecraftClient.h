#pragma once
#include "./Minecraft.h"
#include "../gui/screens/ScreenChooser.h"
#include <memory>
#include "../texture/TextureAtlas.h"
#include <vector>
class LevelRenderer;//#include "../renderer/game/LevelRenderer.h"
class GameRenderer;//#include "../renderer/game/GameRenderer.h"
class ParticleEngine;//#include "../particle/ParticleEngine.h"
#include "ExternalServersFile.h"
#include "../texture/Textures.h"
#include "../gui/gui/Font.h"
#include "../input/MouseHandler.h"
#include "../input/IInputHolder.h"
class Mob;
class LocalPlayer;
#include "../gui/gui/Gui.h"
#include "../options/Options.h"
class SoundEngine;//#include "../sound/SoundEngine.h"
#include "../gui/gui/PixelCalc.h"
#include <mutex>
#include <string>
#include "../level/storage/LevelStorageState.h"
class LevelSettings;
class Player;
#include "../level/Level.h"
#include "../gui/screens/Screen.h"
struct PingedCompatibleServer;
class BuildActionIntention;

class MinecraftClient : public Minecraft {


public:
	static const int INTERACTION_FREQ_MS;
	MinecraftClient(int, char**);
	virtual ~MinecraftClient();

	virtual void init();
	virtual void teardown();

	virtual void startFrame();
	virtual void updateGraphics();
	virtual void endFrame();

	virtual bool handleBack(bool);
	virtual void handleBack();

	virtual void onLowMemory();

	virtual void play(const std::string&, float, float, float, float, float);
	virtual void playUI(const std::string&, float, float);

	void setScreen(Screen*);
	std::unique_ptr<ScreenChooser, std::default_delete<ScreenChooser> > screenChooser;

	void handleBackNoReturn();

	virtual bool useTouchscreen();
	bool supportNonTouchScreen();
	virtual void setSize(int, int, float);

	void optionUpdated(const Options::Option*, bool);

	void optionUpdated(const Options::Option*, float);

	void optionUpdated(const Options::Option*, int);

	void _reloadFancy(bool);

	virtual LevelStorageState selectLevel(const std::string&, const std::string&, const LevelSettings&);
	Options& getOptions();
	virtual void setTextboxText(const std::string&);

	void onUpdatedClient(int, int, int, int);

	void rebuildLevel();
	bool isKindleFire(int);

	bool transformResolution(int*, int*);

	virtual void onAppFocusLost();
	virtual void onAppFocusGained();

	virtual void onAppSuspended();

	virtual void onAppResumed();

	virtual void onInternetUpdate();

	virtual void onPlayerLoaded(Player&);

	void updateScheduledScreen();

	virtual void leaveGame(bool, bool);

	virtual void setLevel(std::unique_ptr<Level, std::default_delete<Level> >&, const std::string&, Player*);


	std::shared_ptr<Screen>& getScreen();



	GameRenderer& getGameRenderer();



	Gui& getGui();

	bool joinMultiplayer(const PingedCompatibleServer&, bool);
	virtual void vibrate(int);

	void locateMultiplayer();
	virtual bool isServerVisible();
	virtual Player* getPlayer();
	virtual LocalPlayer* getLocalPlayer();
	static void resetInput();
	bool useController();
	PixelCalc& getPixelCalc();
	PixelCalc& getPixelCalcUI();

	void lockForControl();

	void unlockControl();

private:
	void initGLStates();
	void restartServer();

	void updateStats();









	bool _verbose;
	int _frames;
	int _lastTickMs;

	static bool _hasInitedStatics;

	static std::shared_ptr<TextureAtlas> _terrainTextureAtlas;
	static std::shared_ptr<TextureAtlas> _itemsTextureAtlas;

	std::vector<bool, std::allocator<bool> > _handleBackEvent;

	virtual void audioEngineOn();
	virtual void audioEngineOff();
	virtual void muteAudio();
	virtual void unMuteAudio();

	virtual void tick(int, int);
	void tickInput();

	void handleMouseDown(int, bool);
	void handleMouseClick(int);

	void _reloadInput();
	void handleBuildAction(BuildActionIntention*);

	virtual void sendLocalMessage(const std::string&, const std::string&);
	void grabMouse();
	void releaseMouse();

	void initFoliageAndTileTextureTessellator();
	void _loadMaterials();
public:
	std::unique_ptr<LevelRenderer, std::default_delete<LevelRenderer> > levelRenderer;
	std::unique_ptr<GameRenderer, std::default_delete<GameRenderer> > gameRenderer;
	std::unique_ptr<ParticleEngine, std::default_delete<ParticleEngine> > particleEngine;
	std::unique_ptr<ExternalServerFile, std::default_delete<ExternalServerFile> > _externalServer;
	std::unique_ptr<Textures, std::default_delete<Textures> > textures;
	std::unique_ptr<Font, std::default_delete<Font> > font;
	std::unique_ptr<MouseHandler, std::default_delete<MouseHandler> > mouseHandler;
	std::unique_ptr<IInputHolder, std::default_delete<IInputHolder> > inputHolder;
	Mob* cameraTargetPlayer;
private:
	LocalPlayer* player;
	std::unique_ptr<Gui, std::default_delete<Gui> > gui;
	std::unique_ptr<Options, std::default_delete<Options> > _options;
	std::unique_ptr<SoundEngine, std::default_delete<SoundEngine> > soundEngine;

	bool hasFocus;

	int missTime;
	int lastInteractionTime;

	bool screenIsTicking;
	bool hasScheduledScreen;
	Screen* scheduledScreen;

	bool _supportsNonTouchscreen;
	std::unique_ptr<PixelCalc, std::default_delete<PixelCalc> > pixelCalc;
	std::unique_ptr<PixelCalc, std::default_delete<PixelCalc> > pixelCalcUi;
	bool hasRunTickingTextureTicks;
};
