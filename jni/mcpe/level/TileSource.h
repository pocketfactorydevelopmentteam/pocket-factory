#pragma once
class Player;
class Level;
class ChunkSource;
#include <memory>
#include <functional>
#include <unordered_set>
class LevelChunk;
class TileTickingQueue;
class Entity;
#include "../CommonType.h"
#include "../phys/AABB.h"
#include <vector>
#include "TileSourceListener.h"
class ChunkPos;
class Tile;
class Material;
class LightLayer;
class Mob;
struct Bounds;
class Biome;
class HitResult;
class Vec3;
#include "../tile/entity/TileEntity.h"
#include "../phys/TilePos.h"



class TileSource {



	typedef TileSourceListener Listener;
	typedef std::unordered_set<TileSourceListener *, std::hash<TileSourceListener *>, std::equal_to<TileSourceListener *>, std::allocator<TileSourceListener *> > ListenerSet;

public:
	const Player* player;

	const bool allowUnpopulatedChunks, publicSource;

	TileSource(Level&, ChunkSource*, bool, bool);

	TileSource(Player&);

	TileSource(const TileSource&);
	TileSource& operator=(const TileSource&);

	virtual ~TileSource();

	Level& getLevel() const;
	const Level& getLevelConst() const;

	void setTickingQueue(TileTickingQueue&);

	TileTickingQueue& getTickQueue() const;

	void addListener(TileSource::Listener*);



	void removeListener(TileSource::Listener*);



	ChunkSource* getChunkSource() const;



	LevelChunk* getChunk(int, int);
	LevelChunk* getChunk(const ChunkPos&);

	LevelChunk* getWritableChunk(const ChunkPos&);

	LevelChunk* getChunkAt(const TilePos&);
	LevelChunk* getChunkAt(int, int, int);

	FullTile getTile(int, int, int);
	FullTile getTile(const TilePos&);

	Tile* getTilePtr(const TilePos&);

	bool isEmptyTile(const TilePos&);

	TileID getTopTile(int, int&, int);







	Height getTopSolidBlock(const TilePos&, bool);

	const Material* getMaterial(const TilePos&);
	Height getHeightmap(const TilePos&);
	TilePos getHeightmapPos(const TilePos&);
	Height findHighestNonEmptyTileAt(const TilePos&);

	uint_fast32_t getBrightnessPropagate(const LightLayer&, const TilePos&);
	uint_fast32_t getLightColor(const TilePos&, int);

	float getBrightness(const TilePos&);

	Brightness getBrightness(const LightLayer&, const TilePos&);

	Brightness getRawBrightness(const TilePos&, bool);

	void setBrightness(const LightLayer&, const TilePos&, Brightness);

	bool canSeeSky(const TilePos&);

	DataID getData(const TilePos&);

	FullTile getTileAndData(const TilePos&);

	bool isSolidBlockingTile(const TilePos&);

	bool mayPlace(TileID, const TilePos&, FacingID, Mob*, bool, Entity*);

	bool hasTile(const TilePos&);


	bool hasChunksAt(const Bounds&);
	bool hasChunksAt(const TilePos&, int);

	bool containsAnyLiquid(const AABB&);
	bool containsFireTile(const AABB&);
	bool containsMaterial(const AABB&, const Material*);
	bool containsLiquid(const AABB&, const Material*);


	bool setTileAndData(const TilePos&, FullTile, int);


	bool findNextTopSolidTileUnder(TilePos&);

	TileEntity* getTileEntity(const TilePos&);

	void tileEntityChanged(TileEntity&);

	void tileEvent(const TilePos&, int, int);

	EntityList& getEntities(Entity*, const AABB&);
	EntityList& getEntities(int, const AABB&, Entity*);
	Entity* getNearestEntityOfType(Entity*, float, float, float, float, int);
	bool isUnobstructedByEntities(const AABB&, Entity*);

	Biome& getBiome(const TilePos&);
	void setGrassColor(int, const TilePos&, int);
	int getGrassColor(const TilePos&);

	void updateNeighborsAt(const TilePos&, TileID);

	std::vector<AABB, std::allocator<AABB> >& getCubes(const AABB&, float*, bool);

	void clip(HitResult&, const Vec3&, const Vec3&, bool, bool);

	float getSeenPercent(const Vec3&, const AABB&);

	void updateLightIfOtherThan(const LightLayer&, const TilePos&, Brightness);

	bool isTopSolidBlocking(const TilePos&);

	bool isTopSolidBlocking(Tile*, DataID);

	ChunkSource* getSource() const;



	void fireAreaChanged(const TilePos&, const TilePos&);
	void fireTilesDirty(int, int, int, int, int, int);
	void fireTileChanged(const TilePos&, FullTile, FullTile, int);
	void fireBrightnessChanged(const TilePos&);
	void fireTileEntityChanged(TileEntity&);
	void fireTileEntityRemoved(std::unique_ptr<TileEntity, std::default_delete<TileEntity> >&);
	void fireTileEvent(int, int, int, int, int);

	void runLightUpdates(const LightLayer&, const TilePos&, const TilePos&);

	void _fireColumnDirty(int, int, int, int, int);


	const TilePos getTopRainTilePos(const TilePos&);
	bool shouldFreezeIgnoreNeighbors(const TilePos&);
	bool shouldSnow(const TilePos&, bool);
	bool isHumidAt(const TilePos&);

	void onChunkDiscarded(LevelChunk&);


	Tile* getTilePtr(int, int, int);
	Height getTopSolidBlock(int, int, bool);
	Height getHeightmap(int, int);
	const Material* getMaterial(int, int, int);
	float getBrightness(int, int, int);
	Brightness getBrightness(const LightLayer&, int, int, int);
	Brightness getRawBrightness(int, int, int, bool);
	void setBrightness(const LightLayer&, int, int, int, Brightness);
	bool canSeeSky(int, int, int);
	DataID getData(int, int, int);
	FullTile getTileAndData(int, int, int);
	bool isEmptyTile(int, int, int);
	bool isSolidRenderTile(int, int, int);
	bool isSolidRenderTile(const TilePos&);
	bool isSolidBlockingTile(int, int, int);
	bool hasTile(int, int, int);
	bool hasChunksAt(const AABB&);
	bool hasChunksAt(const TilePos&, const TilePos&);
	bool hasChunksAt(int, int, int, int);
	bool setTile(int, int, int, TileID, int);
	bool setTileNoUpdate(int, int, int, TileID);
	bool setTileAndDataNoUpdate(int, int, int, FullTile);
	bool setTileAndData(int, int, int, FullTile, int);
	bool setTileAndData(int, int, int, TileID, DataID, int);


	bool setTileAndData(const TilePos&, TileID, DataID, int);


	bool removeTile(int, int, int);
	TileEntity* getTileEntity(int, int, int);
	void tileEvent(int, int, int, int, int);
	bool hasNeighborSignal(int, int, int);

	bool isTopSolidBlocking(int, int, int);


protected:
	Level* level;
	ChunkSource* source;
	TileSource::ListenerSet listeners;

	LevelChunk* lastChunk;

	TileTickingQueue* tickQueue;

	EntityList _tempEntityList;
	std::vector<AABB, std::allocator<AABB> > _tempCubeList;

	void _neighborChanged(const TilePos&, const TilePos&, TileID);

	void _tileUpdated(int, int, int, TileID, int, int);

	bool _shouldFireEvent(LevelChunk&) const;
	bool shouldFreeze(const TilePos&, bool);
	const bool isWaterAt(const TilePos&);
	void _tileChanged(const TilePos&, FullTile, FullTile, int);
	Brightness getSkyDarken();
};
