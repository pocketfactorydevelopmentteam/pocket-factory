#pragma once


enum class Dimension : int { Overworld, Flat, Nether, Undefined };
