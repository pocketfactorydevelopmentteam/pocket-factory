#pragma once
class TileSource;
class TilePos;
#include "../CommonType.h"
#include "../tile/entity/TileEntity.h"
#include <memory>




class TileSourceListener {

public:
	virtual ~TileSourceListener();



	virtual void onSourceCreated(TileSource*);
	virtual void onSourceDestroyed(TileSource*);
	virtual void onTilesDirty(TileSource*, int, int, int, int, int, int);
	virtual void onAreaChanged(TileSource&, const TilePos&, const TilePos&);
	virtual void onTileChanged(TileSource*, const TilePos&, FullTile, FullTile, int);

	virtual void onBrightnessChanged(TileSource&, const TilePos&);



	virtual void onTileEntityChanged(TileSource&, TileEntity&);
	virtual void onTileEntityRemoved(TileSource&, std::unique_ptr<TileEntity, std::default_delete<TileEntity> >&);

	virtual void onTileEvent(TileSource*, int, int, int, int, int);
};
