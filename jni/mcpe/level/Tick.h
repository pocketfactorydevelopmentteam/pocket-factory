#pragma once




struct Tick {

	static const int MAX_TICK_ID = 16777215;

	Tick();




	Tick(const Tick&);



	const Tick& operator=(const Tick&);




	void operator++();



	bool operator<(const Tick&) const;




private:
	int tickID;
};
