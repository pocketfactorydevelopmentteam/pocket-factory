#pragma once
class Level;
#include "../phys/ChunkPos.h"
#include <functional>
#include <memory>
#include <unordered_set>
class Mob;
class TileSource;
class Vec3;
#include <string>
class Random;
class Player;
class MobSpawnerData;
#include "../phys/TilePos.h"





class MobFactory {

public:
	static Mob* CreateMob(int, TileSource&, const Vec3&, Vec3*);

	static std::string GetMobNameID(int);

	static const float MIN_SPAWN_DISTANCE, MAX_SPAWN_DISTANCE;

	bool spawnEnemies;

	MobFactory(Level&);

	void postProcessSpawnMobs(TileSource&, int, int, Random*);

	bool addMob(Mob&, bool);

	bool isSpawnPositionOk(int, TileSource&, const TilePos&);

	int getMobBaseTypeCount(int, bool);

	int tick(Player&);

	bool popCapAllows(Player&, const MobSpawnerData&, bool) const;


private:
	static int _categoryID(int);

	Level& level;

	int baseTypeCount[8];


	float mobsPerChunkSurface[4];
	float mobsPerChunkUnderground[4];

	void _spawnMobCluster(Player&, const TilePos&, bool);
	TilePos _getRingPosition(Player&, float, float, int, int) const;

	void _updateBaseTypeCount(Player&);

	static const std::unordered_set<ChunkPos, std::hash<ChunkPos>, std::equal_to<ChunkPos>, std::allocator<ChunkPos> > spawnRingOffsets;
};
