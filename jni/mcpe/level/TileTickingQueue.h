#pragma once
//#include "TickNextTickData.h"
#include <memory>
#include <vector>
#include <functional>
#include <queue>
#include "../util/Random.h"
class TileSource;
class TilePos;
class Player;
class TickNextTickData;
class TileTickingQueue {

public:
	static const int MAX_TICK_TILES_PER_TICK = 100;




	typedef std::priority_queue<TickNextTickData, std::vector<TickNextTickData, std::allocator<TickNextTickData> >, std::greater<TickNextTickData> > TickDataSet;

	TileTickingQueue();

	void add(TileSource*, const TilePos&, int, int);

	bool tickPendingTicks(long int);
	void tickAllPendingTicks();

	bool isInstaticking() const;



	void reassignTicksFrom(Player&, const PlayerList&);


protected:
	TileTickingQueue::TickDataSet tickQueue;
	long int currentTime;
	bool instaTick;

	long int _tickToCurrentTime(int);

	Random random;
};
