#pragma once




	const Height LEVEL_HEIGHT = 128;
	const int CHUNK_WIDTH = 16;
	const int CHUNK_DEPTH = 16;
	const int CHUNK_COLUMNS = 256;
	const int CHUNK_BLOCK_COUNT = 32768;

	const int RENDERCHUNK_SIDE = 16;
	const int RENDERCHUNK_COLUMNS = 256;
	const int RENDERCHUNK_VOLUME = 4096;

	const int LEVEL_GEN_DEPTH_BITS = 7;
	const int LEVEL_GEN_DEPTH_BITS_PLUS_FOUR = 11;
	const int LEVEL_GEN_DEPTH = 128;
	const int LEVEL_GEN_DEPTH_MINUS_ONE = 127;

	const Height SEA_LEVEL = 63;
