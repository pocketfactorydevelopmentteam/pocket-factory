#pragma once
#include "TileSourceListener.h"
#include "../AppPlatformListener.h"
#include "../phys/ChunkPos.h"
#include <array>
#include <functional>
#include <utility>
#include <memory>
#include "../entity/Entity.h"
#include "../CommonType.h"
class Player;
class IRakNetInstance;
class NetEventCallback;
#include "../util/Random.h"
#include "../phys/HitResult.h"
class Minecraft;
class LevelListener;
#include "storage/LevelStorage.h"
#include "storage/LevelData.h"
#include <unordered_set>
#include "Tick.h"
#include "MobFactory.h"
#include "Dimension.h"
#include "TileSource.h"
class ChunkSource;//#include "chunk/ChunkSource.h"
#include "LightUpdate.h"
#include "TileTickingQueue.h"
#include <string>
class LevelSettings;
class Mob;
#include "../phys/Vec3.h"
#include "../renderer/Color.h"
class TilePos;
class AABB;
class Material;
class Path;
class Particle;
#include "../entity/ParticleType.h"
class LightLayer;
class LevelChunk;
#include "storage/StorageVersion.h"
#include "GeneratorType.h"
class Entity;



class _TickPtr {

	virtual void invalidate() = 0;

public:
	virtual ~_TickPtr();
};



	typedef std::unordered_map<int, Entity *, std::hash<int>, std::equal_to<int>, std::allocator<std::pair<const int, Entity *> > > EntityMap;
	typedef std::vector<LevelListener *, std::allocator<LevelListener *> > ListenerList;
	typedef std::vector<LightUpdate, std::allocator<LightUpdate> > LightUpdateList;

struct PRInfo {
	PRInfo(Entity*, int);
	Entity* e;
	int ticks;
};
typedef std::vector<PRInfo, std::allocator<PRInfo> > PendingList;

struct AdventureSettings {
	AdventureSettings();

	bool doTickTime;
	bool noPvP;
	bool noPvM;
	bool noMvP;
	bool immutableWorld;
	bool showNameTags;
};

class Level : public TileSourceListener, public AppPlatformListener {



	typedef std::unordered_set<TileSource *, std::hash<TileSource *>, std::equal_to<TileSource *>, std::allocator<TileSource *> > RegionSet;


public:
	static const std::array<ChunkPos, 57> tickingChunksOffset;

	static const int MAX_LEVEL_SIZE = 32000000;

	static const int CLOUD_HEIGHT = 128;
	static const int NOON_TICKS = 5000;
	static const int MAX_BRIGHTNESS = 15;
	static const int TICKS_PER_DAY = 19200;
	static const int TICKS_PER_MOON_MONTH = 153600;
	static const int MIDDLE_OF_NIGHT_TIME = 12000;
	static const float MOON_BRIGHTNESS_PER_PHASE[];

	static const int genDepthBits = 7;
	static const int genDepthBitsPlusFour = 11;
	static const int genDepth = 128;
	static const int genDepthMinusOne = 127;

	Level(Minecraft&, std::unique_ptr<LevelStorage, std::default_delete<LevelStorage> >, const std::string&, const LevelSettings&, bool);
	virtual ~Level();

	void _init(const std::string&, const LevelSettings&);

	void _createLevelSources();

	void loadPlayer(Player&, bool);

	virtual bool isNaturalDimension();



	Dimension getDimension() const;



	MobFactory& getMobSpawner() const;
	TileTickingQueue& getTickQueue() const;

	int getDifficulty() const;

	int getLightsToUpdate();
	void updateLights();
	void setUpdateLights(bool);

	virtual bool addEntity(Entity*);
	virtual void addPlayer(Player*);

	Entity* getEntity(int, bool);
	Mob* getMob(int);
	Player* getPlayer(const std::string&);
	Player* getPlayer(int);
	Player* getPlayerByClientId(int);
	Player* getLocalPlayer();
	Player* getRandomPlayer();

	void addListener(LevelListener*);
	void removeListener(LevelListener*);

	bool isDay();
	float getTimeOfDay(float);

	TileSource* getGlobalTileSource();



	ChunkSource& getChunkSource() const;

	float getSunAngle(float);
	float getSkyDarken(float);
	int getMoonPhase() const;
	float getMoonBrightness() const;
	Vec3 getSunlightDirection(float);

	Color getCloudColor(float);
	Color getSkyColor(TileSource&, const TilePos&, float);

	Color getSkyColor(const Entity&, float);
	Color getSunriseColor(float);
	float getSunIntensity(float, const Entity&, float);

	float getStarBrightness(float);

	void tickEntities();
	virtual void tick();

	virtual void directTickEntities();

	void animateTick(Entity*);

	void explode(Entity*, float, float, float, float);
	void explode(Entity*, float, float, float, float, bool);

	bool checkAndHandleWater(const AABB&, const Material*, Entity*);
	void extinguishFire(TileSource&, int, int, int, FacingID);

	bool isDayCycleActive();


	void setDayCycleActive(bool);

	bool isNew();

	int getSeaLevel();

	bool mayInteract(Player*, int, int, int);

	Path* findPath(Entity*, Entity*, float, bool, bool, bool, bool);
	Path* findPath(Entity*, int, int, int, float, bool, bool, bool, bool);

	Player* getNearestPlayer(Entity*, float);
	Player* getNearestPlayer(float, float, float, float);


	virtual void updateSleepingPlayerList();
	long int getTime() const;
	int setTime(long int);
	RandomSeed getSeed();
	const TilePos& getSharedSpawnPos();
	void setDefaultSpawn(const TilePos&);
	const TilePos& getDefaultSpawn() const;
	void setSpawnSettings(bool, bool);
	virtual void setDifficulty(int);

	LevelStorage* getLevelStorage();
	LevelData* getLevelData();

	void saveLevelData();

	void saveGameData();
	void savePlayers();

	Particle* addParticle(ParticleType, const Vec3&, float, float, float, int);
	void playSound(Entity*, const std::string&, float, float);
	void playSound(float, float, float, const std::string&, float, float);

	void levelEvent(Mob*, int, int, int, int, int);

	void broadcastEntityEvent(Entity*, char);

	void awakenAllPlayers();
	void removeAllPlayers();

	std::string getPlayerNames();

	virtual void runLightUpdates(TileSource&, const LightLayer&, const TilePos&, const TilePos&);

	virtual void onSourceCreated(TileSource*);
	virtual void onSourceDestroyed(TileSource*);

	virtual void onNewChunkFor(Player&, LevelChunk&);
	virtual void onChunkLoaded(LevelChunk&);

	void onChunkDiscarded(LevelChunk&);

	void _chunkDirty(TileSource*, LevelChunk*);

	virtual void onTileChanged(TileSource*, const TilePos&, FullTile, FullTile, int);

	virtual void removeEntity(std::unique_ptr<Entity, std::default_delete<Entity> >&&);
	virtual void removeEntity(Entity&);

	void forceRemoveEntity(Entity&);


	void registerTemporaryPointer(_TickPtr*);



	void unregisterTemporaryPointer(_TickPtr*);



	bool destroyTile(TileSource&, const TilePos&, bool);

	void upgradeStorageVersion(StorageVersion);

	virtual void onAppSuspended();

	virtual void onAppResumed();

	bool isUpdatingLights() const;



	const EntityMap& getEntityIdMap() const;
	void _destroyEffect(const TilePos&, FullTile, const Vec3&);

	void destroyEffect(TileSource&, int, int, int, const Vec3&);

protected:
	void tileUpdated(int, int, int, int);

	void tickTemporaryPointers();


private:
	void _syncTime(long int);
public:
	bool isClientSide;

	AdventureSettings adventureSettings;

	int isNightMode() const;
	void setNightMode(bool);

	EntityMap entityIdLookup;
	OwnedEntitySet pendingEntitiesToRemove;
	bool updatingTileEntities;
	PlayerList players;

	Brightness skyDarken;

	IRakNetInstance* raknetInstance;
	NetEventCallback* netEventCallback;
	Random random;

	bool ultraWarm;
	bool hasCeiling;
	float brightnessRamp[16];
	bool isRaining;





	HitResult hitResult;


protected:
	int difficulty;

	Minecraft& minecraft;

	bool isFindingSpawn;

	void _updateLightRamp();

	ListenerList _listeners;
	std::unique_ptr<LevelStorage, std::default_delete<LevelStorage> > levelStorage;
	LevelData levelData;

	Level::RegionSet regions;

	bool allPlayersAreSleeping;
	bool _nightMode;

	bool _lightUpdateRunning;
	Tick currentTick;

	std::unique_ptr<MobFactory, std::default_delete<MobFactory> > mobFactory;
private:
	Dimension dimension;
	std::unique_ptr<TileSource, std::default_delete<TileSource> > globalTileSource;

	std::unique_ptr<ChunkSource, std::default_delete<ChunkSource> > chunkSource;

	LightUpdateList _lightUpdates;
	int availableImmediateLightUpdates;

	bool _isNew;

	double _lastSaveDataTime;
	PendingList _pendingPlayerRemovals;

	int lastTimePacketSent;

	std::unique_ptr<TileTickingQueue, std::default_delete<TileTickingQueue> > tickingQueue;

	std::unordered_set<_TickPtr *, std::hash<_TickPtr *>, std::equal_to<_TickPtr *>, std::allocator<_TickPtr *> > _tempPointers;


public:
	bool tearingDown;
private:
	ChunkSource* _createGenerator(GeneratorType);

	void _saveSomeDirtyChunks();
	void _saveDirtyChunks();

	void _cleanupDisconnectedPlayers();
	int getOldSkyDarken(float);
};
