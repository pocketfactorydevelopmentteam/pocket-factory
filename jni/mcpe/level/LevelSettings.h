#pragma once
#include "../../CommonTypes.h"
#include "Dimension.h"
#include "GeneratorType.h"
#include "TilePos.h"
#include <string>
#include <memory>



class LevelSettings {


public:
	const RandomSeed seed;
	const int gameType;
	const Dimension dimension;
	const GeneratorType generator;

	LevelSettings();




	LevelSettings(RandomSeed, int, Dimension, GeneratorType, const TilePos&);









	const TilePos* getDefaultSpawn() const;



	RandomSeed getSeed() const;



	int getGameType() const;







	static int validateGameType(int);








	static std::string gameTypeToString(int);





private:
	TilePos defaultSpawn;
};
