#pragma once
class LightLayer;
#include "../phys/TilePos.h"
class TileSource;








class LightUpdate {

public:
	const LightLayer* layer;
	TilePos min, max;
	TileSource* region;

	LightUpdate(TileSource&, const LightLayer&, const TilePos&, const TilePos&);

	void operator=(const LightUpdate*);

	void update();

	void expandToContain(const TilePos&);
	void expandToContain(const TilePos&, const TilePos&);

	bool expandIfCloseEnough(TilePos, TilePos);
};
