#pragma once
#include <string>
#include <memory>
namespace RakNet { class BitStream; };
	typedef std::__compressed_pair<RegionFile *, std::default_delete<RegionFile> >::_T2_reference FreeSectorMap;

class RegionFile {

public:
	RegionFile(const std::string&);
	virtual ~RegionFile();

	bool open();
	bool readChunk(int, int, RakNet::BitStream**);
	bool writeChunk(int, int, RakNet::BitStream&);
private:
	bool write(int, RakNet::BitStream&);
	void close();

	FILE* file;
	std::string filename;
	int* offsets;
	int* emptyChunk;
	FreeSectorMap sectorFree;
};
