#pragma once
class LevelData;
class ChunkSource;
#include "StorageVersion.h"
#include <string>
#include <memory>
#include "LevelStorageState.h"
#include "../../nbt/CompoundTag.h"
class Player;









class LevelStorage {


public:
	virtual ~LevelStorage();

	virtual bool loadLevelData(LevelData&) = 0;

	virtual ChunkSource* createChunkStorage(ChunkSource*, StorageVersion) = 0;

	virtual void saveLevelData(LevelData&) = 0;

	virtual const std::string& getFullPath() const = 0;

	virtual void saveData(const std::string&, const std::string&);

	virtual std::string loadData(const std::string&);

	virtual LevelStorageState getState() const = 0;

	virtual std::unique_ptr<CompoundTag, std::default_delete<CompoundTag> > loadPlayerData(const std::string&);
	std::unique_ptr<CompoundTag, std::default_delete<CompoundTag> > loadPlayerData(const Player&);

	virtual void save(Player&);
};
