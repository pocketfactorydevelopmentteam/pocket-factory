#pragma once
#include "../../phys/Vec3.h"
class Player;







class PlayerData {
public:
	void loadPlayer(Player*) const;

	Vec3 pos;
	Vec3 motion;
	float xRot, yRot;

	float fallDistance;
	short onFire;
	short airSupply;
	bool onGround;

	int inventorySlots[9];
};
