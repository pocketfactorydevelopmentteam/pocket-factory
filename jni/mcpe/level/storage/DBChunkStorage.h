#pragma once
#include "../ChunkPos.h"
class LevelChunk;
#include "../../../../src-deps/leveldb/include/leveldb/slice.h"
#include "../chunk/ChunkSource.h"
#include "../../../AppPlatformListener.h"
#include "../../../util/MemoryTracker.h"
#include "../../../../src-deps/leveldb/include/leveldb/write_batch.h"
#include "../../../platform/threading/ThreadLocal.h"
#include <string>
#include <memory>
#include <vector>
#include <functional>
#include <unordered_set>
#include <utility>
#include <unordered_map>
#include <atomic>
class ChunkViewSource;
class DBChunkStorage : public ChunkSource, public AppPlatformListener, public MemoryTracker {







	typedef std::__pointer_type<std::shared_ptr<leveldb::WriteBatch>, std::allocator<std::shared_ptr<leveldb::WriteBatch> > > BufferPtr;
	typedef std::vector<std::shared_ptr<leveldb::WriteBatch>, std::allocator<std::shared_ptr<leveldb::WriteBatch> > > BufferPool;
public:
	DBChunkStorage(ChunkSource*, std::unordered_set<std::basic_string<char> *, std::hash<std::basic_string<char> *>, std::equal_to<std::basic_string<char> *>, std::allocator<std::basic_string<char> *> >::value_type&&);

	virtual ~DBChunkStorage();

	virtual void loadChunk(LevelChunk&);

	virtual bool postProcess(ChunkViewSource&);


	virtual bool saveLiveChunk(LevelChunk&);

	virtual void acquireDiscarded(ChunkSource::ChunkPtr&&);

	virtual void hintDiscardBatchBegin();
	virtual void hintDiscardBatchEnd();

	virtual void onLowMemory();

	virtual void compact();

	virtual MemoryStats getStats() const;


private:
	static ThreadLocal<leveldb::WriteBatch> threadBatch;
	static const std::unordered_set<leveldb::WriteBatch *, std::hash<leveldb::WriteBatch *>, std::equal_to<leveldb::WriteBatch *>, std::allocator<leveldb::WriteBatch *> >::key_type& threadBuffer;
public:
struct ChunkKey {
	ChunkPos pos;

	ChunkKey(const ChunkPos&);
	ChunkKey(const LevelChunk&);

	leveldb::Slice asSlice() const;
};
private:
	std::unordered_set<std::basic_string<char> *, std::hash<std::basic_string<char> *>, std::equal_to<std::basic_string<char> *>, std::allocator<std::basic_string<char> *> >::value_type&& storage;
	DBChunkStorage::BufferPool bufferPool;
	std::vector<std::unique_ptr<LevelChunk, std::default_delete<LevelChunk> >, std::allocator<std::unique_ptr<LevelChunk, std::default_delete<LevelChunk> > > > discardBatch;
	std::unordered_set<ChunkPos, std::hash<ChunkPos>, std::equal_to<ChunkPos>, std::allocator<ChunkPos> > liveChunksBeingSaved;
	std::unordered_map<ChunkPos, std::unique_ptr<LevelChunk, std::default_delete<LevelChunk> >, std::hash<ChunkPos>, std::equal_to<ChunkPos>, std::allocator<std::pair<const ChunkPos, std::unique_ptr<LevelChunk, std::default_delete<LevelChunk> > > > > discardedWhileLiveSaved;
	bool batch;

	DBChunkStorage::BufferPtr _getBuffer();

	void _writeBatch();
	void _serializeChunk(LevelChunk&, const std::unordered_set<leveldb::WriteBatch *, std::hash<leveldb::WriteBatch *>, std::equal_to<leveldb::WriteBatch *>, std::allocator<leveldb::WriteBatch *> >::value_type&, int);

	std::atomic<int> pending;
};
