#pragma once


enum class StorageVersion : int { Unknown, OldV1, OldV2, OldV3, LevelDB1 };
