#pragma once
#include "../chunk/ChunkSource.h"
#include "../levelgen/synth/PerlinSimplexNoise.h"
#include <string>
#include <memory>
#include "StorageVersion.h"
#include "RegionFile.h"
#include <mutex>
class LevelStorage;
#include "../chunk/LevelChunk.h"
class Level;
class ChunkPos;
class LegacyChunkStorage : public ChunkSource {



public:
	LegacyChunkStorage(ChunkSource*, LevelStorage&, StorageVersion);

	virtual ~LegacyChunkStorage();

	virtual void loadChunk(LevelChunk&);

	virtual bool saveLiveChunk(LevelChunk&);

	virtual void acquireDiscarded(ChunkSource::ChunkPtr&&);




private:
	bool done;

	const PerlinSimplexNoise grassNoise;

	const std::string levelPath, importedChunksPath;

	StorageVersion storageVersion;

	std::unique_ptr<RegionFile, std::default_delete<RegionFile> > regionFile, entitiesFile;
	StorageVersion loadedStorageVersion;

	_Allocator chunkEntities, chunkTileEntities;

	std::mutex regionFileMutex, chunkMapMutex;

	bool _openRegionFile();
	void _loadEntities(Level&);

	void _markChunkAsImported(const ChunkPos&);
	bool _isImported(const ChunkPos&);
	void _collectInfo(bool*);
	bool _loadChunk(LevelChunk&);
};
