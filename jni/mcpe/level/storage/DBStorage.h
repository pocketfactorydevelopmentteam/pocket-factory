#pragma once
#include "LevelStorage.h"
#include "../../../../src-deps/leveldb/include/leveldb/db.h"
#include <memory>
#include "../../../../src-deps/leveldb/include/leveldb/options.h"
#include "../../../../src-deps/leveldb/include/leveldb/cache.h"
#include "../../../../src-deps/leveldb/include/leveldb/filter_policy.h"
#include "../../../../src-deps/leveldb/include/leveldb/compressor.h"
#include "LevelStorageState.h"
#include <string>
class ChunkSource;
#include "StorageVersion.h"
#include "../../../nbt/CompoundTag.h"
namespace leveldb { class Slice; };
class LevelData;
class Player;
class KeyValueInput;
namespace leveldb { class WriteBatch; };
class DBStorage : public LevelStorage {






	typedef std::unique_ptr<CompoundTag, std::default_delete<CompoundTag> > TagPtr;
public:
	DBStorage(const std::string&, const std::string&);

	virtual ~DBStorage();

	virtual LevelStorageState getState() const;



	virtual ChunkSource* createChunkStorage(ChunkSource*, StorageVersion);

	virtual const std::string& getFullPath() const;



	DBStorage::TagPtr readTag(const std::string&);

	bool hasKey(const std::string&);

	void writeTag(const std::string&, CompoundTag&);

	void asyncWriteTag(const std::string&, CompoundTag&);

	void deleteAllWithPrefix(const leveldb::Slice&);

	virtual bool loadLevelData(LevelData&);

	virtual void saveLevelData(LevelData&);

	virtual void saveData(const std::string&, const std::string&);
	virtual std::string loadData(const std::string&);

	virtual std::unique_ptr<CompoundTag, std::default_delete<CompoundTag> > loadPlayerData(const std::string&);
	virtual void save(Player&);


protected:
	std::unique_ptr<leveldb::DB, std::default_delete<leveldb::DB> > db;
	leveldb::Options options;
	leveldb::ReadOptions readOptions;
	leveldb::WriteOptions writeOptions;

	std::unique_ptr<leveldb::Cache, std::default_delete<leveldb::Cache> > cache;
	std::unique_ptr<leveldb::FilterPolicy, std::default_delete<leveldb::FilterPolicy> > filterPolicy;
	std::unique_ptr<leveldb::Compressor, std::default_delete<leveldb::Compressor> > compressor;

	LevelStorageState state;

	std::string levelID, fullPath;
	std::string buffer;

	bool _read(const std::string&, std::string&) const;

	bool _read(const leveldb::Slice&, KeyValueInput&) const;

	bool _read(const std::string&);

	void _write(const std::string&, const std::string&) const;

	void _write(const std::unordered_set<leveldb::WriteBatch *, std::hash<leveldb::WriteBatch *>, std::equal_to<leveldb::WriteBatch *>, std::allocator<leveldb::WriteBatch *> >::value_type&) const;

	void _writeAsync(const std::string&, const std::unique_ptr<leveldb::FilterPolicy, std::default_delete<leveldb::FilterPolicy> >*) const;

	std::string _playerKey(const std::string&);
	std::string _playerKey(Player&);
	std::unique_ptr<CompoundTag, std::default_delete<CompoundTag> > _legacyLoadClientPlayer(const std::string&);
	std::unique_ptr<CompoundTag, std::default_delete<CompoundTag> > _legacyLoadPlayer(const std::string&);
};
