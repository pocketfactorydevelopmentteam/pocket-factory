#pragma once
#include <string>
#include <memory>
class LevelData;







struct LevelSummary {

	std::string id;
	std::string name;
	int lastPlayed;
	int gameType;
	int seed;
	uint64_t sizeOnDisk;

	bool operator<(const LevelSummary&) const;
};


	typedef std::vector<LevelSummary, std::allocator<LevelSummary> > LevelSummaryList;

class LevelStorageSource {

public:
	static const std::string TempLevelId;

	virtual ~LevelStorageSource();

	virtual std::string getName() = 0;
	virtual void getLevelList(LevelSummaryList&);

	virtual LevelData* getDataTagFor(const std::string&) = 0;
	virtual std::unique_ptr<LevelStorage, std::default_delete<LevelStorage> > createLevelStorage(const std::string&, bool) = 0;










	virtual bool isNewLevelIdAcceptable(const std::string&) = 0;

	virtual void clearAll() = 0;
	virtual void deleteLevel(const std::string&) = 0;
	virtual void renameLevel(const std::string&, const std::string&) = 0;

	virtual bool isConvertible(const std::string&) = 0;
	virtual bool requiresConversion(const std::string&) = 0;
	virtual bool convertLevel(const std::string&, ProgressListener*) = 0;
};
