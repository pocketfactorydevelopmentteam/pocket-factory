#pragma once
#include "LevelStorageSource.h"
#include <string>
#include <memory>
#include "LevelStorage.h"
class LevelData;





class ExternalFileLevelStorageSource : public LevelStorageSource {

public:
	ExternalFileLevelStorageSource(const std::string&, const std::string&);

	virtual std::string getName();
	virtual void getLevelList(LevelSummaryList&);

	virtual std::unique_ptr<LevelStorage, std::default_delete<LevelStorage> > createLevelStorage(const std::string&, bool);
	virtual LevelData* getDataTagFor(const std::string&);

	virtual bool isNewLevelIdAcceptable(const std::string&);

	virtual void clearAll();
	virtual void deleteLevel(const std::string&);
	virtual void renameLevel(const std::string&, const std::string&);

	virtual bool isConvertible(const std::string&);
	virtual bool requiresConversion(const std::string&);
	virtual bool convertLevel(const std::string&, ProgressListener*);

private:
	void addLevelSummaryIfExists(LevelSummaryList&, const std::string&);
	bool hasTempDirectory();
	std::string getFullPath(const std::string&);

	std::string basePath;
	std::string tmpBasePath;
	bool _hasTempDirectory;
};
