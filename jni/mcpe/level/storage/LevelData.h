#pragma once
#include "PlayerData.h"
#include <string>
#include <memory>
#include "StorageVersion.h"
#include "../../CommonType.h"
#include "../../phys/TilePos.h"
#include "../../nbt/CompoundTag.h"
#include "../Dimension.h"
#include "../GeneratorType.h"
class LevelSettings;
namespace RakNet { class BitStream; };
class Player;






class LevelData {


public:
	LevelData();
	LevelData(const LevelSettings&, const std::string&, GeneratorType, const TilePos*);
	LevelData(CompoundTag*);

	void v1_write(RakNet::BitStream&);
	void v1_read(RakNet::BitStream&, StorageVersion);


	CompoundTag* createTag();

	void getTagData(const CompoundTag*);
	void setTagData(CompoundTag*);

	long int getSeed() const;
	const TilePos& getSpawn() const;
	long int getTime() const;
	long int getSizeOnDisk() const;
	long int getStopTime() const;



	CompoundTag& getLoadedPlayerTag();
	void setLoadedPlayerTo(Player*);

	Dimension getDimension();

	void setSeed(long int);
	void setSpawn(const TilePos&);
	void setStopTime(long int);

	void setTime(long int);
	void setSizeOnDisk(long int);
	void clearLoadedPlayerTag();
	void setDimension(Dimension);

	std::string getLevelName();
	void setLevelName(const std::string&);

	GeneratorType getGenerator() const;
	void setGenerator(GeneratorType);

	long int getLastPlayed() const;

	int getGameType() const;
	void setGameType(int);
	bool getSpawnMobs() const;
	void setSpawnMobs(bool);

	const TilePos& getWorldCenter() const;




	PlayerData playerData;
	int playerDataVersion;
	std::string levelName;

	StorageVersion storageVersion;
private:
	RandomSeed seed;
	TilePos spawn, limitedWorldOrigin;
	long int time;
	int lastPlayed;
	long int sizeOnDisk;
	CompoundTag loadedPlayerTag;
	Dimension dimension;
	int gameType;
	bool spawnMobs;
	long int dayCycleStopTime;


	GeneratorType generator;
};
