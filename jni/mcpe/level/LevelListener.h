#pragma once
#include "TileSourceListener.h"
class Particle;
#include "../entity/ParticleType.h"
class Vec3;
#include <string>
#include <memory>
class Entity;
class Player;
class LevelChunk;
class Mob;






class LevelListener : public TileSourceListener {


public:
	virtual ~LevelListener();

	virtual void allChanged();

	virtual Particle* addParticle(ParticleType, const Vec3&, float, float, float, int);

	virtual void playSound(const std::string&, float, float, float, float, float);
	virtual void playMusic(const std::string&, float, float, float, float);
	virtual void playStreamingMusic(const std::string&, int, int, int);

	virtual void onEntityAdded(Entity&);
	virtual void onEntityRemoved(Entity&);

	virtual void onNewChunkFor(Player&, LevelChunk&);

	virtual void levelEvent(Mob*, int, int, int, int, int);
};
