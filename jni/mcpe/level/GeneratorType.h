#pragma once


enum class GeneratorType : int { Legacy, Overworld, Flat, Nether, Undefined };
