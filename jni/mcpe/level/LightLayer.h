#pragma once




class LightLayer {

public:
	static const LightLayer Sky;
	static const LightLayer Block;

	const int surrounding, id;

	bool operator==(const LightLayer&) const;



private:
	LightLayer(int, int);
};
