#pragma once

#include <vector>
#include <memory>
#include <unordered_set>

#include "util/SmallSet.h"
#include "util/NewType.h"

typedef signed char FacingID;
typedef long unsigned int RandomSeed;

typedef unsigned char Height;
typedef unsigned char Brightness;
typedef signed char FacingID;

typedef unsigned char DataID;

typedef unsigned short VertexUVType;


typedef float VertexPositionType;
typedef unsigned int Index;

typedef unsigned char byte;

typedef unsigned int TextureId;
typedef unsigned int BufferId;

template<typename T>
struct NewT
{
	T raw;
};

struct TileID : public NewT<unsigned char> {};

struct FullTile
{
	static const FullTile AIR;
	TileID id;
	DataID data;
};

class Player;
class Random;
class MinecraftClient;
class Level;
class Entity;
class Mob;
class TileSource;
class CompoundTag;
class Packet;


typedef std::vector<Entity*> EntityList;
typedef std::unordered_set<Player*, std::hash<Player*>> PlayerList;
typedef SmallSet<std::unique_ptr<Entity>> OwnedEntitySet;