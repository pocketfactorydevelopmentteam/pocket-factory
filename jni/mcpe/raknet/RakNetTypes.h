#pragma once


enum StartupResult { RAKNET_STARTED, RAKNET_ALREADY_STARTED, INVALID_SOCKET_DESCRIPTORS, INVALID_MAX_CONNECTIONS, SOCKET_FAMILY_NOT_SUPPORTED, SOCKET_PORT_ALREADY_IN_USE, SOCKET_FAILED_TO_BIND, SOCKET_FAILED_TEST_SEND, PORT_CANNOT_BE_ZERO, FAILED_TO_CREATE_NETWORK_THREAD, STARTUP_OTHER_FAILURE };


enum ConnectionAttemptResult { CONNECTION_ATTEMPT_STARTED, INVALID_PARAMETER, CANNOT_RESOLVE_DOMAIN_NAME, ALREADY_CONNECTED_TO_ENDPOINT, CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS, SECURITY_INITIALIZATION_FAILED };


enum ConnectionState { IS_PENDING, IS_CONNECTING, IS_CONNECTED, IS_DISCONNECTING, IS_SILENTLY_DISCONNECTING, IS_DISCONNECTED, IS_NOT_CONNECTED };


typedef unsigned short SystemIndex;
typedef uint32_t BitSize_t;


enum PublicKeyMode { PKM_INSECURE_CONNECTION, PKM_ACCEPT_ANY_PUBLIC_KEY, PKM_USE_KNOWN_PUBLIC_KEY, PKM_USE_TWO_WAY_AUTHENTICATION };




namespace RakNet {
	struct PublicKey {


		PublicKeyMode publicKeyMode;


		char* remoteServerPublicKey;


		char* myPublicKey;


		char* myPrivateKey;
	};
	struct SocketDescriptor {

		SocketDescriptor();
		SocketDescriptor(unsigned short, const char*);


		unsigned short port;


		char hostAddress[32];







		short socketFamily;









		unsigned short remotePortRakNetWasStartedOn_PS3_PSP2;


		unsigned int extraSocketOptions;
	};

	struct SystemAddress {


		SystemAddress();
		SystemAddress(const char*);
		SystemAddress(const char*, unsigned short);















		union  address;


		unsigned short debugPort;


		static int size();


		static long unsigned int ToInteger(const SystemAddress&);



		unsigned char GetIPVersion() const;



		unsigned int GetIPPROTO() const;


		void SetToLoopback();



		void SetToLoopback(unsigned char);


		bool IsLoopback() const;





		const char* ToString(bool, char) const;





		void ToString(bool, char*, char) const;








		bool FromString(const char*, char, int);


		bool FromStringExplicitPort(const char*, unsigned short, int);


		void CopyPort(const SystemAddress&);


		bool EqualsExcludingPort(const SystemAddress&) const;


		unsigned short GetPort() const;


		unsigned short GetPortNetworkOrder() const;


		void SetPort(unsigned short);


		void SetPortNetworkOrder(unsigned short);


		void SetBinaryAddress(const char*, char);

		void ToString_Old(bool, char*, char) const;


		void FixForIPVersion(const SystemAddress&);

		SystemAddress& operator=(const SystemAddress&);
		bool operator==(const SystemAddress&) const;
		bool operator!=(const SystemAddress&) const;
		bool operator>(const SystemAddress&) const;
		bool operator<(const SystemAddress&) const;


		SystemIndex systemIndex;
	};

	struct RakNetGUID {

		RakNetGUID();
		RakNetGUID(uint64_t);

		uint64_t g;




		const char* ToString() const;




		void ToString(char*) const;

		bool FromString(const char*);

		static long unsigned int ToUint32(const RakNetGUID&);

		RakNetGUID& operator=(const RakNetGUID&);







		SystemIndex systemIndex;
		static int size();

		bool operator==(const RakNetGUID&) const;
		bool operator!=(const RakNetGUID&) const;
		bool operator>(const RakNetGUID&) const;
		bool operator<(const RakNetGUID&) const;
	};
};






	//const Raknet::SystemAddress UNASSIGNED_SYSTEM_ADDRESS;
	//const Raknet::RakNetGUID UNASSIGNED_RAKNET_GUID;




namespace RakNet {
	struct Packet {


		SystemAddress systemAddress;




		RakNetGUID guid;


		unsigned int length;


		BitSize_t bitSize;


		unsigned char* data;



		bool deleteData;



		bool wasGeneratedLocally;
	};

	struct AddressOrGUID {

		RakNetGUID rakNetGuid;
		SystemAddress systemAddress;

		SystemIndex GetSystemIndex() const;
		bool IsUndefined() const;
		void SetUndefined();
		static long unsigned int ToInteger(const AddressOrGUID&);
		const char* ToString(bool) const;
		void ToString(bool, char*) const;

		AddressOrGUID();
		AddressOrGUID(const AddressOrGUID&);




		AddressOrGUID(const SystemAddress&);




		AddressOrGUID(Packet*);
		AddressOrGUID(const RakNetGUID&);




		AddressOrGUID& operator=(const AddressOrGUID&);






		AddressOrGUID& operator=(const SystemAddress&);






		AddressOrGUID& operator=(const RakNetGUID&);






		bool operator==(const AddressOrGUID&) const;
	};
};
	typedef uint64_t NetworkID;

	const int PING_TIMES_ARRAY_SIZE = 5;