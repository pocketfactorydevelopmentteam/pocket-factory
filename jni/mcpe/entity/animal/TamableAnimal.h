#pragma once
#include "Animal.h"
class SitGoal;
class TileSource;
class Player;
class CompoundTag;

	typedef int OwnerId;

class TamableAnimal : public Animal {



public:
	TamableAnimal(TileSource&);

	virtual Player* getOwner() const;
	const OwnerId getOwnerId() const;
	virtual void setOwner(const OwnerId);
	virtual bool isTame() const;

	SitGoal* getSitGoal();
	virtual bool isSitting() const;
	virtual void setSitting(bool);

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual void spawnTamingParticles(bool);

	virtual bool isTamable() const;


protected:
	SitGoal* sitGoal;

	static const int DATA_OWNERID_ID = 17;
	static const int DATAFLAG_ISSITTING = 1;
};
