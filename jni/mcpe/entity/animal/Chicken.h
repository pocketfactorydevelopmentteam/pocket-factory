#pragma once
#include "Animal.h"
class TileSource;
class ItemInstance;
class CompoundTag;
#include <string>
#include <memory>



class Chicken : public Animal {


public:
	Chicken(TileSource&);

	virtual int getEntityTypeId() const;

	virtual int getMaxHealth();

	virtual void aiStep();

	virtual bool isFood(const ItemInstance*) const;

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual float getBaseSpeed();
protected:
	virtual void causeFallDamage(float);

	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();

	virtual bool useNewAi();


	virtual void dropDeathLoot();

	virtual Animal* getBreedOffspring(Animal*);
public:
	bool sheared;
	float flap;
	float flapSpeed;
	float oFlapSpeed, oFlap;
	float flapping;
	int eggTime;
};
