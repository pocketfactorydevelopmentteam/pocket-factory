#pragma once
#include "Animal.h"
class TileSource;
class Player;
class ItemInstance;
#include <string>
#include <memory>


class Pig : public Animal {


	static const int DATA_SADDLE_ID = 16;

public:
	Pig(TileSource&);

	virtual int getEntityTypeId() const;











	virtual bool interactWithPlayer(Player*);

	virtual int getMaxHealth();

	virtual bool isFood(const ItemInstance*) const;

	bool hasSaddle();
	void setSaddle(bool);

	virtual float getBaseSpeed();
	virtual bool canBeControlledByRider();

protected:
	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();

	virtual bool useNewAi();

	virtual int getDeathLoot();

	virtual Animal* getBreedOffspring(Animal*);
};
