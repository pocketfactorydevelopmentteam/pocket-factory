#pragma once
#include "../AgableMob.h"
#include "../Creature.h"
class TileSource;
class Entity;
class Player;
class ItemInstance;
class CompoundTag;
class TilePos;
#include "../../phys/Vec3.h"


class Animal : public AgableMob, public Creature {




public:
	Animal(TileSource&);

	virtual void aiStep();

	Vec3 _randomHeartPos();

	virtual bool hurt(Entity*, int);

	virtual bool interactWithPlayer(Player*);

	virtual int getAmbientSoundInterval();

	virtual bool removeWhenFarAway();

	bool isInLove() const;
	void resetLove();
	virtual bool canMate(const Animal*) const;

	virtual bool isFood(const ItemInstance*) const;

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual bool shouldDespawn() const;



	virtual Animal* getBreedOffspring(Animal*) = 0;
protected:
	virtual float getWalkTargetValue(const TilePos&);
	virtual Entity* findAttackTarget();
private:
	int inLove;
};
