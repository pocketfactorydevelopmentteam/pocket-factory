#pragma once
#include "TamableAnimal.h"
class TileSource;
class CompoundTag;
class Animal;
class ItemInstance;
class Entity;
class Mob;
class Player;
#include <string>
#include <memory>
#include "../ai/goal/Goal.h"
#include "../TempEPtr.h"
#include "../ai/goal/target/TargetGoal.h"
class Wolf : public TamableAnimal {




public:
	Wolf(TileSource&);

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual bool canMate(const Animal*) const;

	virtual void newServerAiStep();
	virtual void normalTick();

	virtual Animal* getBreedOffspring(Animal*);

	float getHeadRollAngle(float);
	float getBodyRollAngle(float, float);
	float getTailAngle(float);
	float getWetShade(float);

	bool isAngry() const;
	bool isWet() const;
	bool isInterested() const;
	void setAngry(bool);
	void setWet(bool);
	void setInterested(bool);

	virtual void finalizeMobSpawn();
	virtual void handleEntityEvent(char);

	virtual bool isFood(const ItemInstance*) const;
	virtual bool doHurtTarget(Entity*);
	virtual bool isAlliedTo(Mob*);
	virtual bool hurt(Entity*, int);

	virtual bool interactWithPlayer(Player*);
	virtual bool canInteractWith(Player*);
	virtual std::string getInteractText(Player*);

	virtual int getEntityTypeId() const;

	virtual float getBaseSpeed();
	virtual const std::string& getTexture();

	virtual bool shouldDespawn() const;


protected:
	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();



	float shakeAnim, shakeAnimO;
	float interestedAngle, interestedAngleO;
	bool _isWet, isShaking;

	static const int DATA_HEALTH_ID = 18;
	static const int DATA_COLLAR_COLOR = 20;

	static const int DATAFLAG_ISANGRY = 2;
	static const int DATAFLAG_ISINTERESTED = 3;

	static const int START_HEALTH = 8;
	static const int MAX_HEALTH = 20;
	static const int TAME_HEALTH = 20;
};



class BegGoal : public Goal {


	Wolf* wolf;
	float lookDistance;
	int lookTime;
	TempEPtr<Player> player;


public:
	BegGoal(Wolf*);








	virtual bool canUse();











	virtual bool canContinueToUse();







	virtual void start();




	virtual void stop();




	virtual void tick();

private:
	bool playerHoldingInteresting(Player*);
};

class OwnerHurtByTargetGoal : public TargetGoal {


	TempEPtr<Mob> ownerHurtBy;
	Wolf* wolf;
public:
	OwnerHurtByTargetGoal(Wolf*);






	virtual bool canUse();




















	virtual void start();
};





class OwnerHurtTargetGoal : public TargetGoal {


	TempEPtr<Mob> ownerHurt;
	Wolf* wolf;
	int timestamp;
public:
	OwnerHurtTargetGoal(Wolf*);







	virtual bool canUse();



























	virtual void start();
};
