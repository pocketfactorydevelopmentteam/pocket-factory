#pragma once
#include "Animal.h"
#include "../../../client/renderer/renderer/Color.h"
class EatTileGoal;
class TileSource;
class Player;
class CompoundTag;
class Random;
#include <string>
#include <memory>











class Sheep : public Animal {



	static const int EAT_ANIMATION_TICKS = 40;
	static const int DATA_WOOL_ID = 16;

public:
	static const Color COLOR[];
	static const int NumColors;

	Sheep(TileSource&);

	virtual int getMaxHealth();

	virtual void aiStep();

	virtual void handleEntityEvent(char);

	float getHeadEatPositionScale(float);
	float getHeadEatAngleScale(float);

	virtual bool interactWithPlayer(Player*);

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	int getColor() const;
	void setColor(int);

	static int getSheepColor(Random*);
	virtual Animal* getBreedOffspring(Animal*);

	virtual const std::string& getTexture();

	bool isSheared() const;
	void setSheared(bool);

	virtual void finalizeMobSpawn();

	virtual void ate();

	virtual int getEntityTypeId() const;

	virtual float getBaseSpeed();

	virtual bool canInteractWith(Player*);
	virtual std::string getInteractText(Player*);
protected:
	virtual void dropDeathLoot();
	virtual int getDeathLoot();

	virtual void jumpFromGround();

	virtual bool useNewAi();
	virtual void newServerAiStep();

	virtual bool shouldHoldGround();

	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();








private:
	int eatAnimationTick;
	EatTileGoal* eatTileGoal;
};
