#pragma once
#include "Animal.h"
class TileSource;
class CompoundTag;
class Player;
#include <string>
#include <memory>
class Animal;



class Cow : public Animal {


public:
	Cow(TileSource&);

	virtual int getEntityTypeId() const;

	virtual int getMaxHealth();

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual bool interactWithPlayer(Player*);

	virtual void normalTick();




	virtual float getBaseSpeed();

	virtual bool canInteractWith(Player*);
	virtual std::string getInteractText(Player*);

protected:
	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();

	virtual bool useNewAi();

	virtual float getSoundVolume();

	virtual int getDeathLoot();
	virtual void dropDeathLoot();

	virtual Animal* getBreedOffspring(Animal*);

	int milkCounter;
};

class MushroomCow : public Cow {


public:
	MushroomCow(TileSource&);

	virtual int getEntityTypeId() const;

	virtual bool interactWithPlayer(Player*);
	virtual bool canInteractWith(Player*);
	virtual std::string getInteractText(Player*);


protected:
	virtual Animal* getBreedOffspring(Animal*);
};
