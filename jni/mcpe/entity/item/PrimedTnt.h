#pragma once
#include "../Entity.h"
class TileSource;
class CompoundTag;







class PrimedTnt : public Entity {


public:
	PrimedTnt(TileSource&);
	PrimedTnt(TileSource&, float, float, float);

	virtual void normalTick();

	virtual bool isPickable();

	virtual int getEntityTypeId() const;

	virtual float getShadowHeightOffs();

	virtual float getShadowRadius() const;



protected:
	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);
private:
	void explode();

public:
	int life;
};
