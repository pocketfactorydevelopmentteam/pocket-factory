#pragma once
#include "Minecart.h"
class TileSource;
class Player;

class MinecartRideable : public Minecart {

public:
	MinecartRideable(TileSource&);
	MinecartRideable(TileSource&, float, float, float);

	virtual bool interactWithPlayer(Player*);
	virtual int getType();
};
