#pragma once
#include "../Entity.h"
#include "../../item/ItemInstance.h"
class TileSource;
class Player;
#include "../../phys/AABB.h"
class CompoundTag;





class ItemEntity : public Entity {



	static const int LIFETIME;
public:
	ItemEntity(TileSource&);
	ItemEntity(TileSource&, float, float, float, const ItemInstance&);
	virtual ~ItemEntity();

	virtual void normalTick();
	virtual bool hurt(Entity*, int);
	virtual void playerTouch(Player*);
	virtual bool isItemEntity();
	virtual int getEntityTypeId() const;
	int getLifeTime() const;
	virtual AABB getHandleWaterAABB() const;
	virtual bool isPushable();
protected:
	virtual void burn(int);

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);
private:
	bool checkInTile(float, float, float);

public:
	ItemInstance item;
	int age;
	int throwTime;
	float bobOffs;
private:
	int tickCount;
	int health;
	int lifeTime;
};
