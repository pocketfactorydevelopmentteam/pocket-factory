#pragma once
#include "../Entity.h"
#include "../../../CommonTypes.h"
class TileSource;
class Level;
class CompoundTag;




class FallingTile : public Entity {


public:
enum class State : int { Falling, WaitRemoval };




	const bool creative;

	FallingTile(TileSource&);
	FallingTile(TileSource&, float, float, float, FullTile, bool);	FallingTile(TileSource&, float, float, float, FullTile, bool);

	void init();

	virtual bool isPickable();

	virtual void normalTick();

	virtual int getEntityTypeId() const;

	virtual float getShadowHeightOffs();

	Level* getLevel();

	virtual float getShadowRadius() const;



	bool isOnGround() const;



protected:
	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

public:
	FullTile tile;

	FallingTile::State state;
	int waitTicks;

	int time;

	void _waitRemoval();
};
