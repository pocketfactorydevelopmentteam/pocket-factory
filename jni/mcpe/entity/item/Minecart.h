#pragma once
#include "../Entity.h"
#include <string>
#include <memory>
class TileSource;
class Vec3;
class Tile;
class Player;
class CompoundTag;
class Minecart : public Entity {


public:
	static const int TYPE_RIDEABLE = 0;
	static const int TYPE_CHEST = 1;
	static const int TYPE_FURNACE = 2;
	static const int TYPE_TNT = 3;
	static const int TYPE_SPAWNER = 4;
	static const int TYPE_HOPPER = 5;

	static const long int serialVersionUID = 0;

protected:
	static const int DATA_ID_HURT = 17;
	static const int DATA_ID_HURTDIR = 18;
	static const int DATA_ID_DAMAGE = 19;
	static const int DATA_ID_DISPLAY_TILE = 20;
	static const int DATA_ID_DISPLAY_OFFSET = 21;
	static const int DATA_ID_CUSTOM_DISPLAY = 22;

public:
	Minecart(TileSource&);
	Minecart(TileSource&, float, float, float);
	static Minecart* createMinecart(TileSource&, float, float, float, int);
	virtual bool isPushable();
	virtual float getRideHeight();
	virtual bool hurt(Entity*, int);
	void destroy(Entity*);
	virtual void animateHurt();
	virtual bool isPickable();
	virtual void remove();
	virtual void normalTick();
	virtual void activateMinecart(int, int, int, bool);
	bool getPosOffs(Vec3&, float, float, float, float);
	bool getPos(Vec3&, float, float, float);
	virtual float getShadowHeightOffs();
	virtual void push(Entity*);
	virtual void lerpTo(float, float, float, float, float, int);
	virtual void lerpMotion(float, float, float);
	void setDamage(float);
	float getDamage();
	void setHurtTime(int);
	int getHurtTime();
	void setHurtDir(int);
	int getHurtDir();
	virtual int getType() = 0;
	Tile* getDisplayTile();
	Tile* getDefaultDisplayTile();
	int getDisplayData();
	int getDefaultDisplayData();
	int getDisplayOffset();
	int getDefaultDisplayOffset();
	void setDisplayTile(int);
	void setDisplayData(int);
	void setDisplayOffset(int);
	bool hasCustomDisplay();
	void setCustomDisplay(bool);
	void setCustomName(const std::string&);
	std::string getAName();
	bool hasCustomName();
	const std::string& getCustomName();
	virtual int getEntityTypeId() const;
	virtual float getShadowRadius() const;
	virtual void push(float, float, float);

	virtual bool canInteractWith(Player*);
	virtual std::string getInteractText(Player*);

protected:
	void comeOffTrack(float);
	void moveAlongTrack(int, int, int, float, float, int, int);
	void applyNaturalSlowdown();
	virtual void readAdditionalSaveData(CompoundTag*);
	virtual void addAdditionalSaveData(CompoundTag*);




	static const int EXITS[];
	bool flipped;
	std::string name;
	int lSteps;
	float lx, ly, lz, lyr, lxr;
	float lxd, lyd, lzd;
	Entity* lastRidingEntity;
	float sentX, sentY, sentZ;
	float sentRotX, sentRotY;
};
