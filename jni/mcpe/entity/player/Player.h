#pragma once
#include "../Mob.h"
#include <string>
#include <memory>
#include "Abilities.h"
#include "../../food/SimpleFoodData.h"
class BaseContainerMenu;
#include "../../raknet/RakNetTypes.h"
class Inventory;
//#include "../../level/chunk/PlayerChunkSource.h"
#include "../../phys/TilePos.h"
#include "../../phys/Vec3.h"
#include "../../item/ItemInstance.h"
class Level;
class ChunkSource;
struct Tick;
class Entity;
class ItemEntity;
class Tile;
class ChestTileEntity;
class FurnaceTileEntity;
class TileEntity;
class LevelChunk;
class CompoundTag;
class PlayerChunkSource;








class Player : public Mob {




	static const int DATA_PLAYER_FLAGS_ID = 16;
	static const int DATA_BED_POSITION_ID = 17;
	static const int DATA_PLAYER_DEAD = 18;
	static const int PLAYER_SLEEP_FLAG = 1;
	static const int PLAYER_DEAD_FLAG = 2;


public:
enum class RespawnState : int { Playing, WaitingSpawnArea };




	static const int MAX_NAME_LENGTH = 16;
	static const int MAX_HEALTH = 20;
	static const float DEFAULT_WALK_SPEED;
	static const float DEFAULT_FLY_SPEED;
	static const int SLEEP_DURATION = 100;
	static const int WAKE_UP_DURATION = 10;

	static const int NUM_ARMOR = 4;


	Player(Level&, bool);
	virtual ~Player();

	void _init();
	virtual void reset();

	void prepareRegion(ChunkSource&);

	PlayerChunkSource* getChunkSource() const;



	int getViewRadius() const;

	virtual int tickWorld(const Tick&);

	virtual void normalTick();
	virtual void moveView();
	virtual void aiStep();
	virtual void travel(float, float);

	virtual void rideTick();

	virtual float getWalkingSpeedModifier();

	virtual void die(Entity*);
	virtual void remove();
	virtual void respawn();

	void updateTeleportDestPos();

	void resetPos(bool);
	Vec3 getStandingPositionOnBlock(const TilePos&);
	TilePos getSpawnPosition();
	void setRespawnPosition(const TilePos&);

	virtual bool isShootable();
	virtual bool isCreativeModeAllowed();
	virtual bool isInWall();
	bool isDestroyingBlock();



	virtual bool hasResource(int);
	bool hasRespawnPosition() const;

	bool isUsingItem();
	ItemInstance* getUseItem();

	void startUsingItem(ItemInstance, int);
	void stopUsingItem();
	void releaseUsingItem();
	virtual void completeUsingItem();

	virtual int getUseItemDuration();
	int getTicksUsingItem();

	int getScore();
	virtual void awardKillScore(Entity*, int);
	virtual void handleEntityEvent(char);

	virtual void take(Entity*, int);

	virtual void drop(const ItemInstance*);
	virtual void drop(const ItemInstance*, bool);
	void reallyDrop(ItemEntity*);

	bool canDestroy(Tile*);
	float getDestroySpeed(Tile*);

	virtual int getMaxHealth();
	bool isHurt();

	virtual bool hurt(Entity*, int);
	virtual void hurtArmor(int);
	void setArmor(int, const ItemInstance*);
	ItemInstance* getArmor(int);
	int getArmorTypeHash();

	void interact(Entity*);
	void attack(Entity*);
	virtual ItemInstance* getCarriedItem();
	bool canUseCarriedItemWhileMoving();

	virtual void startCrafting(int, int, int, int);
	virtual void startStonecutting(int, int, int);
	virtual void startDestroying();



	virtual void stopDestroying();



	virtual void openContainer(ChestTileEntity*);
	virtual void openFurnace(FurnaceTileEntity*);
	void tileEntityDestroyed(int);

	virtual void displayClientMessage(const std::string&);
	virtual void animateRespawn();
	virtual float getHeadHeight() const;
	virtual float getRidingHeight();
	virtual void ride(Entity*);
	void findStandUpPosition(Entity*);

	virtual int getEntityTypeId() const;

	virtual bool isSleeping();
	virtual int startSleepInBed(int, int, int);
	virtual void stopSleepInBed(bool, bool);
	virtual int getSleepTimer();
	void setAllPlayersSleeping();
	float getSleepRotation();
	bool isSleepingLongEnough();
	ItemInstance* getSelectedItem();

	virtual void openTextEdit(TileEntity*);
	virtual int getArmorValue();

	virtual bool isLocalPlayer();

	virtual float getBaseSpeed();

	virtual bool useNewAi();

	void onChunkRemoved(LevelChunk&);

	void settTeleportDestPos(const Vec3&);

protected:
	virtual bool isImmobile();
	virtual void updateAi();
	virtual void closeContainer();
	void setDefaultHeadHeight();

	virtual void readAdditionalSaveData(CompoundTag*);
	virtual void addAdditionalSaveData(CompoundTag*);

	void setBedOffset(int);
	bool checkBed();

	static void animateRespawn(Player*, Level*);
	void spawnEatParticles(const ItemInstance*, int);

	virtual void onViewDistanceChanged(int);



	virtual bool isPushable();


private:
	void touch(Entity*);
public:
	char userType;
	int score;
	float oBob, bob;
	bool spawning;

	std::string name;

	Abilities abilities;
	SimpleFoodData foodData;


	BaseContainerMenu* containerMenu;


	RakNet::RakNetGUID owner;
	std::string uniqueName;
	int clientRandomId;

	int lastThrowTick;

	Inventory* inventory;
	bool hasFakeInventory;
	std::unique_ptr<PlayerChunkSource, std::default_delete<PlayerChunkSource> > chunkRegion;

	TilePos bedPosition;
	Vec3 bedOffset;

	Vec3 teleportDestPos;

	bool mcoValidPlayer;
protected:
	ItemInstance useItem;
	int useItemDuration;
	short sleepCounter;
	ItemInstance armor[4];
	int viewRadius;
	Player::RespawnState respawnState;

	TilePos _findNearBlockToStandOn(const Vec3&);

	int _getTopStandingPosition(const TilePos&);

	void _fixSpawnPosition(const Vec3&);
private:
	TilePos respawnPosition;
	bool playerIsSleeping;
	bool allPlayersSleeping;

	bool destroyingBlock;
};
