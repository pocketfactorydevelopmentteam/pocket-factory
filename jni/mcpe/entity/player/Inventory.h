#pragma once
#include "../../inventory/FillingContainer.h"
class Player;
class ItemInstance;
class Entity;
class Tile;








class Inventory : public FillingContainer {


public:
	static const int INVENTORY_SIZE_DEMO = 27;
	static const int POP_TIME_DURATION = 5;

	Inventory(Player*, bool);
	virtual ~Inventory();

	void clearInventoryWithDefault();



	void selectSlot(int);
	ItemInstance* getSelected();

	static int getSelectionSize();

	bool moveToSelectionSlot(int, int);
	bool moveToSelectedSlot(int);
	bool moveToEmptySelectionSlot(int);

	bool removeItemInstance(const ItemInstance*);

	virtual void doDrop(ItemInstance*, bool);
	virtual bool stillValid(Player*);
	virtual bool add(ItemInstance*);

	int getAttackDamage(Entity*);
	float getDestroySpeed(Tile*);
	bool canDestroy(Tile*);

	virtual int getContainerSize() const;

	int getLinkedSlotForItem(int);
	int getLinkedSlotForItemIdAndAux(int, int);
private:
	void setupDefault();


public:
	int selected;
	Player* player;
};
