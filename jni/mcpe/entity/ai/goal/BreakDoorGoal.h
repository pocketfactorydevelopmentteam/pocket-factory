#pragma once
#include "DoorInteractGoal.h"
class Mob;





class BreakDoorGoal : public DoorInteractGoal {


	int breakTime;

public:
	BreakDoorGoal(Mob*);

	virtual bool canUse();
	virtual void start();
	virtual bool canContinueToUse();
	virtual void tick();
};
