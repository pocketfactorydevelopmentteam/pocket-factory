#pragma once
#include "TargetGoal.h"
class Mob;
class Monster;







class NearestAttackableTargetGoal : public TargetGoal {


public:
	NearestAttackableTargetGoal(Monster*, int, float, int, bool);







	virtual bool canUse();










	virtual void start();


protected:
	Mob* target;
	int targetType;
	int randomInterval;
};
