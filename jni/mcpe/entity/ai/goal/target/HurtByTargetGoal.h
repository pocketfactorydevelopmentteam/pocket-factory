#pragma once
#include "TargetGoal.h"
class Mob;




class HurtByTargetGoal : public TargetGoal {


public:
	HurtByTargetGoal(Mob*, bool);

	virtual bool canUse();

	virtual void start();
private:
	bool alertSameType;
};
