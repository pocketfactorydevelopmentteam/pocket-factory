#pragma once
#include "Goal.h"
#include "../../Entity.h"
#include "../../TempEPtr.h"
class Mob;





class LookAtPlayerGoal : public Goal {

protected:
	TempEPtr<Entity> lookAt;

	float lookDistance;
	int lookTime;
	float probability;

public:
	LookAtPlayerGoal(Mob*, float);
	LookAtPlayerGoal(Mob*, float, float);
	virtual ~LookAtPlayerGoal();

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
	virtual void stop();
	virtual void tick();
};

class LookAtMobGoal : public LookAtPlayerGoal {


	int lookAtMobTypeId;

public:
	LookAtMobGoal(Mob*, float, int);
	LookAtMobGoal(Mob*, float, int, float);
	virtual ~LookAtMobGoal();

	virtual bool canUse();
};


class InteractGoal : public LookAtMobGoal {



public:
	InteractGoal(Mob*, float, int);
	InteractGoal(Mob*, float, int, float);
	virtual ~InteractGoal();
};
