#pragma once
#include "Goal.h"
class Level;
#include "../../Mob.h"
#include "../../TempEPtr.h"
class Path;







class MeleeAttackGoal : public Goal {

protected:
	Level* level;
	TempEPtr<Mob> target;

	int attackTime;
	float speed;
	bool trackTarget;
	Path* path;
	int timeToRecalcPath;

	void _init(float, bool);

	virtual float getAttackReachSqr();

public:
	MeleeAttackGoal(Mob*, float, bool);
	virtual ~MeleeAttackGoal();

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
	virtual void stop();
	virtual void tick();


	virtual void setLevel(Level*);
};
