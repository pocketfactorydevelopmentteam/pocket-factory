#pragma once
#include "Goal.h"
class Level;
#include "../../Mob.h"
#include "../../TempEPtr.h"
class ArrowAttackGoal : public Goal {

public:
	static const int ArrowType = 1;
	static const int SnowballType = 2;

	Level* level;
	TempEPtr<Mob> target;
	int attackTime;
	float speed;
	int seeTime;
	int projectileType;
	int attackInterval;

	ArrowAttackGoal(Mob*, float, int, int);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void stop();
	virtual void tick();

private:
	void fireAtTarget();


public:
	virtual void setLevel(Level*);
};
