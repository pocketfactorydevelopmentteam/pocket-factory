#pragma once
#include "Goal.h"
class PathfinderMob;







class RandomStrollGoal : public Goal {


	float wantedX, wantedY, wantedZ;
	float speed;

public:
	RandomStrollGoal(PathfinderMob*, float);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
};
