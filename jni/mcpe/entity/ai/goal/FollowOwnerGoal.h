#pragma once
#include "Goal.h"
class TamableAnimal;
#include "../../Mob.h"
#include "../../TempEPtr.h"




class FollowOwnerGoal : public Goal {


	TamableAnimal* animal;
	TempEPtr<Mob> owner;
	float speed;
	int timeToRecalcPath;
	float startDistance, stopDistance;
	bool oldAvoidWater;

public:
	FollowOwnerGoal(TamableAnimal*, float, float, float);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
	virtual void stop();
	virtual void tick();

	static const int TeleportDistance = 12;
};
