#pragma once
#include "Goal.h"
#include "../../player/Player.h"
#include "../../TempEPtr.h"
#include <memory>
#include <vector>
class PathfinderMob;
#include <initializer_list>


class TemptGoal : public Goal {


	float speed;
	float px, py, pz, pRotX, pRotY;
	TempEPtr<Player> player;
	int calmDown;
	bool _isRunning;
	std::vector<int, std::allocator<int> > itemIds;
	bool canScare;
	bool oldAvoidWater;

	int pathfinderWaitTicks;
public:
	TemptGoal(PathfinderMob*, float, std::initializer_list<int>, bool);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
	virtual void stop();
	virtual void tick();
	bool isRunning();
};
