#pragma once
#include "Goal.h"
class DoorTile;
class Mob;






class DoorInteractGoal : public Goal {

protected:
	int doorX, doorY, doorZ;
	DoorTile* doorTile;

private:
	bool passed;
	float doorOpenDirX, doorOpenDirZ;

public:
	DoorInteractGoal(Mob*);
	virtual ~DoorInteractGoal();

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
	virtual void tick();

private:
	DoorTile* getDoorTile(int, int, int);
};
