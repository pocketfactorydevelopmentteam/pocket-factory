#pragma once
#include "Goal.h"
class PathfinderMob;







class PanicGoal : public Goal {


	float speed;
	float posX, posY, posZ;

public:
	PanicGoal(PathfinderMob*, float);

	virtual bool canUse();
	virtual void start();
	virtual bool canContinueToUse();
};
