#pragma once
#include "Goal.h"
#include "../../animal/Animal.h"
#include "../../TempEPtr.h"







class FollowParentGoal : public Goal {


	Animal* animal;
	TempEPtr<Animal> parent;
	float speed;
	int timeToRecalcPath;

public:
	FollowParentGoal(Animal*, float);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
	virtual void stop();
	virtual void tick();
};
