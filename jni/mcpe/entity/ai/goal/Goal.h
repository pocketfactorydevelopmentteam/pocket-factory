#pragma once
class Mob;
class TileSource;


class Goal {


	int _requiredControlFlags;

protected:
	static const int NO_ID = -1;
	Goal(Mob*);

	Mob* mob;

	TileSource& getRegion();

public:
	virtual ~Goal();
	virtual bool canUse() = 0;
	virtual bool canContinueToUse();
	virtual bool canInterrupt();
	virtual void start();
	virtual void stop();
	virtual void tick();
	virtual void setRequiredControlFlags(int);
	virtual int getRequiredControlFlags();
};
