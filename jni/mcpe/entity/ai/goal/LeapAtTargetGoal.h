#pragma once
#include "Goal.h"
#include "../../Mob.h"
#include "../../TempEPtr.h"



class LeapAtTargetGoal : public Goal {


	TempEPtr<Mob> target;
	float yd;

public:
	LeapAtTargetGoal(Mob*, float);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
};
