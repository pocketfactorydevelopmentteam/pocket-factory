#pragma once
#include "Goal.h"
class Level;
class Mob;





class EatTileGoal : public Goal {


	static const int EAT_ANIMATION_TICKS = 40;

	Level* level;
	int eatAnimationTick;

public:
	EatTileGoal(Mob*);

	virtual bool canUse();
	virtual void start();
	virtual void stop();
	virtual bool canContinueToUse();
	virtual int getEatAnimationTick();
	virtual void tick();


	virtual void setLevel(Level*);
};
