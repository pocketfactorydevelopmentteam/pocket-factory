#pragma once
#include "Goal.h"
class Creeper;
#include "../../Mob.h"
#include "../../TempEPtr.h"





class SwellGoal : public Goal {


	Creeper* creeper;
	TempEPtr<Mob> target;

public:
	SwellGoal(Creeper*);

	virtual bool canUse();
	virtual void start();
	virtual void stop();
	virtual void tick();
};
