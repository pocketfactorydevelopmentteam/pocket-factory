#pragma once
#include "Goal.h"
class Level;
class PathfinderMob;
class Vec3;





class FleeSunGoal : public Goal {


	float wantedX, wantedY, wantedZ;
	float speed;
	Level* level;

public:
	FleeSunGoal(PathfinderMob*, float);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();

private:
	bool getHidePos(Vec3*);


public:
	virtual void setLevel(Level*);
};
