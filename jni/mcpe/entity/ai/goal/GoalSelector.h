#pragma once
class Goal;
#include <memory>
#include <vector>



class GoalSelector {

public:
class InternalGoal {


public:
	InternalGoal(int, Goal*, bool);

	Goal* goal;
	int prio;
	bool canDeletePointer;
	bool used;
	bool toStart;
};

private:
	std::vector<GoalSelector::InternalGoal *, std::allocator<GoalSelector::InternalGoal *> > goals;

public:
	~GoalSelector();


	void addGoal(int, Goal*, bool);
	void tick();

private:
	bool canUseInSystem(GoalSelector::InternalGoal*);
	bool canCoExist(GoalSelector::InternalGoal*, GoalSelector::InternalGoal*);
};
