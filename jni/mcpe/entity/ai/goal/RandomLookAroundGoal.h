#pragma once
#include "Goal.h"
class Mob;





class RandomLookAroundGoal : public Goal {


	float relX, relZ;
	int lookTime;

public:
	RandomLookAroundGoal(Mob*);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
	virtual void tick();
};
