#pragma once
#include "Goal.h"
class Level;
#include "../../animal/Animal.h"
#include "../../TempEPtr.h"






class BreedGoal : public Goal {


	Animal* animal;
	Level* level;
	TempEPtr<Animal> partner;
	int loveTime;
	float speed;

public:
	BreedGoal(Animal*, float);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void stop();
	virtual void tick();

private:
	Animal* getFreePartner();
	void breed();


public:
	virtual void setLevel(Level*);
};
