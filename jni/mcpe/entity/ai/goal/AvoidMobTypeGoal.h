#pragma once
#include "Goal.h"
#include "../../Entity.h"
#include "../../TempEPtr.h"
class PathfinderMob;
class Path;
class PathNavigation;



class AvoidMobTypeGoal : public Goal {


	TempEPtr<Entity> toAvoid;
	PathfinderMob* pathfinder;
	float walkSpeedModifier, sprintSpeedModifier;
	float maxDist;
	Path* path;
	PathNavigation* pathNav;
	int avoidType;

public:
	AvoidMobTypeGoal(PathfinderMob*, int, float, float, float);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();
	virtual void stop();
	virtual void tick();
};
