#pragma once
class Mob;
#include <functional>
#include <memory>
#include <set>
class Entity;


class Sensing {
	typedef std::set<int, std::less<int>, std::allocator<int> > EntitySet;
public:
	Sensing(Mob*);



	void tick();




	bool canSee(Entity*);












private:
	Mob* mob;
	Sensing::EntitySet seen;
	Sensing::EntitySet unseen;
};
