#pragma once
class Mob;
class Path;
#include "../../../phys/Vec3.h"
class Level;





class PathNavigation {

protected:
	Mob* mob;
	Path* path;
	float speed;
	float maxDist;
	bool avoidSun;
	int _tick;
	int lastStuckCheck;
	Vec3 lastStuckCheckPos;

	bool _canPassDoors;
	bool _canOpenDoors;
	bool avoidWater;
	bool canFloat;

public:
	PathNavigation(Mob*, Level*, float);
	virtual ~PathNavigation();

	void setAvoidWater(bool);
	bool getAvoidWater();
	void setCanOpenDoors(bool);
	bool canPassDoors();
	void setCanPassDoors(bool);
	bool canOpenDoors();
	void setAvoidSun(bool);
	void setSpeed(float);
	void setCanFloat(bool);
	virtual Path* createPath(float, float, float);
	virtual bool moveTo(float, float, float, float);
	virtual Path* createPath(Mob*);
	virtual bool moveTo(Mob*, float);
	virtual bool moveTo(Path*, float);
	Path* getPath();
	virtual void tick();

private:
	void updatePath();

public:
	virtual bool isDone();

	virtual void stop();

private:
	Vec3 getTempMobPos();
	int getSurfaceY();
	bool canUpdatePath();
	bool isInLiquid();
	void trimPathFromSun();
	bool canMoveDirectly(const Vec3, const Vec3, int, int, int);
	bool canWalkOn(int, int, int, int, int, int, const Vec3, float, float);
	bool canWalkAbove(int, int, int, int, int, int, const Vec3, float, float);
};
