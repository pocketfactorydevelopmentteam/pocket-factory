#pragma once
#include "PathNavigation.h"
#include "../../../level/TilePos.h"
class Mob;
class Level;
class Path;

class WallClimberPathNavigation : public PathNavigation {


public:
	WallClimberPathNavigation(Mob*, Level*);






	virtual Path* createPath(float, float, float);







	virtual Path* createPath(Mob*);
	virtual bool moveTo(Mob*, float);
	virtual void tick();

	virtual bool hasDestination();


protected:
	TilePos pathToPosition;
	bool hasPosition;
};
