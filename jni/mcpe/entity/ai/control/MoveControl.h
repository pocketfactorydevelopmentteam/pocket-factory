#pragma once
#include "Control.h"
class Mob;




class MoveControl : public Control {

protected:
	static const float MAX_TURN;
public:
	static const float MIN_SPEED;
	static const float MIN_SPEED_SQR;

	virtual ~MoveControl();




	MoveControl(Mob*);

	bool hasWanted();
	float getSpeed();
	void setWantedPosition(float, float, float, float);
	void setSpeed(float);
	virtual void tick();

protected:
	Mob* mob;
	float wantedX;
	float wantedY;
	float wantedZ;
	float speed;
	bool _hasWanted;
};
