#pragma once
#include "Control.h"
class Mob;
class Entity;




class LookControl : public Control {

public:
	LookControl(Mob*);


	void setLookAt(Entity*, float, float);
	void setLookAt(float, float, float, float, float);

	virtual void tick();

	virtual ~LookControl();



private:
	Mob* mob;
	float yMax, xMax;
	bool hasWanted;

	float wantedX, wantedY, wantedZ;
};
