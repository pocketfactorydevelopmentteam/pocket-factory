#pragma once
#include "Control.h"
class Mob;




class BodyControl : public Control {

public:
	BodyControl(Mob*);

	void clientTick();

private:
	Mob* mob;
	static const float maxClampAngle;
	int timeStill;
	float lastHeadY;
};
