#pragma once
#include "Control.h"
class Mob;




class JumpControl : public Control {


	Mob* mob;
	bool _jump;

public:
	JumpControl(Mob*);

	void jump();
	virtual void tick();

	virtual ~JumpControl();
};
