#pragma once


class Control {

public:
	static const int MoveControlFlag = 1;
	static const int LookControlFlag = 2;
	static const int JumpControlFlag = 4;
};
