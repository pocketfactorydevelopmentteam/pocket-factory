#pragma once
#include <string>
#include <memory>

#include "Entity.h"
#include "ai/control/LookControl.h"
#include "ai/control/MoveControl.h"
#include "ai/control/JumpControl.h"
#include "ai/control/BodyControl.h"
#include "ai/navigation/PathNavigation.h"
#include "ai/Sensing.h"
#include "ai/goal/GoalSelector.h"
#include "../phys/Vec3.h"
#include "../util/Random.h"
#include "../phys/HitResult.h"
#include "../renderer/Color.h"

class TileSource;
class Level;
struct TextureUVCoordinateSet;
class ItemInstance;
class SynchedEntityData;
class CompoundTag;


class Mob : public Entity {






public:
	static const int ATTACK_DURATION = 5;
	static const int SWING_DURATION = 8;

	Mob(TileSource&);
	Mob(Level&);

	void _ctor();

	virtual ~Mob();

	void _init();
	virtual void reset();
	virtual void postInit();

	virtual void knockback(Entity*, int, float, float);
	virtual void die(Entity*);

	virtual bool canSee(Entity*);

	virtual bool onLadder();

	virtual void spawnAnim();
	virtual const std::string& getTexture();

	virtual bool isAlive();
	virtual bool isPickable();
	virtual bool isPushable();
	virtual bool isShootable();

	virtual bool isSleeping();
	virtual bool isWaterMob();

	virtual bool isSneaking();
	virtual void setSneaking(bool);

	virtual float getHeadHeight() const;

	virtual float getVoicePitch();

	virtual void playAmbientSound();
	virtual int getAmbientSoundInterval();
	virtual const TextureUVCoordinateSet& getItemInHandIcon(const ItemInstance*, int);
	virtual void lerpTo(float, float, float, float, float, int);

	void setYya(float);
	virtual float getBaseSpeed() = 0;
	float getSpeed();
	void setSpeed(float);
	void setJumping(bool);

	virtual void normalTick();
	virtual void baseTick();

	virtual void superTick();

	virtual void heal(int);
	virtual int getMaxHealth();
	virtual bool hurt(Entity*, int);
	virtual void actuallyHurt(int);
	virtual void animateHurt();
	virtual int getArmorValue();

	virtual bool isOnFire() const;

	virtual HitResult pick(float, float, bool);

	virtual void travel(float, float);
	virtual void updateWalkAnim();

	virtual void aiStep();

	void pushEntities();


	virtual const SynchedEntityData* getEntityData() const;

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual void lookAt(Entity*, float, float);
	virtual bool isLookingAtAnEntity();


	virtual void beforeRemove();

	virtual bool canSpawn();
	virtual void finalizeMobSpawn();
	virtual bool shouldDespawn() const;

	virtual float getAttackAnim(float);

	float getYHeadRot();
	void setYHeadRot(float);

	virtual bool isBaby() const;
	virtual bool isTamable() const;

	virtual void handleEntityEvent(char);
	virtual ItemInstance* getCarriedItem();
	virtual int getUseItemDuration();

	virtual void swing();
	virtual void ate();

	PathNavigation& getNavigation();
	LookControl& getLookControl();
	MoveControl& getMoveControl();
	JumpControl& getJumpControl();
	Sensing& getSensing();

	virtual float getMaxHeadXRot();

	virtual Mob* getLastHurtByMob();
	virtual void setLastHurtByMob(Mob*);

	virtual Mob* getLastHurtMob();
	virtual void setLastHurtMob(Entity*);
	int getLastHurtMobTimestamp();

	virtual Mob* getTarget();
	virtual void setTarget(Mob*);
	virtual bool isAlliedTo(Mob*);

	virtual bool doHurtTarget(Entity*);

	virtual bool canBeControlledByRider();

	virtual void teleportTo(float, float, float);

	virtual Color getOverlayColor(float) const;

protected:
	virtual void causeFallDamage(float);

	virtual void outOfWorld();
	virtual bool removeWhenFarAway();

	virtual int getDeathLoot();
	virtual void dropDeathLoot();

	virtual bool isImmobile();

	virtual void jumpFromGround();

	virtual void updateAi();
	virtual void newServerAiStep();
	virtual void serverAiMobStep();

	virtual void setSize(float, float);

	virtual float getSoundVolume();
	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();

	virtual float getWalkingSpeedModifier();

	virtual int getDamageAfterArmorAbsorb(int);
	virtual void hurtArmor(int);

	bool interpolateOnly();

	virtual bool useNewAi();
	bool getSharedFlag(int) const;
	void setSharedFlag(int, bool);
	void checkDespawn(Mob*);
	void checkDespawn();
	void updateAttackAnim();
	virtual void rideTick();

private:
	Mob* updateMobId(int*);

public:
	int invulnerableDuration;

	bool surfaceMonster;

	float timeOffs;

	float rotA;
	float yBodyRot, yBodyRotO;
	float yHeadRot, yHeadRotO;


	float oAttackAnim, attackAnim;

	int health;
	int lastHealth;

	int hurtTime;
	int hurtDuration;
	float hurtDir;

	int deathTime;
	int attackTime;
	float oTilt, tilt;

	int lookTime;
	float fallTime;

	float walkAnimSpeedO;
	float walkAnimSpeed;
	float walkAnimPos;

	Vec3 aimDirection;
	int arrowCount;
	int removeArrowTime;

	Random random;





	bool bypassArmor;
	float xxa, yya, yRotA;

	void setPersistent();


protected:
	float walkAnimSpeedMultiplier;
	bool swinging;
	int swingTime;
	int noActionTime;
	float defaultLookAngle;

	float flyingSpeed;
	float frictionModifier;

	std::string textureName;
	std::string modelName;
	int deathScore;
	float oRun, run;
	float animStep, animStepO;
	float rotOffs;

	int lSteps;
	float lx, ly, lz, lyr, lxr;
	int lastHurt;
	int dmgSpill;

	float sentX, sentY, sentZ, sentRotX, sentRotY;
	float sentXd, sentYd, sentZd;
	int sentYHeadRot;


	bool allowAlpha;
	bool jumping;
	bool autoSendPosRot;

	std::unique_ptr<LookControl, std::default_delete<LookControl> > lookControl;
	std::unique_ptr<MoveControl, std::default_delete<MoveControl> > moveControl;
	std::unique_ptr<JumpControl, std::default_delete<JumpControl> > jumpControl;
	std::unique_ptr<BodyControl, std::default_delete<BodyControl> > bodyControl;
	std::unique_ptr<PathNavigation, std::default_delete<PathNavigation> > navigation;
	std::unique_ptr<Sensing, std::default_delete<Sensing> > sensing;

	GoalSelector goalSelector;
	GoalSelector targetSelector;
private:
	int targetId;
	int lookingAtId;
	int lastHurtMobId;
	int lastHurtMobTimestamp;
	int lastHurtByMobId;
	int lastHurtByMobTime;
	int ambientSoundTime;

	float speed;

	bool persistent;
};
