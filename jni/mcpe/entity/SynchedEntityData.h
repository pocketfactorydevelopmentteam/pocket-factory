#pragma once
#include <functional>
#include <utility>
#include <memory>
#include <map>
#include <string>
#include <vector>
class IDataOutput;
class IDataInput;
class Entity;
#include "../item/ItemInstance.h"
#include "../phys/TilePos.h"

class DataItem {
public:
	DataItem(int, int);




	virtual ~DataItem();

	int getId() const;



	int getType() const;



	bool isDirty() const;



	void setDirty(bool);


	virtual bool isDataEqual(const DataItem&) const;


private:
	const int type;
	const int id;
	bool dirty;
};















template<typename T> class DataItem2 : public DataItem {

public:
	DataItem2(int, int, const signed char&);



	void setFlag(int);


	void clearFlag(int);


	bool getFlag(int) const;


	signed char data;
};



























class SynchedEntityData {
public:
	static const int MAX_STRING_DATA_LENGTH = 64;
	static const int EOF_MARKER = 127;

	typedef signed char TypeChar;
	typedef short TypeShort;
	typedef int TypeInt;
	typedef float TypeFloat;
private:
	static const int TYPE_BYTE = 0;
	static const int TYPE_SHORT = 1;
	static const int TYPE_INT = 2;
	static const int TYPE_FLOAT = 3;
	static const int TYPE_STRING = 4;

	static const int TYPE_ITEMINSTANCE = 5;
	static const int TYPE_POS = 6;


	static const int TYPE_MASK = 224;
	static const int TYPE_SHIFT = 5;


	static const int MAX_ID_VALUE = 31;

	typedef std::map<int, DataItem *, std::less<int>, std::allocator<std::pair<const int, DataItem *> > > Map;

	typedef std::vector<DataItem *, std::allocator<DataItem *> > DataList;
public:
	SynchedEntityData();

	~SynchedEntityData();

	int getTypeId(const SynchedEntityData::TypeChar&);
	int getTypeId(const SynchedEntityData::TypeShort&);
	int getTypeId(const SynchedEntityData::TypeInt&);
	int getTypeId(const SynchedEntityData::TypeFloat&);
	int getTypeId(const std::string&);
	int getTypeId(const ItemInstance&);
	int getTypeId(const TilePos&);



















	SynchedEntityData::TypeChar getByte(int) const;
	SynchedEntityData::TypeShort getShort(int) const;
	SynchedEntityData::TypeInt getInt(int) const;
	SynchedEntityData::TypeFloat getFloat(int) const;
	std::string getString(int) const;
	ItemInstance getItemInstance(int);
	TilePos getPos(int) const;

















































	void markDirty(int);




	bool isDirty() const;



	static void pack(SynchedEntityData::DataList*, IDataOutput*);

	SynchedEntityData::DataList packDirty();

	void packAll(IDataOutput*) const;

	static SynchedEntityData::DataList unpack(IDataInput*);






	void assignValues(SynchedEntityData::DataList*, Entity*);

private:
	static void writeDataItem(IDataOutput*, const DataItem*);

	SynchedEntityData::Map itemsById;
	bool _isDirty;
};
