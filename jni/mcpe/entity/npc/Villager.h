#pragma once
#include "../AgableMob.h"
class TileSource;
class CompoundTag;
class Player;
#include <string>
#include <memory>
class Villager : public AgableMob {


public:
	Villager(TileSource&);
	Villager(TileSource&, int);

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual void finalizeMobSpawn();

	virtual void newServerAiStep();


	virtual bool interactWithPlayer(Player*);

	virtual float getBaseSpeed();
	virtual int getEntityTypeId() const;

	void setProfession(int);
	int getProfession();

	virtual bool shouldDespawn() const;




protected:
	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();


private:
	void init(int);

	static const int DATA_PROFESSION_ID = 16;

public:
	static const int PROFESSION_FARMER = 0;
	static const int PROFESSION_LIBRARIAN = 1;
	static const int PROFESSION_PRIEST = 2;
	static const int PROFESSION_SMITH = 3;
	static const int PROFESSION_BUTCHER = 4;
	static const int PROFESSION_MAX = 5;
};
