#pragma once
#include "PathfinderMob.h"
class TileSource;
class CompoundTag;



class AgableMob : public PathfinderMob {


public:
	AgableMob(TileSource&);

	int getAge();
	void setAge(int);
	virtual bool isBaby() const;

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual void aiStep();

	virtual void setSize(float, float);
	void updateSize(bool);

	virtual float getShadowRadius() const;




private:
	void internalSetSize(float);


protected:
	bool getAnimalFlag(int) const;
	void setAnimalFlag(int, bool);

private:
	int age;

	float registeredBBWidth;
	float registeredBBHeight;

	static const int DATA_FLAGS_ID = 14;

	static const int DATAFLAG_ISBABY = 0;
};
