#pragma once


enum class ParticleType : int { None, Bubble, Crit, Smoke, Explode, Flame, Lava, LargeSmoke, RedDust, IconCrack, SnowballPoof, LargeExplode, HugeExplosion, MobFlame, Heart, Terrain, TownAura, Portal, WaterSplash, DripWater, DripLava, FallingDust, _count };
