#pragma once
#include "HangingEntity.h"
class Motive;
class TileSource;
#include <string>
#include <memory>
class CompoundTag;
class Painting : public HangingEntity {

public:
	Painting(TileSource&);
	Painting(TileSource&, int, int, int, int);
	Painting(TileSource&, int, int, int, int, const std::string&);

	void setRandomMotive(int);

	virtual float getShadowRadius() const;



	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual int getWidth();
	virtual int getHeight();

	virtual void dropItem();
	virtual int getEntityTypeId() const;
	virtual bool isPickable();

	const Motive* motive;
};
