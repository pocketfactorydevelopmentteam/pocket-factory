#pragma once
#include "Mob.h"
class Path;
class TileSource;
class Entity;
class TilePos;














class PathfinderMob : public Mob {


	static const float MAX_TURN;
public:
	PathfinderMob(TileSource&);
	virtual ~PathfinderMob();

	virtual bool canSpawn();

	bool isPathFinding();
	void setPath(Path*);

	virtual Entity* getAttackTarget();
	virtual void setAttackTarget(Entity*);

	virtual float getWalkTargetValue(const TilePos&);
	int getNoActionTime();
protected:
	virtual Entity* findAttackTarget();

	virtual void checkHurtTarget(Entity*, float);
	virtual void checkCantSeeTarget(Entity*, float);

	virtual float getWalkingSpeedModifier();

	virtual bool shouldHoldGround();

	virtual void updateAi();

	virtual void findRandomStrollLocation();
	int attackTargetId;
	bool holdGround;
	int fleeTime;
public:
	static const int CAN_OPEN_DOORS = 1;
	static const int AVOID_WATER = 2;

private:
	Path* path;
};
