#pragma once
#include "Entity.h"
class TileSource;
class Player;
class CompoundTag;
class HangingEntity : public Entity {

public:
	HangingEntity(TileSource&);
	HangingEntity(TileSource&, int, int, int, int);
	void init();
	void setDir(int);
	void setPosition(int, int, int);
	virtual void normalTick();
	virtual bool survives();
	virtual bool isPickable();
	virtual bool interactWithPlayer(Player*);
	virtual void move(float, float, float);
	virtual void push(float, float, float);
	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);
	virtual int getWidth() = 0;
	virtual int getHeight() = 0;
	virtual void dropItem() = 0;
	virtual bool isHangingEntity();
	virtual float getBrightness(float);
	virtual bool hurt(Entity*, int);
private:
	float offs(int);
public:
	int dir;
	int xTile, yTile, zTile;
private:
	int checkInterval;
};
