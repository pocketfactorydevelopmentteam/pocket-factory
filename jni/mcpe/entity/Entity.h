#pragma once
#include <string>
#include <memory>
#include "SynchedEntityData.h"
#include "../phys/ChunkPos.h"
class TileSource;
class Level;
#include "../phys/Vec3.h"
#include "../renderer/Color.h"
#include "../phys/AABB.h"
#include "EntityRendererId.h"
#include "../util/Random.h"
class Material;
class TilePos;
class Player;
class ItemEntity;
class ItemInstance;
class CompoundTag;
class EntityPos;
#include "../phys/Vec2.h"





class Entity {


public:
	static int entityCounter;
	static const int TOTAL_AIR_SUPPLY = 300;
	static const std::string RIDING_TAG;

	Entity(TileSource&);
	Entity(Level&);

	virtual ~Entity();

	void _init();
	virtual void reset();

	TileSource& getRegion() const;




	void setRegion(TileSource&);

	bool operator==(Entity&);

	virtual void remove();

	virtual void setPos(float, float, float);
	virtual void setPos(const Vec3&);
	virtual void move(float, float, float);

	Vec3 getInterpolatedPosition(float) const;
	Vec3 getInterpolatedPosition2(float) const;
	Vec2 getInterpolatedRotation(float) const;

	Vec3 getViewVector(float) const;

	void checkTileCollisions();

	virtual void moveTo(float, float, float, float, float);
	void moveTo(const Vec3&, float, float);



	virtual void moveRelative(float, float, float);

	virtual void lerpTo(float, float, float, float, float, int);
	virtual void lerpMotion(float, float, float);

	virtual void turn(float, float);
	virtual void interpolateTurn(float, float);

	void tick(TileSource&);
	virtual void normalTick();
	virtual void baseTick();

	virtual void rideTick();
	virtual void positionRider(bool);
	virtual float getRidingHeight();
	virtual float getRideHeight();
	virtual void ride(Entity*);

	virtual bool intersects(float, float, float, float, float, float);
	virtual bool isFree(float, float, float, float);
	virtual bool isFree(float, float, float);
	virtual bool isInWall();
	virtual bool isInWater() const;
	virtual bool isInWaterOrRain();
	virtual bool isInLava();
	bool isInClouds() const;

	virtual bool isUnderLiquid(const Material*) const;

	virtual float getShadowRadius() const;



	virtual void makeStuckInWeb();

	virtual float getHeadHeight() const;
	virtual float getShadowHeightOffs();

	Vec3 getRandomPointInAABB(Random&);

	virtual bool isSkyLit(float);
	virtual float getBrightness(float);

	Vec3 getCenter(float);

	float distanceTo(Entity*);
	float distanceTo(float, float, float);
	float distanceTo(const Vec3&);


	float distanceToSqr(float, float, float);
	float distanceToSqr(const Vec3&);



	float distanceToSqr(Entity*);
	float distanceSqrToBlockPosCenter(const TilePos&);

	virtual bool interactPreventDefault();
	virtual bool interactWithPlayer(Player*);
	virtual bool canInteractWith(Player*);
	virtual std::string getInteractText(Player*);
	virtual void playerTouch(Player*);

	virtual void push(Entity*);
	virtual void push(float, float, float);

	virtual bool isPickable();
	virtual bool isPushable();
	virtual bool isShootable();

	virtual bool isSneaking();

	virtual bool isAlive();
	virtual bool isOnFire() const;

	virtual bool isCreativeModeAllowed();

	virtual bool isSurfaceMob() const;



	bool shouldRender();
	virtual bool shouldRenderAtSqrDistance(float);

	virtual bool hurt(Entity*, int);
	virtual void animateHurt();

	virtual void handleEntityEvent(char);

	virtual float getPickRadius();

	virtual ItemEntity* spawnAtLocation(int, int);
	virtual ItemEntity* spawnAtLocation(int, int, float);

	virtual ItemEntity* spawnAtLocation(const ItemInstance&, float);

	virtual void awardKillScore(Entity*, int);

	virtual void setEquippedSlot(int, int, int);

	virtual bool save(CompoundTag&);
	virtual void saveWithoutId(CompoundTag&);
	virtual bool load(CompoundTag&);

	virtual const SynchedEntityData* getEntityData() const;

	bool isEntityType(int);

	virtual int getEntityTypeId() const;



	virtual EntityRendererId queryEntityRenderer();

	bool isInstanceOf(int) const;
	bool isMob() const;

	bool isPlayer() const;



	Player* asPlayer();




	virtual bool isItemEntity();

	virtual bool isHangingEntity();

	virtual int getAuxData();

	void setOnFire(int);

	virtual AABB getHandleWaterAABB() const;

protected:
	virtual void setRot(float, float);
	virtual void setSize(float, float);
	virtual void setPos(EntityPos*);
	virtual void outOfWorld();

	virtual void checkFallDamage(float, bool);
	virtual void causeFallDamage(float);
	virtual void markHurt();

	virtual void burn(int);
	virtual void lavaHurt();

	virtual void readAdditionalSaveData(CompoundTag*) = 0;
	virtual void addAdditionalSaveData(CompoundTag*) = 0;

	virtual void playStepSound(int, int, int, int);

	virtual void checkInsideTiles();

	bool isInWorld() const;




public:
	virtual void playSound(const std::string&, float, float);

	virtual void onSynchedDataUpdate(int);


	SynchedEntityData entityData;
	float x, y, z;
	ChunkPos chunkPos;

	int entityId;

	float viewScale;

	TileSource* region;
	bool added;
	Level* level;
	float xo, yo, zo;
	float xd, yd, zd;
	float yRot, xRot;
	float yRotO, xRotO;
	float xRideRotA, yRideRotA;

	float terrainSurfaceOffset;
	Vec3 lastLightDirection;
	Color lastLightColor;

	AABB bb;

	float heightOffset;

	float bbWidth;
	float bbHeight;

	float walkDistO;
	float walkDist;

	float xOld, yOld, zOld;
	float ySlideOffset;
	float footSize;
	float pushthrough;

	int tickCount;
	int invulnerableTime;
	int airSupply;
	int onFire;
	int flameTime;

	EntityRendererId entityRendererId;

	Entity* rider;
	Entity* riding;









	float fallDistance;
	bool blocksBuilding;

	bool onGround;
	bool horizontalCollision, verticalCollision;
	bool collision;
	bool hurtMarked;

	bool noPhysics;
	bool invisible;
	bool reallyRemoveIfPlayer;

	bool isRiding();
	bool saveAsMount(CompoundTag&);

	bool wasInWater;
	static Random sharedRandom;
	int airCapacity;
	bool makeStepSound;
	bool fireImmune;


	bool firstTick;
	int nextStep;
	static const int DATA_AIR_SUPPLY_ID = 1;
	bool isStuckInWeb;

	void _updateOwnerChunk();

private:
	bool removed;

public:
	const bool isRemoved() const;
};
