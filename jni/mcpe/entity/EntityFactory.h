#pragma once
class Entity;
class TileSource;
class CompoundTag;




class EntityFactory {

public:
	static Entity* CreateEntity(int, TileSource&);
	static Entity* loadEntity(CompoundTag*, TileSource&);
};
