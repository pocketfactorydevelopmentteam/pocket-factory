#pragma once
#include "../Entity.h"
class TileSource;
class Mob;
class Vec3;
class CompoundTag;
class HitResult;








class Throwable : public Entity {


public:
	Throwable(TileSource&);
	Throwable(Mob&);
	Throwable(TileSource&, float, float, float);

	void shoot(const Vec3&, float, float);
	void shoot(float, float, float, float, float);

	virtual void normalTick();
	virtual void lerpMotion(float, float, float);

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual bool shouldRenderAtSqrDistance(float);

	virtual float getShadowHeightOffs();

	virtual int getAuxData();
protected:
	virtual float getThrowPower();
	virtual float getThrowUpAngleOffset();

	virtual float getGravity();

	virtual void onHit(const HitResult&) = 0;

	void _throwableHit(const HitResult&, int);

private:
	void _init();

public:
	int shakeTime;
protected:
	bool inGround;
	int ownerId;
private:
	int life;
	int flightTime;
	int xTile;
	int yTile;
	int zTile;
	int lastTile;
};
