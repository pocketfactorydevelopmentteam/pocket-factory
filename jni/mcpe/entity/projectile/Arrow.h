#pragma once
#include "../Entity.h"
#include "../../level/TilePos.h"
#include "../../../CommonTypes.h"
class TileSource;
class Mob;
class CompoundTag;
class Player;



class Arrow : public Entity {



	static const float ARROW_BASE_DAMAGE;


public:
	Arrow(TileSource&);
	Arrow(TileSource&, float, float, float);
	Arrow(Mob*, Mob*, float, float);
	Arrow(Mob*, float);

	void _init();

	void shoot(float, float, float, float, float);

	virtual void lerpMotion(float, float, float);

	virtual void normalTick();

	virtual int getEntityTypeId() const;

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual void playerTouch(Player*);

	virtual float getShadowHeightOffs();

	virtual float getShadowRadius() const;



	virtual int getAuxData();

	bool playerArrow;
	int shakeTime;
	int ownerId;
	bool critArrow;
private:
	TilePos tileStuckTo;
	FullTile lastStuckTo;
	bool inGround;
	int life;
	int flightTime;
};
