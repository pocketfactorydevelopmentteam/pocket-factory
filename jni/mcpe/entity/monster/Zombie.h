#pragma once
#include "Monster.h"
class TileSource;
class Entity;
#include <string>
#include <memory>




class Zombie : public Monster {


public:
	Zombie(TileSource&);

	virtual ~Zombie();

	virtual int getMaxHealth();

	virtual void aiStep();
	virtual int getEntityTypeId() const;
	virtual void die(Entity*);
	virtual int getAttackDamage(Entity*);

	virtual float getBaseSpeed();
protected:
	virtual int getArmorValue();

	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();


	virtual int getDeathLoot();

	virtual bool useNewAi();

	int fireCheckTick;
};
