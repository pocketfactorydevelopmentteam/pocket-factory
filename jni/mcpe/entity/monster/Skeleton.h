#pragma once
#include "Monster.h"
#include "../../item/ItemInstance.h"
class TileSource;
#include <string>
#include <memory>
class Entity;





class Skeleton : public Monster {


public:
	Skeleton(TileSource&);

	virtual int getMaxHealth();

	virtual bool useNewAi();
	virtual void aiStep();

	virtual int getDeathLoot();

	virtual ItemInstance* getCarriedItem();

	virtual int getEntityTypeId() const;

	virtual float getBaseSpeed();

protected:
	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();

	virtual void checkHurtTarget(Entity*, float);
	virtual void dropDeathLoot();
	virtual int getUseDuration();
private:
	ItemInstance bow;
	int fireCheckTick;
};
