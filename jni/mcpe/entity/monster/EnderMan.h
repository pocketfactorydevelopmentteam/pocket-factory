#pragma once
#include "Monster.h"
class TileSource;
class CompoundTag;
#include "../../../CommonTypes.h"
class Player;
class Entity;
#include <string>
#include <memory>
#include "../ai/goal/Goal.h"
#include "../ai/goal/target/NearestAttackableTargetGoal.h"
#include "../TempEPtr.h"
class EnderMan : public Monster {


public:
	EnderMan(TileSource&);

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual void normalTick();
	virtual void aiStep();
	virtual void newServerAiStep();

	virtual int getMaxHealth();

	FullTile getCarryingBlock();
	void setCarryingBlock(FullTile);	void setCarryingBlock(FullTile);

	bool isCreepy();
	void setCreepy(bool);

	virtual int getEntityTypeId() const;
	virtual bool useNewAi();

	virtual float getBaseSpeed();

	bool isLookingAtMe(Player*);

	virtual bool hurt(Entity*, int);

	bool teleport();
	bool teleport(float, float, float);
	bool teleportTowards(Entity*);

protected:
	virtual int getDeathLoot();

	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();


private:
	static const int DATA_CARRY_BLOCK = 16;
	static const int DATA_CARRY_DATA = 17;
	static const int DATA_CREEPY = 18;

	static bool mayTakeIsSetup;


public:
	bool aggroedByPlayer;
	static bool mayTake[];
};



class EndermanLeaveBlockGoal : public Goal {


	EnderMan* enderman;

public:
	EndermanLeaveBlockGoal(EnderMan*);





	virtual bool canUse();













	virtual void tick();
};


class EndermanTakeBlockGoal : public Goal {


	EnderMan* enderman;

public:
	EndermanTakeBlockGoal(EnderMan*);





	virtual bool canUse();













	virtual void tick();
};


class EndermanLookForPlayerGoal : public NearestAttackableTargetGoal {


	TempEPtr<Player> pendingTarget;
	int aggroTime;
	int teleportTime;
	EnderMan* enderman;

public:
	EndermanLookForPlayerGoal(EnderMan*);







	virtual bool canUse();













	virtual void start();






	virtual void stop();







	virtual bool canContinueToUse();













	virtual void tick();
};
