#pragma once
#include "Monster.h"
class TileSource;
class Entity;
#include <string>
#include <memory>
#include "../ai/goal/Goal.h"
#include "../ai/goal/RandomStrollGoal.h"






class Silverfish : public Monster {


public:
	Silverfish(TileSource&);


	virtual void aiStep();

	virtual void normalTick();


	virtual int getMaxHealth();

	virtual int getEntityTypeId() const;

	virtual float getBaseSpeed();

	virtual bool useNewAi();

	virtual bool hurt(Entity*, int);


protected:
	bool makeStepSound();

	virtual Entity* findAttackTarget();

	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();

	virtual int getDeathLoot();

	SilverfishWakeUpFriendsGoal* friendsGoal;
};

class SilverfishWakeUpFriendsGoal : public Goal {


public:
	SilverfishWakeUpFriendsGoal(Silverfish*);

	void notifyHurt();

	virtual bool canUse();
	virtual void tick();

protected:
	Silverfish* silverfish;
	int lookForFriends;
};

class SilverfishMergeWithStoneGoal : public RandomStrollGoal {


public:
	SilverfishMergeWithStoneGoal(Silverfish*);

	virtual bool canUse();
	virtual bool canContinueToUse();
	virtual void start();

protected:
	Silverfish* silverfish;
	int selectedFace;
	bool doMerge;
};
