#pragma once
#include "Zombie.h"
#include "../../item/ItemInstance.h"
class TileSource;
#include <string>
#include <memory>
class CompoundTag;
class Entity;
class Player;
class PigZombie : public Zombie {

public:
	PigZombie(TileSource&);
	virtual bool useNewAi();
	virtual void normalTick();
	virtual const std::string& getTexture();
	virtual bool canSpawn();
	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);
	virtual bool hurt(Entity*, int);
	virtual bool interactWithPlayer(Player*);
	virtual int getEntityTypeId() const;
	virtual int getAttackTime();
	virtual ItemInstance* getCarriedItem();

	virtual float getBaseSpeed();
protected:
	virtual Entity* findAttackTarget();
	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();
	virtual void dropDeathLoot();
	virtual int getDeathLoot();
private:
	void alert(Entity*);


	int angerTime;
	int playAngrySoundIn;
	int stunedTime;
	ItemInstance weapon;
};
