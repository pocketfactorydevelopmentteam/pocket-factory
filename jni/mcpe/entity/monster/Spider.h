#pragma once
#include "Monster.h"
class TileSource;
class Entity;
#include <string>
#include <memory>
#include "../ai/goal/target/NearestAttackableTargetGoal.h"
class Monster;
#include "../ai/goal/MeleeAttackGoal.h"
class Mob;




class Spider : public Monster {



	static const int DATA_FLAGS_ID = 16;
public:
	Spider(TileSource&);


	virtual void aiStep();

	virtual void normalTick();


	virtual int getMaxHealth();






	virtual bool onLadder();


	virtual void makeStuckInWeb();

	float getModelScale();

	bool isClimbing();
	void setClimbing(bool);

	virtual float getShadowRadius() const;



	virtual int getEntityTypeId() const;

	virtual float getBaseSpeed();

	virtual bool useNewAi();


protected:
	bool makeStepSound();

	virtual Entity* findAttackTarget();

	virtual const char* getAmbientSound();
	virtual std::string getHurtSound();
	virtual std::string getDeathSound();

	virtual int getDeathLoot();
};



class SpiderAttackGoal : public MeleeAttackGoal {

public:
	SpiderAttackGoal(Mob*);





	virtual bool canContinueToUse();










	virtual float getAttackReachSqr();
};




class SpiderTargetGoal : public NearestAttackableTargetGoal {

public:
	SpiderTargetGoal(Monster*);





	virtual bool canUse();
};
