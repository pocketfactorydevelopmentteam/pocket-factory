#pragma once
#include "../PathfinderMob.h"
class TileSource;
class Entity;
class CompoundTag;
class TilePos;






class Monster : public PathfinderMob {


public:
	Monster(TileSource&);

	virtual void aiStep();
	virtual void normalTick();

	virtual bool canSpawn();
	virtual bool shouldDespawn() const;


	virtual bool hurt(Entity*, int);
	virtual bool doHurtTarget(Entity*);

	virtual int getAttackDamage(Entity*);

	void setNightly();

	virtual bool isSurfaceMob() const;



	virtual void addAdditionalSaveData(CompoundTag*);

	virtual void readAdditionalSaveData(CompoundTag*);

protected:
	bool surfaceMonster;

	virtual Entity* findAttackTarget();







	virtual void checkHurtTarget(Entity*, float);

	virtual float getWalkTargetValue(const TilePos&);
	virtual int getAttackTime();
	int attackDamage;

	int lastHurtByMobId;
};
