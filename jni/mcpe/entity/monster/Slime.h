#pragma once
#include "Monster.h"
class TileSource;
#include <string>
#include <memory>
class CompoundTag;
class Player;
class Entity;
#include "../ai/control/MoveControl.h"
#include "../ai/goal/Goal.h"






class Slime : public Monster {



	static const int DATA_SIZE_ID = 16;
public:
	Slime(TileSource&);


	virtual void aiStep();

	virtual void normalTick();


	virtual int getMaxHealth();

	virtual void finalizeMobSpawn();

	void setSlimeSize(int);
	int getSlimeSize() const;
	int getJumpDelay();

	virtual bool doPlayJumpSound();




	virtual void playJumpSound();






	std::string getSquishSound();








	float getModelScale();

	virtual float getShadowRadius() const;



	virtual int getEntityTypeId() const;

	virtual float getBaseSpeed();
	virtual bool isDealsDamage();

	virtual bool useNewAi();

	virtual void addAdditionalSaveData(CompoundTag*);
	virtual void readAdditionalSaveData(CompoundTag*);

	virtual void onSynchedDataUpdate(int);

	virtual void playerTouch(Player*);
	virtual void remove();

	virtual bool canSpawn();

protected:
	bool makeStepSound();

	virtual Entity* findAttackTarget();

	virtual std::string getHurtSound();
	virtual std::string getDeathSound();

	virtual int getDeathLoot();

	virtual void decreaseSquish();




	Slime* createChild();

public:
	float squish, oSquish;
	float targetSquish;
};


class SlimeMoveControl : public MoveControl {

protected:
	float yRot;
	int jumpDelay;
	Slime* slime;
	bool isAggressive;

public:
	SlimeMoveControl(Slime*);







	void setDirection(float, bool);





	void setWantedMovement(float);





	virtual void tick();
};






























class SlimeKeepOnJumpingGoal : public Goal {


	Slime* slime;

public:
	SlimeKeepOnJumpingGoal(Slime*);







	virtual bool canUse();





	virtual void tick();
};




class SlimeRandomDirectionGoal : public Goal {

	Slime* slime;

	float chosenDegrees;
	int nextRandomizeTime;

public:
	SlimeRandomDirectionGoal(Slime*);










	virtual bool canUse();





	virtual void tick();
};









class SlimeAttackGoal : public Goal {


	Slime* slime;
	int growTiredTimer;

public:
	SlimeAttackGoal(Slime*);








	virtual bool canUse();
















	virtual void start();




	virtual bool canContinueToUse();



















	virtual void tick();
};





class SlimeFloatGoal : public Goal {


	Slime* slime;

public:
	SlimeFloatGoal(Slime*);








	virtual bool canUse();




	virtual void tick();
};
