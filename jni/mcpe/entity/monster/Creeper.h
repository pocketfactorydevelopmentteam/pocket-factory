#pragma once
#include "Monster.h"
class TileSource;
class Player;
#include <string>
#include <memory>
class Entity;
#include "../../../client/renderer/renderer/Color.h"


class Creeper : public Monster {


public:
	Creeper(TileSource&);

	virtual int getMaxHealth();

	virtual void normalTick();

	virtual bool interactWithPlayer(Player*);

	float getSwelling(float) const;
	int getSwellDir();
	void setSwellDir(int);

	virtual int getEntityTypeId() const;
	virtual bool useNewAi();

	virtual float getBaseSpeed();

	virtual bool canInteractWith(Player*);

	virtual std::string getInteractText(Player*);

	virtual Color getOverlayColor(float) const;
protected:
	virtual int getDeathLoot();

	virtual void checkCantSeeTarget(Entity*, float);
	virtual void checkHurtTarget(Entity*, float);

	virtual std::string getHurtSound();
	virtual std::string getDeathSound();
private:
	int swell;
	int oldSwell;

	int swellDir;

	static const int DATA_SWELL_DIR = 16;
	static const int MAX_SWELL = 30;
};
