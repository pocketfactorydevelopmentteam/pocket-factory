#pragma once

#include <memory>
#include <vector>
#include <string>

#include "Container.h"
#include "../item/ItemInstance.h"

class ListTag;
class Player;


class FillingContainer : public Container {



	typedef std::vector<ItemInstance *, std::allocator<ItemInstance *> > ItemList;
public:
	static const int MAX_INVENTORY_STACK_SIZE = 254;

	FillingContainer(int, int, int, bool);
	virtual ~FillingContainer();

	bool hasResource(int);
	bool removeResource(int);
	bool removeResource(const ItemInstance&);
	int removeResource(const ItemInstance&, bool);
	void swapSlots(int, int);

	virtual bool add(ItemInstance*);
	FillingContainer::ItemList* getSlotList(int&);

	void dropSlot(int, bool, bool);
	void dropAll(bool);

	virtual ItemInstance removeItem(int, int);

	void clearSlot(int);
	void clearInventory(int);

	ListTag* save(ListTag*);
	void load(ListTag*);
	void replace(std::vector<ItemInstance, std::allocator<ItemInstance> >, int);
	void replaceSlot(int, ItemInstance*);

	virtual void setItem(int, ItemInstance*);
	virtual ItemInstance* getItem(int);

	virtual std::string getName() const;
	virtual int getMaxStackSize() const;
	virtual int getContainerSize() const = 0;

	void setContainerChanged();

	virtual bool stillValid(Player*);
	bool contains(ItemInstance*) const;

	virtual void startOpen();
	virtual void stopOpen();

	virtual void doDrop(ItemInstance*, bool);

	int getSlot(int, int);
	int getNonEmptySlot(int, int);
	int getEmptySlotsCount();
	int getLinkedSlotsCount();

	int getSlot(int);
	int getNonEmptySlot(int);
	int getSlotWithRemainingSpace(const ItemInstance&);
	int getFreeSlot();

	int addResource(const ItemInstance&);
	int addItem(ItemInstance*);

	void release(int);
	void fixBackwardCompabilityItem(ItemInstance&);



	void unlinkSlot(int);
	bool linkSlot(int, int);
	bool linkEmptySlot(int);
	int getLinkedSlot(int);
	ItemInstance* getLinked(int);
	void compressLinkedSlotList(int);

	struct LinkedSlot {
		LinkedSlot();


		LinkedSlot(int);



		int inventorySlot;
	};
	
	std::vector<FillingContainer::LinkedSlot, std::allocator<FillingContainer::LinkedSlot> > linkedSlots;
	FillingContainer::ItemList items;

	bool _isCreative;
};
