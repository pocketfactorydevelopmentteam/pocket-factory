#pragma once
#include "../Container.h"
#include <string>
#include <memory>
class Player;
#include "../item/ItemInstance.h"




class CraftingContainer : public Container {


public:
	CraftingContainer(int, int);








	virtual ~CraftingContainer();



	virtual ItemInstance* getItem(int);






	ItemInstance* getItem(int, int);








	virtual void setItem(int, ItemInstance*);





















	virtual int getContainerSize() const;


	virtual int getMaxStackSize() const;


	virtual std::string getName() const;



	void setContainerChanged();

	virtual bool stillValid(Player*);



	virtual void startOpen();
	virtual void stopOpen();

private:
	ItemInstance* items;
	int size;
	int width;
};
