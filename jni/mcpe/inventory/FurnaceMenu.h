#pragma once
#include "BaseContainerMenu.h"
class FurnaceTileEntity;
#include "../item/ItemInstance.h"
#include <memory>
#include <vector>





class FurnaceMenu : public BaseContainerMenu {


public:
	FurnaceMenu(FurnaceTileEntity*);

	virtual ~FurnaceMenu();

	virtual void setSlot(int, ItemInstance*);
	virtual void setData(int, int);
	virtual std::vector<ItemInstance, std::allocator<ItemInstance> > getItems();

	virtual void broadcastChanges();
	virtual void setListener(IContainerListener*);

	virtual bool tileEntityDestroyedIsInvalid(int);

	FurnaceTileEntity* furnace;

	int furnaceTileEntityId;
private:
	int lastTickCount;
	int lastLitTime;
	int lastLitDuration;
};
