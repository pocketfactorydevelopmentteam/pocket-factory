#pragma once
#include <string>
#include <memory>
#include <vector>

#include "../item/ItemInstance.h"

class Player;

class Container {

public:
	static const int LARGE_MAX_STACK_SIZE = 64;

	Container(int);



	virtual ~Container();

	virtual ItemInstance* getItem(int) = 0;
	virtual void setItem(int, ItemInstance*) = 0;
	virtual ItemInstance removeItem(int, int) = 0;

	virtual std::string getName() const = 0;
	virtual int getContainerSize() const = 0;
	virtual int getMaxStackSize() const = 0;

	virtual bool stillValid(Player*) = 0;

	virtual void startOpen() = 0;
	virtual void stopOpen() = 0;

	virtual std::vector<ItemInstance, std::allocator<ItemInstance> > getSlotCopies();









	int containerId;
	int containerType;
};
