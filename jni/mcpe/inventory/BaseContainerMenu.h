#pragma once
#include "../item/ItemInstance.h"
#include <memory>
#include <vector>






class IContainerListener {
public:
	virtual ~IContainerListener();
	virtual void refreshContainer(BaseContainerMenu*, const std::vector<ItemInstance, std::allocator<ItemInstance> >&);
	virtual void slotChanged(BaseContainerMenu*, int, const ItemInstance&, bool);
	virtual void setContainerData(BaseContainerMenu*, int, int);
};



class BaseContainerMenu {

	typedef std::vector<ItemInstance, std::allocator<ItemInstance> > ItemList;
public:
	BaseContainerMenu(int);
	virtual ~BaseContainerMenu();

	virtual BaseContainerMenu::ItemList getItems() = 0;

	virtual void setSlot(int, ItemInstance*) = 0;
	virtual void setData(int, int);

	virtual void setListener(IContainerListener*);
	virtual void broadcastChanges();

	virtual bool isResultSlot(int);

	virtual bool tileEntityDestroyedIsInvalid(int) = 0;

	int containerId;
	int containerType;
	IContainerListener* listener;

	BaseContainerMenu::ItemList lastSlots;
};
