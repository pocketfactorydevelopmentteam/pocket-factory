#pragma once
#include "BaseContainerMenu.h"
class Container;
#include "../item/ItemInstance.h"
#include <memory>
#include <vector>





class ContainerMenu : public BaseContainerMenu {


public:
	ContainerMenu(Container*, int);

	virtual ~ContainerMenu();

	virtual void setSlot(int, ItemInstance*);
	virtual std::vector<ItemInstance, std::allocator<ItemInstance> > getItems();

	virtual bool tileEntityDestroyedIsInvalid(int);

	Container* container;
protected:
	int tileEntityId;
};
