#pragma once
#include "BaseContainerMenu.h"
class Container;
#include "../item/ItemInstance.h"
#include <memory>
#include <vector>





class InventoryMenu : public BaseContainerMenu {


public:
	InventoryMenu(Container*);

	virtual ~InventoryMenu();

	virtual void setSlot(int, ItemInstance*);
	virtual std::vector<ItemInstance, std::allocator<ItemInstance> > getItems();

	virtual bool tileEntityDestroyedIsInvalid(int);
private:
	Container* container;
};
