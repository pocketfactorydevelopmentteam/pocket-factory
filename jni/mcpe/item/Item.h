#pragma once

#include <string>
#include <memory>

#include "ItemInstance.h"
#include "../CommonType.h"
#include "../Tile/Tile.h"
#include "../Texture/TextureAtlas.h"
#include "../Texture/TextureUVCoordinateSet.h"

class ItemInstance;
class Tile;
class Item
{
public:
	//void** vtable;
	int maxStackSize;
	char random[12];
	const int id;
	int maxDamage;
	char texture[32];
	int creativeCategory;
	char rest[12];

    Item(int);

    virtual ~Item();
    virtual int getMaxStackSize(const ItemInstance*);
    virtual void setMaxStackSize(int);
	virtual bool canBeDepleted();
	virtual TextureUVCoordinateSet getIcon(int, int, bool) const;
	virtual int getIconYOffset() const;
	virtual void setIcon(std::string const&, int);
	virtual void setIcon(TextureUVCoordinateSet);
	virtual bool isMirroredArt() const;
	virtual void use(ItemInstance&, Player&);
	virtual void useOn(ItemInstance*, Player*, int, int, int, signed char, float, float, float);
	virtual int getMaxUseDuration() const;
	virtual void useTimeDepleted(ItemInstance*, Level*, Player*);
	virtual int getUseAnimation() const;
	virtual void releaseUsing(ItemInstance*, Player*, int);
	virtual float getDestroySpeed(ItemInstance*, Tile*);
	virtual bool canDestroySpecial(const Tile*) const;
	virtual void* getLevelDataForAuxValue(int) const;
	virtual bool isStackedByData() const;
	virtual int getMaxDamage();
	virtual int getAttackDamage(Entity*);
	virtual void hurtEnemy(ItemInstance*, Mob*, Mob*);
	virtual void interactEnemy(ItemInstance*, Mob*, Player*);
	virtual void mineBlock(ItemInstance*, int, int, int, int, Mob*);
	virtual void handEquipped();
	virtual bool isHandEquipped() const;
	virtual bool isFood() const;
	virtual bool isSeed() const;
	virtual bool isArmor() const;
	virtual bool isLiquidClipItem(int) const;
	virtual std::string getName(const ItemInstance*) const;
	virtual std::string getDescription() const;
	virtual std::string getDescription(const ItemInstance*) const;
	virtual std::string getDescriptionId() const;
	virtual std::string getDescriptionId(const ItemInstance*) const;
	virtual void setDescriptionId(std::string const&);
	virtual bool isEmissive(int) const;
	virtual int getAnimationFrameFor(Mob&) const;

	static Item* items[512];

	static void initItems();
};
