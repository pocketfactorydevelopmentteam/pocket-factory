#pragma once

#include "Item.h"
#include "../tile/Tile.h"
#include "../CommonType.h"

class Item;
struct TextureUVCoordinateSet;

class ItemInstance
{
public:

	ItemInstance();
	ItemInstance(bool);
	ItemInstance(const Tile*);
	ItemInstance(const Tile*, int);
	ItemInstance(const Tile*, int, int);
	ItemInstance(const Item*);
	ItemInstance(const Item*, int);
	ItemInstance(const Item*, int, int);
	ItemInstance(const ItemInstance&);
	ItemInstance(int, int, int);

	void init(int, int, int);
	bool isNull() const;
	void setNull();
	bool operator==(const ItemInstance&) const;
	bool matches(const ItemInstance*) const;



	ItemInstance remove(int);

	const TextureUVCoordinateSet& getIcon(int, bool) const;

	Item* getItem() const;



	Tile* getTile() const;



	int getId() const;

	bool isInstance(Item*) const;



	bool isInstance(Tile*) const;



	bool isItem() const;


	bool isValid() const;


	bool isEmpty() const;



	float getDestroySpeed(Tile*);

	bool useOn(Player*, int, int, int, FacingID, float, float, float);
	ItemInstance& use(Player&);





	bool isStackedByData() const;
	bool isStackable() const;
	int getMaxStackSize() const;
	static bool isStackable(const ItemInstance*, const ItemInstance*);

	bool isDamaged() const;
	bool isDamageableItem() const;
	int getDamageValue() const;
	int getMaxDamage() const;

	int getAuxValue() const;
	void setAuxValue(int);

	void hurtAndBreak(int, Mob*);
	void hurtEnemy(Mob*, Mob*);

	void mineBlock(int, int, int, int, Mob*);
	int getAttackDamage(Entity*);
	bool canDestroySpecial(Tile*);
	void snap(Player*);
	ItemInstance useTimeDepleted(Level*, Player*);
	bool isLiquidClipItem();

	void interactEnemy(Mob*, Player*);


	static bool matches(const ItemInstance*, const ItemInstance*);
	static bool matchesNulls(const ItemInstance*, const ItemInstance*);

	static bool isArmorItem(const ItemInstance*);
	static bool isItem(const ItemInstance*);

	bool sameItem(ItemInstance*) const;



	bool sameItemAndAux(ItemInstance*) const;

	static ItemInstance* clone(const ItemInstance*);
	static ItemInstance cloneSafe(const ItemInstance*);

	std::string getDescriptionId() const;
	std::string getName() const;
	ItemInstance* setDescriptionId(const std::string&);
	std::string toString() const;

	CompoundTag* save(CompoundTag*);
	void load(CompoundTag*);
	static ItemInstance* fromTag(CompoundTag*);

	void releaseUsing(Player*, int);
	int getMaxUseDuration() const;

	//UseAnim::UseAnimation getUseAnimation() const;

	void useAsFuel();


	int count;






	int auxValue;

	Item* item;
	Tile* tile;

	bool valid;

	bool _setItem(int);
};
