#pragma once
#include <memory>
#include "../renderer/MaterialPtr.h"
#include "../phys/Vec3.h"
#include "../entity/Entity.h"

class TileTessellator;
class Textures;
class ItemInstance;

class EntityShaderManager
{
public:
	EntityShaderManager();
};

class EntityRenderer : EntityShaderManager {
public:
	EntityRenderer();
};


class ItemRenderer : public EntityRenderer {
public:
	std::unique_ptr<TileTessellator> tileTessellator;
	float rndFloats[16];
	bool inited;
	MaterialPtr entityAlphatestMat;
	MaterialPtr uiFillMat;
	MaterialPtr uiBlitMat;
	MaterialPtr uiIconBlitMat;

	static ItemRenderer& singleton();

	void render(Entity&, Vec3 const&, float, float);
	void renderGuiItemNew(Textures*, ItemInstance const*,int,float,float,float,float,float);
};
