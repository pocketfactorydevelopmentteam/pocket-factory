#pragma once
class FoodItem;
class Player;





class SimpleFoodData {

public:
	SimpleFoodData();

	void eat(int);
	void eat(FoodItem*);

	void tick(Player*);
private:
	int foodLevel;
};
