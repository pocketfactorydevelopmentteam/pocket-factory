#pragma once
#include <memory>
#include <vector>
#include <iterator>


template<typename T> class SmallSet {





	typedef typename std::vector<T>::iterator iterator;
	typedef typename std::vector<T>::const_iterator const_iterator;

















public:
	SmallSet(int);




	SmallSet<T>::iterator find(const T&);



	bool contains(const T&) const;








	void erase(const SmallSet<T>::iterator&);




	void erase(const T&);





	T& operator[](int);




	SmallSet<T>::const_iterator begin() const;


	SmallSet<T>::const_iterator end() const;

	size_t size() const;



	bool empty() const;



	void clear();




protected:
	std::vector<T> c;
};
