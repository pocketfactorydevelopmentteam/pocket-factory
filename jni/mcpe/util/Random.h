#pragma once
#include "../CommonType.h"
#include "../phys/Vec3.h"

class Random {


public:
	Random();


	Random(RandomSeed);



	void setSeed(RandomSeed);






	RandomSeed getSeed();


	bool nextBoolean();


	float nextFloat();



	float nextFloat(float);



	float nextFloat(float, float);



	double nextDouble();



	int nextInt();



	int nextInt(int);




	int nextInt(int, int);



	int nextLong();


	int nextLong(int);




	int nextUnsignedInt();



	int nextUnsignedInt(unsigned int);




	unsigned char nextUnsignedChar();



	float nextGaussian();

















public:
	RandomSeed _seed;


	static const int N = 624;
	static const int M = 397;
	static const unsigned int MATRIX_A = 2567483615;
	static const unsigned int UPPER_MASK = 2147483648;
	static const unsigned int LOWER_MASK = 2147483647;

	long unsigned int _mt[624];
	int _mti;

	bool haveNextNextGaussian;
	float nextNextGaussian;


	void init_genrand(long unsigned int);


















	void init_by_array(long unsigned int*, int);

























	long unsigned int genrand_int32();









































	long int genrand_int31();





	double genrand_real1();






	double genrand_real2();






	double genrand_real3();






	double genrand_res53();









	void rrDiff(float&);





	void rrDiff(float&, float&);








	void rrDiff(float&, float&, float&);














public:
	void next2float16(float&, float&);






	void next4float8(float&, float&, float&, float&);







	Vec3 nextVec3();
};
