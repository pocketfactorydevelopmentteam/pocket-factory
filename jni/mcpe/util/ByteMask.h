#pragma once




class ByteMask {

public:
	void setEmpty();



	void add(unsigned char);




	bool contains(unsigned char) const;



	unsigned char toByte() const;



protected:
	unsigned char mask;
};
