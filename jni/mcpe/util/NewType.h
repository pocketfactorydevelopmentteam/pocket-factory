#pragma once





template<typename T> struct NewType {
	typedef T Raw;
	Raw value;

	NewType(const Raw&);
	NewType();
	NewType(const NewType<T>&);
	NewType<T>& operator=(const NewType<T>&);

	//Raw& operator T&();



	bool operator==(const NewType<T>&) const;
	bool operator<(const NewType<T>&) const;
};
