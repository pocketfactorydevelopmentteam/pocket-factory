#pragma once
#include <string>
#include <memory>






struct MemoryStats {
	unsigned int totalBytes;
	int elements;

	MemoryStats(unsigned int, int);





	MemoryStats& operator+=(const MemoryStats&);





	bool operator<(const MemoryStats&) const;
};



class MemoryTracker {


protected:
	static MemoryTracker* root;

public:
	static void memoryStats();

	MemoryTracker(const std::string&, MemoryTracker*);

	virtual ~MemoryTracker();

	virtual MemoryStats getStats() const = 0;


protected:
	MemoryTracker();
};
