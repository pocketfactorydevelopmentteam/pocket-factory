#pragma once
#include <string>
#include <memory>
#include <vector>



struct Token {

	typedef std::vector<Token, std::allocator<Token> > List;

enum class Type : int { String, Number };




	Token(const std::string&);

	bool isNumber() const;



	int getValue(int) const;

	bool getBool(bool) const;

	const std::string& getText(const std::string&) const;

	bool compatibleWith(Token::Type) const;

protected:
	std::string text;
	int value;
	Token::Type type;
	bool isDefault;

	bool _parseRandom();



public:
	static Token::List tokenize(const std::string&);
};
