#pragma once
class MouseAction;



class ITouchScreenModel {

public:
	virtual ~ITouchScreenModel();
	virtual int getPointerId(const MouseAction&);
	virtual int getPointerId(int, int, int);
};
