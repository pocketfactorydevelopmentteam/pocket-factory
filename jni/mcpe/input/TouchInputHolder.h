#pragma once
#include "../gui/gui/GuiComponent.h"
#include "ITurnInput.h"
#include "IBuildInput.h"
#include "../../util/SmoothFloat.h"
#include "TouchAreaModel.h"
class IInputHolder;
class Options;
class MinecraftClient;
class Config;
class Entity;
class Player;
#include "TouchscreenInput.h"
class IMoveInput;
class ITurnInput;
class IBuildInput;



































class UnifiedTurnBuild : public GuiComponent, public ITurnInput, public IBuildInput {



public:
	static const int MODE_OFFSET = 1;
	static const int MODE_DELTA = 2;

	UnifiedTurnBuild(int, int, int, float, float, IInputHolder*, MinecraftClient*);


























	void setSensitivity(float);



	virtual void onConfigChanged(const Config&);



















	float calcNewAlpha(float, float);










	virtual TurnDelta getTurnDelta();






































































































	void updateFeedbackProgressAlpha(float);



















	bool isInsideArea(float, float);



	int mode;

	static float getSpeedSquared(Entity*);







	void render(float);












	virtual bool tickBuild(Player*, BuildActionIntention*);












































	bool allowPicking;
	float alpha;
	SmoothFloat smoothAlpha;

	RectangleArea screenArea;
	RectangleArea moveArea;
	RectangleArea joyTouchArea;
	RectangleArea inventoryArea;

private:
	IInputHolder* _holder;


	int cid;
	float cxO, cyO;
	bool wasActive;

	TouchAreaModel _model;
	IncludeExcludeArea _area;

	bool _decidedTurnMode;

	double _startTurnTime;
	float _totalMoveDelta;
	float _maxMovement;
	float _sensitivity;


	double _lastBuildDownTime;
	float _buildMovement;
	bool _sentFirstRemove;
	bool _canDestroy;
	bool _forceCanUse;

	int state;
	static const int State_None = 0;
	static const int State_Deciding = 1;
	static const int State_Turn = 2;
	static const int State_Destroy = 3;
	static const int State_Build = 4;

	static const int MaxBuildMovement = 20;
	static const int RemovalMilliseconds = 400;

	static const int AREA_TURN = 100;
	Options* _options;
	MinecraftClient* _mc;
};







class TouchInputHolder : public IInputHolder {

public:
	TouchInputHolder(MinecraftClient*, Options*);






	virtual ~TouchInputHolder();


	virtual void onConfigChanged(const Config&);







	virtual bool allowPicking();

























	virtual bool allowInteract();













	virtual void render(float);



	virtual IMoveInput* getMoveInput();
	virtual ITurnInput* getTurnInput();
	virtual IBuildInput* getBuildInput();

private:
	TouchscreenInput _move;
	UnifiedTurnBuild _turnBuild;

	MinecraftClient* _nc;

	static const int MovementLimit = 200;
};
