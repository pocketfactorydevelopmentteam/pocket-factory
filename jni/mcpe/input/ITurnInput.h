#pragma once
#include "../game/IConfigListener.h"






class TurnDelta {
public:
	TurnDelta(float, float);




	float x, y;
};


class ITurnInput : public IConfigListener {
public:
	virtual ~ITurnInput();
	virtual void onConfigChanged(const Config&);

	virtual TurnDelta getTurnDelta() = 0;
protected:
	ITurnInput();



	float getDeltaTime();









	static float linearTransform(float, float, float, bool);






private:
	double _lastTime;
};
