#pragma once
#include "IMoveInput.h"
#include "../gui/gui/GuiComponent.h"
#include "TouchAreaModel.h"
class Options;
class MinecraftClient;
#include "../renderer/renderer/Mesh.h"
class Config;
class Player;
class Tessellator;








class TouchscreenInput : public IMoveInput, public GuiComponent {


public:
	static const float BUTTONS_TRANSPARENCY;

	static const int KEY_UP = 0;
	static const int KEY_DOWN = 1;
	static const int KEY_LEFT = 2;
	static const int KEY_RIGHT = 3;
	static const int KEY_JUMP = 4;
	static const int KEY_SNEAK = 5;
	static const int KEY_CRAFT = 6;
	static const int NumKeys = 7;

	TouchscreenInput(MinecraftClient*, Options*);
	virtual ~TouchscreenInput();

	virtual void onConfigChanged(const Config&);

	virtual void tick(Player*);
	virtual void render(float);

	virtual void setKey(int, bool);
	virtual void releaseAllKeys();

	const RectangleArea& getRectangleArea();
	const RectangleArea& getPauseRectangleArea();

	bool allowPicking(float, float);
private:
	void clear();

	RectangleArea _boundingRectangle;

	bool _keys[8];
	Options* _options;

	bool _pressedJump;
	bool _forward;
	bool _northJump;
	bool _renderFlightImage;
	TouchAreaModel _model;
	MinecraftClient* _minecraft;

	RectangleArea* aLeft;
	RectangleArea* aRight;
	RectangleArea* aUp;
	RectangleArea* aDown;
	RectangleArea* aPause;
	RectangleArea* aChat;
	RectangleArea* aDebug;
	RectangleArea* aInteract;

	RectangleArea* aJump;
	RectangleArea* aUpLeft;
	RectangleArea* aUpRight;
	bool _pauseIsDown;

	Mesh _render;
	bool _allowHeightChange;
	float _sneakTapTime;

	bool _buttons[8];
	bool isButtonDown(int);
	void rebuild();

	static void drawRectangleArea(Tessellator&, RectangleArea*, int, int, float);
	static void drawRectangleArea(Tessellator&, RectangleArea*, int, int, float, float);
	bool canInteract();
};
