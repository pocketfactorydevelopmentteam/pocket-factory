#pragma once
#include "ITouchScreenModel.h"
#include <memory>
#include <vector>
class MouseAction;



class IArea {
public:
	IArea();



	virtual ~IArea();
	virtual bool isInside(float, float) = 0;


	bool deleteMe;
};


class TouchAreaModel : public ITouchScreenModel {

public:
	TouchAreaModel();

	virtual ~TouchAreaModel();



	void clear();









	virtual int getPointerId(const MouseAction&);



	virtual int getPointerId(int, int, int);















	void addArea(int, IArea*);








struct Area {
	IArea* area;
	int areaId;
};
private:
	std::vector<TouchAreaModel::Area *, std::allocator<TouchAreaModel::Area *> > _areas;
};









































class RectangleArea : public IArea {

public:
	RectangleArea(float, float, float, float);






	virtual bool isInside(float, float);




	virtual float centerX();




	virtual float centerY();





	float _x0, _x1;
	float _y0, _y1;
};





















class IncludeExcludeArea : public IArea {

public:
	IncludeExcludeArea();

	virtual ~IncludeExcludeArea();



	void clear();














	void include(IArea*);
	void exclude(IArea*);

	virtual bool isInside(float, float);
















private:
	std::vector<IArea *, std::allocator<IArea *> > _includes;
	std::vector<IArea *, std::allocator<IArea *> > _excludes;
};
