#pragma once
#include "KeyboardInput.h"
class Options;
class Player;
class ControllerMoveInput : public KeyboardInput {


public:
	ControllerMoveInput(Options*);
	virtual ~ControllerMoveInput();

	virtual void tick(Player*);

	virtual void setKey(int, bool);
	virtual void releaseAllKeys();

private:
	bool mAscend;
	bool mDescend;
};
