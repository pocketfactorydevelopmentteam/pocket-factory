#pragma once
class ITurnInput;





class MouseHandler {

public:
	MouseHandler(ITurnInput*);
	MouseHandler();
	~MouseHandler();

	void setTurnInput(ITurnInput*);

	void grab();
	void release();

	void poll();

	float xd, yd;
private:
	int toSkip;
	ITurnInput* _turnInput;
};
