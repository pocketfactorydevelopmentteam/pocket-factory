#pragma once
#include "IBuildInput.h"
class Player;
class ControllerBuildInput : public IBuildInput {


public:
	ControllerBuildInput();
	virtual ~ControllerBuildInput();

	virtual bool tickBuild(Player*, BuildActionIntention*);

	static bool isPerformingUse();
	static bool isPerformingRemove();
private:
	bool mSentFirstRemove;
	int mLastBuildTime;
};
