#pragma once
#include "../game/IConfigListener.h"
class IMoveInput;
class ITurnInput;
class IBuildInput;






class IInputHolder : public IConfigListener {

public:
	IInputHolder();





	virtual ~IInputHolder();

	virtual void render(float);

	virtual bool allowPicking();





	virtual bool allowInteract();



	virtual void onConfigChanged(const Config&);





	virtual IMoveInput* getMoveInput() = 0;
	virtual ITurnInput* getTurnInput() = 0;
	virtual IBuildInput* getBuildInput() = 0;

	float mousex, mousey;
	float alpha;
};


class CustomInputHolder : public IInputHolder {

public:
	CustomInputHolder(IMoveInput*, ITurnInput*, IBuildInput*);




	virtual ~CustomInputHolder();




	void setInputs(IMoveInput*, ITurnInput*, IBuildInput*);





	virtual IMoveInput* getMoveInput();
	virtual ITurnInput* getTurnInput();
	virtual IBuildInput* getBuildInput();

private:
	IMoveInput* _move;
	ITurnInput* _turn;
	IBuildInput* _build;
};
