#pragma once
class Player;
class Config;






class IMoveInput {

protected:
	IMoveInput();









public:
	virtual ~IMoveInput();

	virtual void tick(Player*);
	virtual void render(float);

	virtual void setKey(int, bool);
	virtual void releaseAllKeys();

	virtual void onConfigChanged(const Config&);


	float xa;
	float ya;

	bool wasJumping;
	bool jumping;
	bool sneaking;

	bool wantUp;
	bool wantDown;
	bool isChangingFlightHeight;
};
