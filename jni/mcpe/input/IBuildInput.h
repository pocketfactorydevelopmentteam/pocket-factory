#pragma once
#include "../game/IConfigListener.h"
class Player;



class BuildActionIntention {
public:
	BuildActionIntention();


	BuildActionIntention(int);



	bool isFirstRemove();
	bool isRemoveContinue();

	bool isBuild();
	bool isRemove();

	bool isAttack();
	bool isInteract();

	int action;

	static const int BAI_BUILD = 1;
	static const int BAI_REMOVE = 2;
	static const int BAI_FIRSTREMOVE = 4;

	static const int BAI_ATTACK = 8;
	static const int BAI_INTERACT = 16;
};


class IBuildInput : public IConfigListener {
public:
	virtual ~IBuildInput();

	virtual void onConfigChanged(const Config&);

	virtual bool tickBuild(Player*, BuildActionIntention*);
};
