#pragma once
#include <string>
#include <memory>



	typedef std::vector<std::basic_string<char>, std::allocator<std::basic_string<char> > > StringVector;
class OptionsFile {

public:
	void save(const StringVector&);
	StringVector getOptionStrings();
	void setSettingsFolderPath(const std::string&);

private:
	std::string settingsPath;
};
