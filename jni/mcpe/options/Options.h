#pragma once
#include <string>
#include <memory>
#include <vector>
class MinecraftClient;
#include "OptionsFile.h"







	typedef std::vector<std::basic_string<char>, std::allocator<std::basic_string<char> > > StringVector;

class Options {

public:
enum OptionType { OptionBoolean, OptionProgress, OptionString, OptionInt };






class KeyMapping {
public:
	std::string name;
	int key;

	KeyMapping();

	KeyMapping(const std::string&, int);
};



class Option {

	const Options::OptionType _optionType;
	const std::string _captionId;
	const int _ordinal;

public:
	static const Options::Option MUSIC;
	static const Options::Option SOUND;
	static const Options::Option INVERT_MOUSE;
	static const Options::Option SENSITIVITY;
	static const Options::Option VIEW_DISTANCE;
	static const Options::Option VIEW_BOBBING;
	static const Options::Option LIMIT_FRAMERATE;
	static const Options::Option DIFFICULTY;
	static const Options::Option GRAPHICS;
	static const Options::Option GUI_SCALE;

	static const Options::Option THIRD_PERSON;
	static const Options::Option HIDE_GUI;
	static const Options::Option SERVER_VISIBLE;
	static const Options::Option LEFT_HANDED;
	static const Options::Option USE_TOUCHSCREEN;
	static const Options::Option USE_TOUCH_JOYPAD;
	static const Options::Option DESTROY_VIBRATION;

	static const Options::Option FANCY_SKIES;
	static const Options::Option ANIMATE_TEXTURES;
	static const Options::Option PIXELS_PER_MILLIMETER;

	static const Options::Option NAME;

	static const Options::Option LIMIT_WORLD_SIZE;
	static const Options::Option GAMMA;












	Option(int, const std::string&, Options::OptionType);





	bool isProgress() const;



	bool isBoolean() const;



	bool isString() const;



	bool isInt() const;



	int getId();



	std::string getCaptionId() const;
};



private:
	static const float SOUND_MIN_VALUE;
	static const float SOUND_MAX_VALUE;
	static const float MUSIC_MIN_VALUE;
	static const float MUSIC_MAX_VALUE;
	static const float SENSITIVITY_MIN_VALUE;
	static const float SENSITIVITY_MAX_VALUE;
	static const float PIXELS_PER_MILLIMETER_MIN_VALUE;
	static const float PIXELS_PER_MILLIMETER_MAX_VALUE;
	static const char* RENDER_DISTANCE_NAMES[];
	static const char* DIFFICULTY_NAMES[];
	static const char* GUI_SCALE[];
	static const std::vector<int, std::allocator<int> > DIFFICULTY_LEVELS;

	static const std::vector<int, std::allocator<int> > RENDERDISTANCE_LEVELS_512PLUS;
	static const std::vector<int, std::allocator<int> > RENDERDISTANCE_LEVELS_256PLUS;
	static const std::vector<int, std::allocator<int> > RENDERDISTANCE_LEVELS_LOMEM;

public:
	static bool debugGl;

	float music;
	float sound;
	float sensitivity;

	float gameSensitivity;
	bool invertYMouse;
	int viewDistanceBlocks;
	bool bobView;
	bool limitFramerate;
	bool fancyGraphics;
	bool preferPolyTessellation;
	bool useMouseForDigging;
	bool isLeftHanded;
	bool destroyVibration;


	Options::KeyMapping keyUp;
	Options::KeyMapping keyLeft;
	Options::KeyMapping keyDown;
	Options::KeyMapping keyRight;
	Options::KeyMapping keyJump;
	Options::KeyMapping keyBuild;
	Options::KeyMapping keyDrop;
	Options::KeyMapping keyChat;
	Options::KeyMapping keyFog;
	Options::KeyMapping keySneak;
	Options::KeyMapping keyCraft;
	Options::KeyMapping keyDestroy;
	Options::KeyMapping keyUse;

	Options::KeyMapping keyMenuNext;
	Options::KeyMapping keyMenuPrevious;
	Options::KeyMapping keyMenuOk;
	Options::KeyMapping keyMenuCancel;

	Options::KeyMapping* keyMappings[16];

	MinecraftClient* minecraft;


	int difficulty;
	bool hideGui;
	bool thirdPersonView;
	bool renderDebug;
	float gamma;

	bool isFlying;
	bool smoothCamera;
	bool fixedCamera;
	float flySpeed;
	float cameraSpeed;
	int guiScale;
	std::string username;

	bool serverVisible;
	bool isJoyTouchArea;
	bool useTouchScreen;
	bool fancySkies;
	bool animateTextures;
	int lastMajorVersion;
	int lastMinorVersion;
	int lastPatchVersion;
	int lastBetaVersion;
	float pixelsPerMillimeter;
	bool dev_autoLoadLevel, dev_showChunkMap, dev_disableFileSystem;
	bool limitWorldSize;
	std::string flatWorldLayers;

	void init(MinecraftClient*, std::string);
	void initDefaultValues();

	std::string getKeyDescription(int);

	std::string getKeyMessage(int);

	void setKey(int, int);

	void set(const Options::Option*, float);
	void set(const Options::Option*, int);
	void set(const Options::Option*, std::string);

	void toggle(const Options::Option*, int);

	int getIntValue(const Options::Option*);

	float getProgressValue(const Options::Option*);

	std::string getStringValue(const Options::Option*);

	bool getBooleanValue(const Options::Option*);

	float getProgrssMin(const Options::Option*);

	float getProgrssMax(const Options::Option*);

	std::vector<int, std::allocator<int> > getValues(const Options::Option*);

	std::string getMessage(const Options::Option*);

	std::string getDescription(const Options::Option*, const std::string);

	void update();

	void updateGameSensitivity();

	void load();
	void save();
	void validateVersion();

	void addOptionToSaveOutput(StringVector&, std::string, bool);
	void addOptionToSaveOutput(StringVector&, std::string, float);
	void addOptionToSaveOutput(StringVector&, std::string, int);
	void addOptionToSaveOutput(StringVector&, std::string, std::string);
	void notifyOptionUpdate(const Options::Option*, bool);
	void notifyOptionUpdate(const Options::Option*, float);
	void notifyOptionUpdate(const Options::Option*, int);
	bool canModify(const Options::Option*);
	bool hideOption(const Options::Option*);

	void setAdditionalHiddenOptions(const std::vector<const Options::Option *, std::allocator<const Options::Option *> >&);
private:
	static bool readFloat(const std::string&, float&);
	static bool readInt(const std::string&, int&);
	static bool readBool(const std::string&, bool&);
	std::string formatDescriptionString(const char*, const char**, int);
	const std::vector<int, std::allocator<int> >& _renderDistanceLevels();


	OptionsFile optionsFile;
	std::vector<const Options::Option *, std::allocator<const Options::Option *> > additionalHiddenOptions;
};
