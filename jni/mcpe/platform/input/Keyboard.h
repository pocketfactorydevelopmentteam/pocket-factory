#pragma once
#include <memory>
#include <vector>
#include <string>


class KeyboardAction {

public:
	static const int KEYUP = 0;
	static const int KEYDOWN = 1;

	KeyboardAction(unsigned char, int);




	int state;
	unsigned char key;
};

class KeyboardTextInput {


public:
	KeyboardTextInput(const std::string&, bool);





	std::string text;
	bool keepImePosition;
};







class Keyboard {

public:
	static const int KEY_BACKSPACE = 8;
	static const int KEY_RETURN = 13;

	static const int KEY_LSHIFT = 16;
	static const int KEY_ESCAPE = 27;
	static const int KEY_SPACE = 32;
	static const int KEY_PG_DOWN = 33;
	static const int KEY_PG_UP = 34;

	static const int KEY_LEFT = 37;
	static const int KEY_UP = 38;
	static const int KEY_RIGHT = 39;
	static const int KEY_DOWN = 40;

	static const int KEY_A = 65;
	static const int KEY_B = 66;
	static const int KEY_C = 67;
	static const int KEY_D = 68;
	static const int KEY_E = 69;
	static const int KEY_F = 70;
	static const int KEY_G = 71;
	static const int KEY_H = 72;
	static const int KEY_I = 73;
	static const int KEY_J = 74;
	static const int KEY_K = 75;
	static const int KEY_L = 76;
	static const int KEY_M = 77;
	static const int KEY_N = 78;
	static const int KEY_O = 79;
	static const int KEY_P = 80;
	static const int KEY_Q = 81;
	static const int KEY_R = 82;
	static const int KEY_S = 83;
	static const int KEY_T = 84;
	static const int KEY_U = 85;
	static const int KEY_V = 86;
	static const int KEY_W = 87;
	static const int KEY_X = 88;
	static const int KEY_Y = 89;
	static const int KEY_Z = 90;

	static const int KEY_F1 = 112;
	static const int KEY_F2 = 113;
	static const int KEY_F3 = 114;
	static const int KEY_F4 = 115;
	static const int KEY_F5 = 116;
	static const int KEY_F6 = 117;
	static const int KEY_F7 = 118;
	static const int KEY_F8 = 119;
	static const int KEY_F9 = 120;
	static const int KEY_F10 = 121;
	static const int KEY_F11 = 122;
	static const int KEY_F12 = 123;

	static const int KEY_SLASH = 191;

	static const int KEY_ANDROID_MENU = 255;

	static bool isKeyDown(int);



	static void reset();






	static void feed(unsigned char, int);




	static void feedText(const std::string&, bool);



	static bool next();







	static bool nextTextChar();






	static void rewind();




	static int getEventKey();



	static int getEventKeyState();


	static KeyboardTextInput getUtf8Input();


private:
	static int _index;
	static int _textIndex;
	static int _states[];
	static std::vector<KeyboardAction, std::allocator<KeyboardAction> > _inputs;
	static std::vector<KeyboardTextInput, std::allocator<KeyboardTextInput> > _inputText;
	static bool _inited;
};
