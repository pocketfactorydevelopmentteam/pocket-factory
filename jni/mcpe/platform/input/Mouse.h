#pragma once
#include <memory>
#include <vector>





class MouseAction {

public:
	static const char ACTION_MOVE = 0;
	static const char ACTION_LEFT = 1;
	static const char ACTION_RIGHT = 2;
	static const char ACTION_WHEEL = 3;

	static const char DATA_UP = 0;
	static const char DATA_DOWN = 1;

	MouseAction(char, char, short, short, char);
	MouseAction(char, char, short, short, short, short, char);

	bool isButton() const;

	short x, y;
	short dx, dy;
	char action;
	char data;
	char pointerId;
};







class MouseDevice {

public:
	static const int MAX_NUM_BUTTONS = 4;

	MouseDevice();

	char getButtonState(int);
	bool isButtonDown(int);


	bool wasFirstMovement();

	short getX();
	short getY();

	short getDX();
	short getDY();

	void reset();
	void reset2();

	bool next();
	void rewind();

	bool getEventButtonState();
	char getEventButton();
	const MouseAction& getEvent();

	void feed(char, char, short, short);
	void feed(char, char, short, short, short, short);

private:
	int _index;
	short _x, _y;
	short _dx, _dy;
	short _xOld, _yOld;
	char _buttonStates[4];
	std::vector<MouseAction, std::allocator<MouseAction> > _inputs;
	int _firstMovementType;
	static const int DELTA_NOTSET = -9999;
};
