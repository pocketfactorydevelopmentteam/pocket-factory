#pragma once
#include "Mouse.h"
#include <memory>
#include <vector>


	typedef MouseDevice TouchPointer;

class Multitouch {

public:
	static const int MAX_POINTERS = 12;

	static bool isPointerDown(int);


	static int getFirstActivePointerIdEx();




	static int getFirstActivePointerIdExThisUpdate();


	static int getActivePointerIds(const const int**);


	static int getActivePointerIdsThisUpdate(const const int**);


	static void commit();

	static short getX(int);
	static short getY(int);

	static short getDX(int);
	static short getDY(int);

	static bool wasFirstMovement(int);

	static bool isPressed(int);
	static bool isReleased(int);

	static bool isEdgeTouch(int);

	static bool isPressedThisUpdate(int);
	static bool isReleasedThisUpdate(int);

	static void reset();
	static void resetThisUpdate();

	static bool next();

	static void rewind();



	static MouseAction& getEvent();



	static void feed(char, char, short, short, char);

private:
	static const std::__split_buffer<MouseAction, std::allocator<MouseAction> &>::value_type g(int);



	static int _clampPointerId(int);






	static bool _wasPressed[];
	static bool _wasReleased[];
	static bool _wasEdgeTouch[];


	static bool _wasPressedThisUpdate[];
	static bool _wasReleasedThisUpdate[];

	static TouchPointer _pointers[];
	static std::vector<MouseAction, std::allocator<MouseAction> > _inputs;

	static int _activePointerList[];
	static int _activePointerCount;

	static int _activePointerThisUpdateList[];
	static int _activePointerThisUpdateCount;

	static int _index;
};
