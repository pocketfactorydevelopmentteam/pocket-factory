#pragma once




class Controller {

public:
	static const int NUM_TRIGGERS = 2;
	static const int NUM_STICKS = 2;
	static const int STATE_TOUCH = 1;
	static const int STATE_RELEASE = 0;
	static const int STATE_MOVE = -1;
	static const int KEY_JUMP = 23;
	static const int KEY_LEFT_SHOULDER = 102;
	static const int KEY_RIGHT_SHOULDER = 103;
	static const int KEY_MENU_INVENTORY = 100;
	static const int CONTROLLER_BACK = 4;

	static const float DIRECTION_X_THRESHOLD;
	static const float DIRECTION_Y_THRESHOLD;

enum StickDirection { NONE, UP, RIGHT, DOWN, LEFT };

	static bool isTouched(int);

	static void feed(int, int, float, float);
	static void feedTrigger(int, float);

	static float getX(int);
	static float getY(int);
	static Controller::StickDirection getDirection(int);
	static Controller::StickDirection getXDirection(int, float);
	static Controller::StickDirection getYDirection(int, float);
	static float getPressure(int);

	static float getTransformedX(int, float, float, bool);
	static float getTransformedY(int, float, float, bool);

	static void reset();
	static bool isReset();

private:
	static bool isValidStick(int);
	static bool isValidTrigger(int);
	static float linearTransform(float, float, float, bool);

	static float triggerValues[];
	static float stickValuesX[];
	static float stickValuesY[];
	static bool isTouchedValues[];
	static bool inReset;
};
