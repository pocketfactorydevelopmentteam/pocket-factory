#pragma once
#include <string>
#include "IOSRestCaller.h"










struct IOSRestCallerGlue : public std::basic_string<char>* {
protected:
	IOSRestCallerContainer restCallerContainer;
	NSURLConnection* restConnection;
	NSMutableData* resultData;
	IOSRestCallerObjectContainer restCallerObjectContainer;
	int statusCode;
};
