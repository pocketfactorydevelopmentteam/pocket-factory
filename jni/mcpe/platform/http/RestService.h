#pragma once
#include <string>
#include <memory>
#include <functional>
#include <utility>
#include <map>
class RestService {
public:
	RestService(const std::string&);
	void setCookieData(const std::string&, const std::string&);
	const std::map<std::basic_string<char>, std::basic_string<char>, std::less<std::basic_string<char> >, std::allocator<std::pair<const std::basic_string<char>, std::basic_string<char> > > > getCookieData() const;
	std::string getCookieDataAsString() const;
	const std::string& getServiceURL() const;
protected:
	std::map<std::basic_string<char>, std::basic_string<char>, std::less<std::basic_string<char> >, std::allocator<std::pair<const std::basic_string<char>, std::basic_string<char> > > > _cookies;
	std::string _serviceAddress;
};
