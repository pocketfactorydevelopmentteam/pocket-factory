#pragma once
#include "../../network/http/RestRequestJob.h"
#include <mutex>
namespace std { struct IOSRestRequest; };
#include <string>
#include <memory>









class IOSRestCallerJob : public RestRequestJob {
public:
	IOSRestCallerJob() const;
	virtual void stop() const;
	virtual void onOperationComplete(int, const std::string&) const;
	virtual void retryRequest() const;
protected:
	virtual void run() const;
	virtual void finish() const;
	virtual void createAndRunRequest() const;



	std::mutex _iosRequestLock;
	std::mutex _iosWaitLock;
	condition_variable _waitSynchronization;
	std::IOSRestRequest* _request;
	int _returnStatusCode;
	std::string _returnContent;
	bool _retryRequest;
};

struct IOSRestCallerJobContainer {
	IOSRestCallerJobContainer();
	IOSRestCallerJobContainer(IOSRestCallerJob*);


	IOSRestCallerJob* restCaller;
};
