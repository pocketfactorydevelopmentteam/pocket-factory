#pragma once
#include "IOSRestCallerJob.h"











struct IOSRestRequest : public NSObject {
protected:
	IOSRestCallerJobContainer _restCallerJobContainer;
	NSURLConnection* _restConnection;
	NSMutableData* _resultData;
	int _statusCode;
	int _retryTime;
};
