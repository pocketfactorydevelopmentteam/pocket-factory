#pragma once
struct RestCallerObject;
struct IOSRestCallerGlue;
#include <memory>
#include <vector>
#include <string>
class MCOConnector;








class IOSRestCaller : public RestCaller {
public:
	virtual void httpSuccess(int, const std::string&, RestCallerObject*);
	virtual void httpFailed(int, const std::string&, RestCallerObject*);
	virtual void generalFailed(RestCallerObject*);
	virtual void callComplete(IOSRestCallerGlue*);
	IOSRestCaller(MCOConnector*, const std::string&);
	virtual std::string urlEscape(const std::string&);
protected:
	virtual void makeRequest(RestCallerObject*);
	virtual void requestStop();
	virtual int numOfCurrentRequests();


	std::vector<IOSRestCallerGlue *, std::allocator<IOSRestCallerGlue *> > restCallers;
};

struct IOSRestCallerContainer {
	IOSRestCallerContainer();
	IOSRestCallerContainer(IOSRestCaller*);


	IOSRestCaller* restCaller;
};

struct IOSRestCallerObjectContainer {
	IOSRestCallerObjectContainer();
	IOSRestCallerObjectContainer(RestCallerObject*);


	RestCallerObject* operation;
};
