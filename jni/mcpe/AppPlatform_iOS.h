#pragma once
#include <string>
#include <memory>
#include <vector>

#include "AppPlatform.h"
#include "util/Token.h"

struct minecraftpeViewController;
struct ImageData;

class AppPlatform_iOS : public AppPlatform {


public:
	AppPlatform_iOS(minecraftpeViewController*);

	void setBasePath(const std::string&);

	void saveScreenshot(const std::string&, int, int);



	unsigned int rgbToBgr(unsigned int);



	virtual void showDialog(int);
	virtual int getUserInputStatus();
	virtual StringVector getUserInput();

	virtual void swapBuffers();

	virtual std::string getImagePath(const std::string&, bool);

	virtual void loadPNG(ImageData&, const std::string&);

	virtual std::string readAssetFile(const std::string&);

	virtual std::string getDateString(int);

	virtual int checkLicense();








	virtual void buyGame();

	virtual int getScreenWidth();
	virtual int getScreenHeight();
	virtual float getPixelsPerMillimeter();

	virtual bool isTouchscreen();
	virtual bool supportsVibration();


	virtual bool hasIDEProfiler();



	virtual void vibrate(int);

	virtual bool isNetworkEnabled(bool);

	virtual StringVector getOptionStrings();

	virtual void showKeyboard(const std::string&, int, bool);
	virtual void hideKeyboard();
	virtual bool isPowerVR();
	virtual void openLoginWindow();
	virtual std::allocator<Token> getLoginInformation();
	virtual void setLoginInformation(const std::allocator<Token>&);
	virtual void clearSessionIDAndRefreshToken();

	virtual std::vector<std::basic_string<char>, std::allocator<std::basic_string<char> > > getBroadcastAddresses();

	virtual int64_t getAvailableMemory();

private:
	std::string _basePath;
	minecraftpeViewController* _viewController;

	bool vibrationSupported, IDEProfiler;
};



























































































































































































































































struct __block_descriptor {};
};
